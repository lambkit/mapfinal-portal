package com.mapfinal.server.amap;

import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.kit.StrKit;
import com.mapfinal.data.MapLayer;
import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;

/**
 * 高德地图
 * @author yangyong
 *
 */
public class AMapServer implements Server {

	private String name;
	private AMapConfig config;
	private Map<String, ServerLayer> serverLayers;
	private int rate = 0;

	public static AMapServer create(AMapConfig config) {
		if (config == null)
			return null;
		AMapServer server = new AMapServer();
		server.setConfig(config);
		return server;
	}

	@Override
	public boolean publish(MapLayer layer) {
		// TODO Auto-generated method stub
		// not finish
		return false;
	}
	
	public boolean hasLayer(String layerName) {
		if(StrKit.isBlank(layerName)) return false;
		return serverLayers.containsKey(layerName);
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return config.getUrl();
	}

	@Override
	public boolean isCluster() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addServerLayer(ServerLayer serverLayer) {
		if (serverLayer == null)
			return;
		if (serverLayers == null) {
			serverLayers = Maps.newHashMap();
		}
		serverLayers.put(serverLayer.getName(), serverLayer);
	}
	
	@Override
	public ServerLayer getServerLayer(String layerName) {
		// TODO Auto-generated method stub
		if (serverLayers == null) return null;
		return serverLayers.get(layerName);
	}

	@Override
	public Map<String, ServerLayer> getServerLayers() {
		// TODO Auto-generated method stub
		return serverLayers;
	}
	
	public void setServerLayers(Map<String, ServerLayer> serverLayers) {
		this.serverLayers = serverLayers;
	}

	public AMapConfig getConfig() {
		return config;
	}

	public void setConfig(AMapConfig config) {
		this.config = config;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	
}
