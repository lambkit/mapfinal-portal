package com.mapfinal.data;

import java.io.File;
import java.util.Map;

import com.google.common.collect.Maps;
import com.jfinal.kit.PathKit;
import com.lambkit.core.aop.AopKit;
import com.lambkit.db.datasource.DataSourceConfig;

/**
 *存储管理
 * @author yangyong
 *
 */
public class StoreManager {

	private static StoreManager manager = null;
	
	public static StoreManager me() {
		if(manager==null) {
			manager = AopKit.singleton(StoreManager.class);
		}
		return manager;
	}
	
	//private String workspace;
	private Map<String, MapStore> stores;
	
	/**
	 * 加入存储
	 * @param store
	 */
	public void addStore(MapStore store) {
		if(stores==null) {
			stores = Maps.newHashMap();
		}
		stores.put(store.getName(), store);
	}
	
	public MapStore getStore(String name) {
		if(stores!=null) {
			return stores.get(name);
		}
		return null;
	}
	
	public MapStore getDefaultStore() {
		if(stores!=null) {
			return stores.get(DataSourceConfig.NAME_DEFAULT);
		}
		return null;
	}

	public Map<String, MapStore> getStores() {
		return stores;
	}

	public void setStores(Map<String, MapStore> stores) {
		this.stores = stores;
	}
	
	

	/*
	public String getWorkspace() {
		return workspace;
	}

	public void setWorkspace(String workspace) {
		this.workspace = workspace;
	}
	*/
	
	public String getWorkspacePath() {
		String path = PathKit.getWebRootPath();
		path += File.separator + "data" + File.separator + "workspace";
		return path;
	}
}
