package com.lambkit.module.cms.web.handle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.kit.HandlerKit;
import com.jfinal.kit.StrKit;
import com.lambkit.module.cms.common.CmsConsts;
import com.lambkit.module.cms.common.CmsOptions;

/**
 * @author yangyong
 * @Title: 伪静态处理器
 */
public class CmsHandler extends Handler {

    private static final ThreadLocal<String> targetContext = new ThreadLocal<>();
    private static final ThreadLocal<HttpServletRequest> requestContext = new ThreadLocal<>();

    public static String getCurrentTarget() {
        return targetContext.get();
    }

    public static HttpServletRequest getCurrentRequest() {
        return requestContext.get();
    }

    private static final String ADDON_TARGET_PREFIX = "/addons";
    private static final String TEMPLATES_TARGET_PREFIX = "/templates";
    private static final String ATTACHMENT_TARGET_PREFIX = "/attachment";


    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {

        //不让访问 插件目录 下的 .html、 .sql 文件 和 WEB-INF 目录下的任何文件
        if (target.startsWith(ADDON_TARGET_PREFIX)) {
            if (target.endsWith(".html")
                    || target.endsWith(".sql")
                    || target.contains("WEB-INF")) {
                HandlerKit.renderError404(request, response, isHandled);
                return;
            }
        }

        //不让访问 模板目录 下的 .html 文件
        if (target.startsWith(TEMPLATES_TARGET_PREFIX)) {
            if (target.endsWith(".html")) {
                HandlerKit.renderError404(request, response, isHandled);
                return;
            }
        }

        //附件目录
        if (target.startsWith(ATTACHMENT_TARGET_PREFIX)) {
            //AttachmentHandlerKit.handle(target,request,response,isHandled);
            return;
        }


        String suffix = CmsOptions.getAppUrlSuffix();
        if (StrKit.isBlank(suffix)  // 不启用伪静态
                && target.indexOf('.') != -1) {
            //return 表示让服务器自己去处理
            return;
        }

        //启用伪静态
        if (StrKit.notBlank(suffix) && target.endsWith(suffix)) {
            target = target.substring(0, target.length() - suffix.length());
        }

        try {
            targetContext.set(target);
            requestContext.set(request);
            request.setAttribute("VERSION", CmsConsts.VERSION);
            request.setAttribute("CPATH", request.getContextPath());
            next.handle(target, request, response, isHandled);
        } finally {
            targetContext.remove();
            requestContext.remove();
        }
    }
}
