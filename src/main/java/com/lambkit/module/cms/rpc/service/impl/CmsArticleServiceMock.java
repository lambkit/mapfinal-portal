/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.service.impl;

import java.util.List;

import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.service.BaseServiceMock;

import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.cms.rpc.api.CmsArticleService;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-22
 * @version 1.0
 * @since 1.0
 */
public class CmsArticleServiceMock extends BaseServiceMock<CmsArticle> implements CmsArticleService {

	@Override
	public Page<CmsArticle> paginateInNormal(Integer pageNumber, Integer pageSize, String orderBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmsArticle findFirstByAlias(String alias) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<CmsArticle> search(Integer pageNumber, Integer pageSize, String keyword) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CmsArticle> findListByCategoryId(Long categoryId, String orderBy, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<CmsArticle> paginateByCategoryId(Integer pageNumber, Integer pageSize, Long categoryId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<CmsArticle> paginateByCategoryIdInNormal(Integer pageNumber, Integer pageSize, Long categoryId,
			String orderBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmsArticle findNextById(Long articleId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CmsArticle findPreviousById(Long articleId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CmsArticle> findRelevantListByArticleId(Long articleId, Integer status, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<CmsArticle> findListByTagId(Long tagId, String orderBy, Integer count) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<CmsArticle> paginateByTagId(Integer pageNumber, Integer pageSize, Long tagId) {
		// TODO Auto-generated method stub
		return null;
	}

}