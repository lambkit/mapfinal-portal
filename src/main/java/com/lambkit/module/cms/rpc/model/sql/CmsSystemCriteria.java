/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsSystemCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsSystemCriteria create() {
		return new CmsSystemCriteria();
	}
	
	public static CmsSystemCriteria create(Column column) {
		CmsSystemCriteria that = new CmsSystemCriteria();
		that.add(column);
        return that;
    }

    public static CmsSystemCriteria create(String name, Object value) {
        return (CmsSystemCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_system", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsSystemCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsSystemCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsSystemCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsSystemCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsSystemCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsSystemCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsSystemCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsSystemCriteria andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsSystemCriteria andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsSystemCriteria andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsSystemCriteria andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
       public CmsSystemCriteria andSystemIdEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdNotEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdGreaterThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdLessThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsSystemCriteria andSystemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsSystemCriteria andSystemIdIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andSystemIdNotIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Long", "Float");
          return this;
      }
	public CmsSystemCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsSystemCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsSystemCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsSystemCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public CmsSystemCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public CmsSystemCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public CmsSystemCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsSystemCriteria andCodeIsNull() {
		isnull("code");
		return this;
	}
	
	public CmsSystemCriteria andCodeIsNotNull() {
		notNull("code");
		return this;
	}
	
	public CmsSystemCriteria andCodeIsEmpty() {
		empty("code");
		return this;
	}

	public CmsSystemCriteria andCodeIsNotEmpty() {
		notEmpty("code");
		return this;
	}
        public CmsSystemCriteria andCodeLike(java.lang.String value) {
    	   addCriterion("code", value, ConditionMode.FUZZY, "code", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemCriteria andCodeNotLike(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_FUZZY, "code", "java.lang.String", "String");
          return this;
      }
      public CmsSystemCriteria andCodeEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeNotEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeGreaterThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeLessThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("code", value1, value2, ConditionMode.BETWEEN, "code", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemCriteria andCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("code", value1, value2, ConditionMode.NOT_BETWEEN, "code", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemCriteria andCodeIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.IN, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andCodeNotIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.NOT_IN, "code", "java.lang.String", "String");
          return this;
      }
	public CmsSystemCriteria andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsSystemCriteria andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsSystemCriteria andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsSystemCriteria andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
        public CmsSystemCriteria andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemCriteria andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsSystemCriteria andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemCriteria andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemCriteria andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemCriteria andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsSystemCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsSystemCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsSystemCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsSystemCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsSystemCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsSystemCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsSystemCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsSystemCriteria andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsSystemCriteria andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsSystemCriteria andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsSystemCriteria andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
       public CmsSystemCriteria andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsSystemCriteria andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsSystemCriteria andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemCriteria andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}