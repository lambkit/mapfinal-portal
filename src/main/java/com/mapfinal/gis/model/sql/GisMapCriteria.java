/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisMapCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisMapCriteria create() {
		return new GisMapCriteria();
	}
	
	public static GisMapCriteria create(Column column) {
		GisMapCriteria that = new GisMapCriteria();
		that.add(column);
        return that;
    }

    public static GisMapCriteria create(String name, Object value) {
        return (GisMapCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_map", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisMapCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisMapCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisMapCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisMapCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisMapCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisMapCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisMapCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisMapCriteria andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public GisMapCriteria andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public GisMapCriteria andIdIsEmpty() {
		empty("id");
		return this;
	}

	public GisMapCriteria andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
       public GisMapCriteria andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public GisMapCriteria andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public GisMapCriteria andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public GisMapCriteria andAppidIsNull() {
		isnull("appid");
		return this;
	}
	
	public GisMapCriteria andAppidIsNotNull() {
		notNull("appid");
		return this;
	}
	
	public GisMapCriteria andAppidIsEmpty() {
		empty("appid");
		return this;
	}

	public GisMapCriteria andAppidIsNotEmpty() {
		notEmpty("appid");
		return this;
	}
       public GisMapCriteria andAppidEqualTo(java.lang.Long value) {
          addCriterion("appid", value, ConditionMode.EQUAL, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidNotEqualTo(java.lang.Long value) {
          addCriterion("appid", value, ConditionMode.NOT_EQUAL, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidGreaterThan(java.lang.Long value) {
          addCriterion("appid", value, ConditionMode.GREATER_THEN, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("appid", value, ConditionMode.GREATER_EQUAL, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidLessThan(java.lang.Long value) {
          addCriterion("appid", value, ConditionMode.LESS_THEN, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("appid", value, ConditionMode.LESS_EQUAL, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("appid", value1, value2, ConditionMode.BETWEEN, "appid", "java.lang.Long", "Float");
    	  return this;
      }

      public GisMapCriteria andAppidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("appid", value1, value2, ConditionMode.NOT_BETWEEN, "appid", "java.lang.Long", "Float");
          return this;
      }
        
      public GisMapCriteria andAppidIn(List<java.lang.Long> values) {
          addCriterion("appid", values, ConditionMode.IN, "appid", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andAppidNotIn(List<java.lang.Long> values) {
          addCriterion("appid", values, ConditionMode.NOT_IN, "appid", "java.lang.Long", "Float");
          return this;
      }
	public GisMapCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public GisMapCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public GisMapCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public GisMapCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public GisMapCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public GisMapCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public GisMapCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public GisMapCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public GisMapCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public GisMapCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisMapCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisMapCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisMapCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisMapCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public GisMapCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public GisMapCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisMapCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisMapCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisMapCriteria andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public GisMapCriteria andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public GisMapCriteria andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public GisMapCriteria andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
       public GisMapCriteria andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public GisMapCriteria andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public GisMapCriteria andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public GisMapCriteria andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public GisMapCriteria andTemplateIsNull() {
		isnull("template");
		return this;
	}
	
	public GisMapCriteria andTemplateIsNotNull() {
		notNull("template");
		return this;
	}
	
	public GisMapCriteria andTemplateIsEmpty() {
		empty("template");
		return this;
	}

	public GisMapCriteria andTemplateIsNotEmpty() {
		notEmpty("template");
		return this;
	}
        public GisMapCriteria andTemplateLike(java.lang.String value) {
    	   addCriterion("template", value, ConditionMode.FUZZY, "template", "java.lang.String", "Float");
    	   return this;
      }

      public GisMapCriteria andTemplateNotLike(java.lang.String value) {
          addCriterion("template", value, ConditionMode.NOT_FUZZY, "template", "java.lang.String", "Float");
          return this;
      }
      public GisMapCriteria andTemplateEqualTo(java.lang.String value) {
          addCriterion("template", value, ConditionMode.EQUAL, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateNotEqualTo(java.lang.String value) {
          addCriterion("template", value, ConditionMode.NOT_EQUAL, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateGreaterThan(java.lang.String value) {
          addCriterion("template", value, ConditionMode.GREATER_THEN, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("template", value, ConditionMode.GREATER_EQUAL, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateLessThan(java.lang.String value) {
          addCriterion("template", value, ConditionMode.LESS_THEN, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateLessThanOrEqualTo(java.lang.String value) {
          addCriterion("template", value, ConditionMode.LESS_EQUAL, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("template", value1, value2, ConditionMode.BETWEEN, "template", "java.lang.String", "String");
    	  return this;
      }

      public GisMapCriteria andTemplateNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("template", value1, value2, ConditionMode.NOT_BETWEEN, "template", "java.lang.String", "String");
          return this;
      }
        
      public GisMapCriteria andTemplateIn(List<java.lang.String> values) {
          addCriterion("template", values, ConditionMode.IN, "template", "java.lang.String", "String");
          return this;
      }

      public GisMapCriteria andTemplateNotIn(List<java.lang.String> values) {
          addCriterion("template", values, ConditionMode.NOT_IN, "template", "java.lang.String", "String");
          return this;
      }
}