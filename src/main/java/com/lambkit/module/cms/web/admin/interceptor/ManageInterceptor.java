package com.lambkit.module.cms.web.admin.interceptor;

import com.jfinal.log.Log;


import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * 后台过滤器
 * Created by ZhangShuzheng on 2017/01/12.
 */
public class ManageInterceptor implements Interceptor {

	private static final Log LOG = Log.getLog(ManageInterceptor.class);

	@Override
	public void intercept(Invocation inv) {
		// TODO Auto-generated method stub
		inv.invoke();
	}


}
