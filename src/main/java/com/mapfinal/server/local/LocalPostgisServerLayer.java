package com.mapfinal.server.local;

import java.util.List;

import org.geotools.map.Layer;

import com.mapfinal.data.MapLayer;
import com.mapfinal.data.postgis.PostgisKit;
import com.mapfinal.data.postgis.PostgisStore;
import com.mapfinal.server.Server;

public class LocalPostgisServerLayer extends LocalServerLayer {

	private String name;
	private String localName;
	private Server server;
	private List<String> mapApis;
	
	public static LocalPostgisServerLayer create(PostgisStore store, Server server, MapLayer layer) {
		if(layer==null || server==null) return null;
		LocalPostgisServerLayer serverLayer = new LocalPostgisServerLayer();
		Layer geoLayer = PostgisKit.createLayer(store.getConfig(), layer.getNativeName(), layer.getDefaultStyle());
		serverLayer.addLayer(geoLayer);
		serverLayer.setLocalName(layer.getName());
		serverLayer.setServer(server);
		layer.addServerLayer(serverLayer);
		server.addServerLayer(serverLayer);
		return serverLayer;
	}
	
	public void close() {
		server = null;
		if(mapApis!=null) mapApis.clear();
		mapApis = null;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public String localName() {
		// TODO Auto-generated method stub
		return localName;
	}
	
	@Override
	public Server getServer() {
		// TODO Auto-generated method stub
		return server;
	}

	@Override
	public List<String> getMapApis() {
		// TODO Auto-generated method stub
		return mapApis;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	public void setMapApis(List<String> mapApis) {
		this.mapApis = mapApis;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocalName() {
		return localName;
	}

	public void setLocalName(String localName) {
		this.localName = localName;
	}
}
