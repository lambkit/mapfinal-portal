/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.web.tag;

import java.io.IOException;
import java.util.Map;

import com.mapfinal.gis.model.GisServerApi;
import com.mapfinal.gis.service.GisServerApiService;
import com.lambkit.common.util.StringUtils;
import com.lambkit.web.tag.LambkitTemplateModel;
import com.jfinal.kit.StrKit;
import com.jfinal.render.FreeMarkerRender;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-27
 * @version 1.0
 * @since 1.0
 */
/**
 * gis_server_api标签<br>
 * 参数：{id:主键}
 * 返回值：{entity:gis_server_api信息}
 * @author lambkit
 */
public class GisServerApiTag extends LambkitTemplateModel {

	@Override
	public void onRender(Environment env, Map params, TemplateModel[] loopVars,
			TemplateDirectiveBody body) throws TemplateException, IOException {
		// TODO Auto-generated method stub
		String id = get(params, "id");
		String serverkey = get(params, "serverkey");
		String layerkey = get(params, "layerkey");
		String name = get(params, "name");
		String title = get(params, "title");
		String url = get(params, "url");
		String type = get(params, "type");
		String apiId = get(params, "api_id");
		String urlpattern = get(params, "urlpattern");
		String targeturi = get(params, "targetUri");
		String status = get(params, "status");
		int pagenum = getInt(params, "pagenum", 0);
		int pagesize = getInt(params, "pagesize", 0);
		String wheresql = get(params, "sql", null);
		String sql = " from gis_server_api where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(id)) sql += " and id=" + id;//bigserial
			if(StringUtils.hasText(serverkey)) sql += " and serverkey like '%" + serverkey + "%'";//varchar
			if(StringUtils.hasText(layerkey)) sql += " and layerkey like '%" + layerkey + "%'";//varchar
			if(StringUtils.hasText(name)) sql += " and name like '%" + name + "%'";//varchar
			if(StringUtils.hasText(title)) sql += " and title like '%" + title + "%'";//varchar
			if(StringUtils.hasText(url)) sql += " and url like '%" + url + "%'";//varchar
			if(StringUtils.hasText(type)) sql += " and type like '%" + type + "%'";//varchar
			if(StringUtils.hasText(apiId)) sql += " and api_id=" + apiId;//int8
			if(StringUtils.hasText(urlpattern)) sql += " and urlpattern like '%" + urlpattern + "%'";//varchar
			if(StringUtils.hasText(targeturi)) sql += " and targetUri like '%" + targeturi + "%'";//varchar
			if(StringUtils.hasText(status)) sql += " and status like '%" + status + "%'";//varchar
		} else {
			sql += wheresql;
		}
		
		String orderby = get(params, "orderby", null);
		
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		
		GisServerApiService service = GisServerApi.service();
		
		String tagEntityKeyname = get(params, "key", "entity");
		if(pagenum==0) {
			env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
					service.dao().findFirst("select *" + sql)));
		} else {
			if(pagesize==0) {
				env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
						service.dao().find("select *" + sql)));
			} else {
				env.setVariable(tagEntityKeyname, FreeMarkerRender.getConfiguration().getObjectWrapper().wrap(
						service.dao().paginate(pagenum, pagesize, "select *", sql)));
			}
		}
        body.render(env.getOut());
	}
}
