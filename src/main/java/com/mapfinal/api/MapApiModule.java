package com.mapfinal.api;

import com.jfinal.config.Routes;
import com.lambkit.Lambkit;
import com.lambkit.common.service.ServiceManager;
import com.lambkit.core.rpc.RpcConfig;
import com.lambkit.module.LambkitModule;
import com.mapfinal.MapFinalConfig;
import com.mapfinal.api.controller.MapApiController;
import com.mapfinal.api.service.GeoApiService;
import com.mapfinal.api.service.GeoApiServiceImpl;
import com.mapfinal.api.service.GeoApiServiceMock;

public class MapApiModule extends LambkitModule {

	@Override
	public void configRoute(Routes me) {
		me.add("/api", MapApiController.class); 
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		if("server".equals(getConfig().getServerType())) {
			registerLocalService();
		} else if("client".equals(getConfig().getServerType())) {
			registerRemoteService();
		} 
	}
	
	public void registerLocalService() {
		registerLocalService(getRpcGroup(), getRpcVersion(), getRpcPort());
	}
	
	public void registerLocalService(String group, String version, int port) {
		ServiceManager.me().mapping(GeoApiService.class, GeoApiServiceImpl.class, GeoApiServiceMock.class, group, version, port);
	}
	
	public void registerRemoteService() {
		registerRemoteService(getRpcGroup(), getRpcVersion(), getRpcPort());
	}
	
	public void registerRemoteService(String group, String version, int port) {
		ServiceManager.me().remote(GeoApiService.class, GeoApiServiceMock.class, group, version, port);
	}
	
	public int getRpcPort() {
		return Lambkit.config(RpcConfig.class).getDefaultPort();
	}
	
	public String getRpcGroup() {
		return Lambkit.config(RpcConfig.class).getDefaultGroup();
	}
	
	public String getRpcVersion() {
		return getConfig().getVersion();
	}
	
	public MapFinalConfig getConfig() {
		return Lambkit.config(MapFinalConfig.class);
	}
}
