/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsCategoryTagCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsCategoryTagCriteria create() {
		return new CmsCategoryTagCriteria();
	}
	
	public static CmsCategoryTagCriteria create(Column column) {
		CmsCategoryTagCriteria that = new CmsCategoryTagCriteria();
		that.add(column);
        return that;
    }

    public static CmsCategoryTagCriteria create(String name, Object value) {
        return (CmsCategoryTagCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_category_tag", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryTagCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryTagCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsCategoryTagCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsCategoryTagCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryTagCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryTagCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryTagCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryTagCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsCategoryTagCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsCategoryTagCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsCategoryTagCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsCategoryTagCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsCategoryTagCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsCategoryTagCriteria andCategoryTagIdIsNull() {
		isnull("category_tag_id");
		return this;
	}
	
	public CmsCategoryTagCriteria andCategoryTagIdIsNotNull() {
		notNull("category_tag_id");
		return this;
	}
	
	public CmsCategoryTagCriteria andCategoryTagIdIsEmpty() {
		empty("category_tag_id");
		return this;
	}

	public CmsCategoryTagCriteria andCategoryTagIdIsNotEmpty() {
		notEmpty("category_tag_id");
		return this;
	}
       public CmsCategoryTagCriteria andCategoryTagIdEqualTo(java.lang.Long value) {
          addCriterion("category_tag_id", value, ConditionMode.EQUAL, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdNotEqualTo(java.lang.Long value) {
          addCriterion("category_tag_id", value, ConditionMode.NOT_EQUAL, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdGreaterThan(java.lang.Long value) {
          addCriterion("category_tag_id", value, ConditionMode.GREATER_THEN, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_tag_id", value, ConditionMode.GREATER_EQUAL, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdLessThan(java.lang.Long value) {
          addCriterion("category_tag_id", value, ConditionMode.LESS_THEN, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_tag_id", value, ConditionMode.LESS_EQUAL, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("category_tag_id", value1, value2, ConditionMode.BETWEEN, "categoryTagId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("category_tag_id", value1, value2, ConditionMode.NOT_BETWEEN, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategoryTagCriteria andCategoryTagIdIn(List<java.lang.Long> values) {
          addCriterion("category_tag_id", values, ConditionMode.IN, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryTagIdNotIn(List<java.lang.Long> values) {
          addCriterion("category_tag_id", values, ConditionMode.NOT_IN, "categoryTagId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategoryTagCriteria andCategoryIdIsNull() {
		isnull("category_id");
		return this;
	}
	
	public CmsCategoryTagCriteria andCategoryIdIsNotNull() {
		notNull("category_id");
		return this;
	}
	
	public CmsCategoryTagCriteria andCategoryIdIsEmpty() {
		empty("category_id");
		return this;
	}

	public CmsCategoryTagCriteria andCategoryIdIsNotEmpty() {
		notEmpty("category_id");
		return this;
	}
       public CmsCategoryTagCriteria andCategoryIdEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdNotEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.NOT_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdGreaterThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdLessThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("category_id", value1, value2, ConditionMode.BETWEEN, "categoryId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategoryTagCriteria andCategoryIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("category_id", value1, value2, ConditionMode.NOT_BETWEEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategoryTagCriteria andCategoryIdIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andCategoryIdNotIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.NOT_IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategoryTagCriteria andTagIdIsNull() {
		isnull("tag_id");
		return this;
	}
	
	public CmsCategoryTagCriteria andTagIdIsNotNull() {
		notNull("tag_id");
		return this;
	}
	
	public CmsCategoryTagCriteria andTagIdIsEmpty() {
		empty("tag_id");
		return this;
	}

	public CmsCategoryTagCriteria andTagIdIsNotEmpty() {
		notEmpty("tag_id");
		return this;
	}
       public CmsCategoryTagCriteria andTagIdEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdNotEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.NOT_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdGreaterThan(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.GREATER_THEN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.GREATER_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdLessThan(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.LESS_THEN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("tag_id", value, ConditionMode.LESS_EQUAL, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("tag_id", value1, value2, ConditionMode.BETWEEN, "tagId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategoryTagCriteria andTagIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("tag_id", value1, value2, ConditionMode.NOT_BETWEEN, "tagId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategoryTagCriteria andTagIdIn(List<java.lang.Long> values) {
          addCriterion("tag_id", values, ConditionMode.IN, "tagId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryTagCriteria andTagIdNotIn(List<java.lang.Long> values) {
          addCriterion("tag_id", values, ConditionMode.NOT_IN, "tagId", "java.lang.Long", "Float");
          return this;
      }
}