package com.mapfinal.server;

import com.jfinal.config.Routes;
import com.mapfinal.server.geoserver.GeoserverController;

/**
 * 服务接口  路由
 * @author yangyong
 */
public class ServerRoute extends Routes {

	@Override
	public void config() {
		// TODO Auto-generated method stub
		
		//LocalServer
		
		//GeoServer
		add("/map/geoserver", GeoserverController.class);
		//ArcGIS Server
		
		//SuperMap Sever
		
		//Baidu BMap
		
		//AMap
		
		//Tianditu
		
	}

}
