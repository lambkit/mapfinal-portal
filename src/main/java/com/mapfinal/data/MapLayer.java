package com.mapfinal.data;

import java.util.List;

import com.mapfinal.server.Server;
import com.mapfinal.server.ServerLayer;

public interface MapLayer {
	/**
	 * 名称（唯一值）
	 * @return
	 */
	String getName();
	/**
	 * 标题
	 * @return
	 */
	String getTitle();
	/**
	 * 原有名称，表格名、文件名
	 * @return
	 */
	String getNativeName();
	/**
	 * 默认样式
	 * @return
	 */
	String getDefaultStyle();
	/**
	 * 存储名称
	 * @return
	 */
	String getStoreName();
	/**
	 * 坐标系统
	 * @return
	 */
	String getSRS();
	/**
	 * 是否发布
	 * @return
	 */
	boolean isPublish();
	/**
	 * 是否地图（可发布）图层
	 * @return
	 */
	boolean publishLayer();

	/**
	 * 所属服务器的图层信息
	 * @return
	 */
	List<ServerLayer> getServerLayers();
	
	void addServerLayer(ServerLayer serverLayer);
	/**
	 * 数据接口
	 * @return
	 */
	List<String> getDataApis();

	void addDataApi(String api);
	
	/**
	 * 发布图层
	 * @param server
	 * @return
	 */
	boolean publish(Server server);
	
}
