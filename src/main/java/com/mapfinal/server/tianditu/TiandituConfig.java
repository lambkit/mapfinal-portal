package com.mapfinal.server.tianditu;

import com.lambkit.core.config.annotation.PropertieConfig;

@PropertieConfig(prefix="mapfinal.server.tianditu")
public class TiandituConfig {
	
	private String name = "tianditu";
	private String version;
	private String url = "http://api.tianditu.gov.cn";
	private String tk;

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getTk() {
		return tk;
	}
	public void setTk(String tk) {
		this.tk = tk;
	}
}
