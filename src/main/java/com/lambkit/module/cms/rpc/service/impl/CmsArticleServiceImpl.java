/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.common.service.LambkitModelServiceImpl;
import com.lambkit.core.aop.AopKit;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.cms.rpc.api.CmsArticleService;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.cms.rpc.model.CmsArticleTag;
import com.lambkit.module.upms.rpc.model.UpmsUser;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-22
 * @version 1.0
 * @since 1.0
 */
public class CmsArticleServiceImpl extends LambkitModelServiceImpl<CmsArticle> implements CmsArticleService {
	
	private CmsArticle DAO = null;
	
	public CmsArticle dao() {
		if(DAO==null) {
			DAO = AopKit.singleton(CmsArticle.class);
		}
		return DAO;
	}

	@Override
	public Page<CmsArticle> paginateByCategoryId(Integer pageNumber, Integer pageSize, Long categoryId) {
		// TODO Auto-generated method stub
		SqlPara sqlPara = dao().getSqlPara("cms.selectCmsArticlesByCategoryId", categoryId);
		return dao().paginate(pageNumber, pageSize, sqlPara);
	}

	@Override
	public Page<CmsArticle> paginateByTagId(Integer pageNumber, Integer pageSize, Long tagId) {
		// TODO Auto-generated method stub
		SqlPara sqlPara = dao().getSqlPara("cms.selectCmsArticlesByTagId", tagId);
		return dao().paginate(pageNumber, pageSize, sqlPara);
	}

	@Override
	public Page<CmsArticle> paginateInNormal(Integer pageNumber, Integer pageSize, String orderBy) {
		// TODO Auto-generated method stub
		Example example = CmsArticle.sql().andStatusEqualTo(CmsArticle.STATUS_NORMAL).example();
		example.setOrderBy(orderBy);
		Page<CmsArticle> dataPage = paginate(pageNumber, pageSize, example);
		return joinUserInfo(dataPage);
	}

	@Override
	public CmsArticle findFirstByAlias(String alias) {
		// TODO Auto-generated method stub
		Example example = CmsArticle.sql().andAliasEqualTo(alias).example();
		return findFirst(example);
	}

	@Override
	public Page<CmsArticle> search(Integer pageNumber, Integer pageSize, String keyword) {
		Columns columns = Columns.create("status", CmsArticle.STATUS_NORMAL)
                .likeAppendPercent("title", keyword);
        return joinUserInfo(paginateByColumns(pageNumber, pageSize, columns, "order_number desc,id desc"));
	}

	@Override
	public List<CmsArticle> findListByCategoryId(Long categoryId, String orderBy, Integer count) {
		// TODO Auto-generated method stub
		Kv cond = Kv.by("categoryId", categoryId).set("orderby", orderBy).set("status", CmsArticle.STATUS_NORMAL);
		SqlPara sqlPara = dao().getSqlPara("cms.findCmsArticlesByCategoryId", cond);
		return dao().find(sqlPara, count);
	}

	@Override
	public Page<CmsArticle> paginateByCategoryIdInNormal(Integer pageNumber, Integer pageSize, Long categoryId, String orderBy) {
		Example example = CmsArticle.sql().andStatusEqualTo(CmsArticle.STATUS_NORMAL).example();
		example.setOrderBy(orderBy);
		Page<CmsArticle> dataPage = paginate(pageNumber, pageSize, example);
		return joinUserInfo(dataPage);
	}

	@Override
	public CmsArticle findNextById(Long articleId) {
		Example example = CmsArticle.sql().andArticleIdGreaterThan(articleId)
				.andStatusEqualTo(CmsArticle.STATUS_NORMAL).example();
		example.setOrderBy("article_id");
		return findFirst(example);
	}

	@Override
	public CmsArticle findPreviousById(Long articleId) {
		Example example = CmsArticle.sql().andArticleIdLessThan(articleId)
				.andStatusEqualTo(CmsArticle.STATUS_NORMAL).example();
		example.setOrderBy("article_id desc");
		return findFirst(example);
	}

	@Override
	public List<CmsArticle> findRelevantListByArticleId(Long articleId, Integer status, Integer count) {
        List<CmsArticleTag> tags = CmsArticleTag.service().findListByArticleId(articleId);
        if (tags == null || tags.isEmpty()) {
            return null;
        }
        List<Long> tagIds = tags.stream().map(tag -> tag.getTagId()).collect(Collectors.toList());
        Kv cond = Kv.by("tagIds", tagIds.toArray()).set("articleId", articleId).set("status", status);
        SqlPara sqlPara = dao().getSqlPara("cms.findRelevantListByArticleId", cond);
        return dao().find(sqlPara, count);
	}

	@Override
	public List<CmsArticle> findListByTagId(Long tagId, String orderBy, Integer count) {
		Kv cond = Kv.by("tagId", tagId).set("orderby", orderBy).set("status", CmsArticle.STATUS_NORMAL);
		SqlPara sqlPara = dao().getSqlPara("cms.findListByTagId", cond);
		return dao().find(sqlPara, count);
	}
	
	private Page<CmsArticle> joinUserInfo(Page<CmsArticle> page) {
        UpmsUser.service().join(page, "user_id");
        return page;
    }

    private List<CmsArticle> joinUserInfo(List<CmsArticle> list) {
    	UpmsUser.service().join(list, "user_id");
        return list;
    }

    private CmsArticle joinUserInfo(CmsArticle article) {
    	UpmsUser.service().join(article, "user_id");
        return article;
    }
}
