/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsMenuCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsMenuCriteria create() {
		return new CmsMenuCriteria();
	}
	
	public static CmsMenuCriteria create(Column column) {
		CmsMenuCriteria that = new CmsMenuCriteria();
		that.add(column);
        return that;
    }

    public static CmsMenuCriteria create(String name, Object value) {
        return (CmsMenuCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_menu", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsMenuCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsMenuCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsMenuCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsMenuCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsMenuCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsMenuCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsMenuCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsMenuCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsMenuCriteria andMenuIdIsNull() {
		isnull("menu_id");
		return this;
	}
	
	public CmsMenuCriteria andMenuIdIsNotNull() {
		notNull("menu_id");
		return this;
	}
	
	public CmsMenuCriteria andMenuIdIsEmpty() {
		empty("menu_id");
		return this;
	}

	public CmsMenuCriteria andMenuIdIsNotEmpty() {
		notEmpty("menu_id");
		return this;
	}
       public CmsMenuCriteria andMenuIdEqualTo(java.lang.Long value) {
          addCriterion("menu_id", value, ConditionMode.EQUAL, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdNotEqualTo(java.lang.Long value) {
          addCriterion("menu_id", value, ConditionMode.NOT_EQUAL, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdGreaterThan(java.lang.Long value) {
          addCriterion("menu_id", value, ConditionMode.GREATER_THEN, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("menu_id", value, ConditionMode.GREATER_EQUAL, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdLessThan(java.lang.Long value) {
          addCriterion("menu_id", value, ConditionMode.LESS_THEN, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("menu_id", value, ConditionMode.LESS_EQUAL, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("menu_id", value1, value2, ConditionMode.BETWEEN, "menuId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsMenuCriteria andMenuIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("menu_id", value1, value2, ConditionMode.NOT_BETWEEN, "menuId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsMenuCriteria andMenuIdIn(List<java.lang.Long> values) {
          addCriterion("menu_id", values, ConditionMode.IN, "menuId", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andMenuIdNotIn(List<java.lang.Long> values) {
          addCriterion("menu_id", values, ConditionMode.NOT_IN, "menuId", "java.lang.Long", "Float");
          return this;
      }
	public CmsMenuCriteria andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public CmsMenuCriteria andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public CmsMenuCriteria andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public CmsMenuCriteria andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
       public CmsMenuCriteria andPidEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidNotEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidGreaterThan(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidLessThan(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsMenuCriteria andPidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsMenuCriteria andPidIn(List<java.lang.Integer> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Integer", "Float");
          return this;
      }

      public CmsMenuCriteria andPidNotIn(List<java.lang.Integer> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Integer", "Float");
          return this;
      }
	public CmsMenuCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsMenuCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsMenuCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsMenuCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public CmsMenuCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public CmsMenuCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public CmsMenuCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsMenuCriteria andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public CmsMenuCriteria andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public CmsMenuCriteria andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public CmsMenuCriteria andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
        public CmsMenuCriteria andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public CmsMenuCriteria andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public CmsMenuCriteria andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuCriteria andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuCriteria andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public CmsMenuCriteria andTargetIsNull() {
		isnull("target");
		return this;
	}
	
	public CmsMenuCriteria andTargetIsNotNull() {
		notNull("target");
		return this;
	}
	
	public CmsMenuCriteria andTargetIsEmpty() {
		empty("target");
		return this;
	}

	public CmsMenuCriteria andTargetIsNotEmpty() {
		notEmpty("target");
		return this;
	}
        public CmsMenuCriteria andTargetLike(java.lang.String value) {
    	   addCriterion("target", value, ConditionMode.FUZZY, "target", "java.lang.String", "String");
    	   return this;
      }

      public CmsMenuCriteria andTargetNotLike(java.lang.String value) {
          addCriterion("target", value, ConditionMode.NOT_FUZZY, "target", "java.lang.String", "String");
          return this;
      }
      public CmsMenuCriteria andTargetEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetNotEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.NOT_EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetGreaterThan(java.lang.String value) {
          addCriterion("target", value, ConditionMode.GREATER_THEN, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.GREATER_EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetLessThan(java.lang.String value) {
          addCriterion("target", value, ConditionMode.LESS_THEN, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetLessThanOrEqualTo(java.lang.String value) {
          addCriterion("target", value, ConditionMode.LESS_EQUAL, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("target", value1, value2, ConditionMode.BETWEEN, "target", "java.lang.String", "String");
    	  return this;
      }

      public CmsMenuCriteria andTargetNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("target", value1, value2, ConditionMode.NOT_BETWEEN, "target", "java.lang.String", "String");
          return this;
      }
        
      public CmsMenuCriteria andTargetIn(List<java.lang.String> values) {
          addCriterion("target", values, ConditionMode.IN, "target", "java.lang.String", "String");
          return this;
      }

      public CmsMenuCriteria andTargetNotIn(List<java.lang.String> values) {
          addCriterion("target", values, ConditionMode.NOT_IN, "target", "java.lang.String", "String");
          return this;
      }
	public CmsMenuCriteria andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsMenuCriteria andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsMenuCriteria andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsMenuCriteria andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
       public CmsMenuCriteria andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsMenuCriteria andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsMenuCriteria andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsMenuCriteria andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}