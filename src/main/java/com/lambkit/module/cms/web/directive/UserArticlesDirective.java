package com.lambkit.module.cms.web.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.upms.rpc.model.UpmsUser;
import com.lambkit.web.LambkitControllerContext;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

import java.util.List;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms.web.directive
 */
@JFinalDirective("userArticles")
public class UserArticlesDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        Long userId = getParaToLong("userId", scope);
        UpmsUser user = LambkitControllerContext.get().getAttr("user");

        if (userId == null && user == null) {
            throw new RuntimeException("#userArticles() args is error,userId must not be null." + getLocation());
        }

        if (userId == null) userId = user.getUserId();

        String orderBy = getPara("orderBy", scope, "id desc");
        int status = getParaToInt("status", scope, CmsArticle.STATUS_NORMAL);
        int count = getParaToInt("count", scope, 10);


        Columns columns = Columns.create("user_id", userId);
        columns.add("status", status);

        List<CmsArticle> articles = CmsArticle.service().findListByColumns(columns, orderBy, count);

        if (articles == null || articles.isEmpty()) {
            return;
        }

        scope.setLocal("articles", articles);
        renderBody(env, scope, writer);
    }


    @Override
    public boolean hasEnd() {
        return true;
    }
}
