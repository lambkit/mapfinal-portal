package com.lambkit.module.cms.web.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.module.cms.rpc.model.CmsComment;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

import java.util.List;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms.web.directive
 */
@JFinalDirective("comments")
public class CommentsDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        String orderBy = getPara("orderBy", scope, "id desc");
        int count = getParaToInt("count", scope, 10);

        Columns columns = Columns.create("status", CmsComment.STATUS_NORMAL);
        List<CmsComment> comments = CmsComment.service().findListByColumns(columns, orderBy, count);

        if (comments == null || comments.isEmpty()) {
            return;
        }

        scope.setLocal("comments", comments);
        renderBody(env, scope, writer);
    }


    @Override
    public boolean hasEnd() {
        return true;
    }
}
