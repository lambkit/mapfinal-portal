package com.mapfinal;

import com.lambkit.common.model.LambkitModel;

/**
 * 图层中某个图元的管理和操作
 * @author yangyong
 */
public class GeoModel<M extends GeoModel<M>> extends LambkitModel<M> {

	private static final long serialVersionUID = -7930505891593743217L;

	//属性model功能
	
	//空间数据的修改、运算
	
	//空间类型 point line polygon
	//getGeoType
	
	//空间投影
	//getGeoProj
	
	//srid
	//getGeoSrid
	//setGeoSrid
	
	//--------------------------
	//添加几何字段
	//AddGeometryColumn
	//删除几何字段
	//DropGeometryColumn
	//检查数据库几何字段并在geometry_columns中归档 Probe_Geometry_Columns() 
	
	//-------------------------
	//单一
	
	//
}
