/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.web.directive;

import com.mapfinal.gis.model.GisLayer;
import com.mapfinal.gis.service.GisLayerService;
import com.lambkit.common.util.StringUtils;
import com.lambkit.web.directive.LambkitDirective;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-27
 * @version 1.0
 * @since 1.0
 */
/**
 * gis_layer标签<br>
 * 参数：{id:主键}
 * 返回值：{entity:gis_layer信息}
 * @author lambkit
 */
public class GisLayerDirective extends LambkitDirective {

	@Override
	public void onRender(Env env, Scope scope, Writer writer) {
		// TODO Auto-generated method stub
		String layerkey = getPara("layerkey", scope);
		String name = getPara("name", scope);
		String title = getPara("title", scope);
		String groups = getPara("groups", scope);
		String dataType = getPara("data_type", scope);
		String status = getPara("status", scope);
		String thumbnail = getPara("thumbnail", scope);
		int pagenum = getPara("pagenum", scope, 0);
		int pagesize = getPara("pagesize", scope, 0);
		String wheresql = getPara("sql", scope);
		String sql = " from gis_layer where "; 
		if(wheresql == null) {
			sql += " 1=1 ";
			if(StringUtils.hasText(layerkey)) sql += " and layerkey like '%" + layerkey + "%'";//varchar
			if(StringUtils.hasText(name)) sql += " and name like '%" + name + "%'";//varchar
			if(StringUtils.hasText(title)) sql += " and title like '%" + title + "%'";//varchar
			if(StringUtils.hasText(groups)) sql += " and groups like '%" + groups + "%'";//varchar
			if(StringUtils.hasText(dataType)) sql += " and data_type like '%" + dataType + "%'";//varchar
			if(StringUtils.hasText(status)) sql += " and status like '%" + status + "%'";//varchar
			if(StringUtils.hasText(thumbnail)) sql += " and thumbnail like '%" + thumbnail + "%'";//varchar
		} else {
			sql += wheresql;
		}
		String orderby = getPara("orderby", scope);
		if(StrKit.notBlank(orderby)) {
			sql += " order by " + orderby;
		}
		GisLayerService service = GisLayer.service();
		String tagEntityKeyname = getPara("key", scope, "entity");
		if(pagenum==0) {
			scope.setLocal(tagEntityKeyname, service.dao().findFirst("select *" + sql));
		} else {
			if(pagesize==0) {
				scope.setLocal(tagEntityKeyname, service.dao().find("select *" + sql));
			} else {
				scope.setLocal(tagEntityKeyname, service.dao().paginate(pagenum, pagesize, "select *", sql));
			}
		}
		renderBody(env, scope, writer);
	}
}
