package com.lambkit.module.cms.core.layer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Title: 用于分层排序 或 用于进行树分层
 * @Package com.lambkit.module.cms.core.layer
 */
public class SortKit {

    public static <M extends SortModel> void fillParentAndChild(List<M> models) {
        if (models == null || models.size() == 0) {
            return;
        }
        for (M child : models) {
            M parent = getParent(models, child);
            if (parent != null) {
                child.setParent(parent);
                if (parent.getChilds() == null || !parent.getChilds().contains(child)) {
                    parent.addChild(child);
                }
            }
        }
    }

    private static <M extends SortModel> M getParent(List<M> models, M child) {
        for (M m : models) {
            if (child.getParentId() != null && child.getParentId().equals(m.getId())) {
                return m;
            }
        }
        return null;
    }

    public static <M extends SortModel> void toLayer(List<M> models) {
        if (models == null || models.isEmpty()) {
            return;
        }
        toTree(models);
        List<M> layerModelList = new ArrayList<>();
        treeToLayer(models, layerModelList);
        models.clear();
        models.addAll(layerModelList);
    }

    private static <M extends SortModel> void treeToLayer(List<M> treeList, List<M> layerList) {
        if (treeList == null || treeList.isEmpty()) {
            return;
        }

        for (M model : treeList) {
            layerList.add(model);
            treeToLayer(model.getChilds(), layerList);
        }
    }


    public static <M extends SortModel> void toTree(List<M> models) {
        if (models == null || models.isEmpty()) {
            return;
        }
        List<M> temp = new ArrayList<>(models);
        models.clear();
        for (M model : temp) {
            if (model.isTop()) {
                model.setLayerNumber(0);
                fillChild(model, temp);
                models.add(model);
            }
        }
    }

    private static <M extends SortModel> void fillChild(M parent, List<M> models) {
        for (M model : models) {
            if (parent.getId().equals(model.getParentId())) {
                model.setParent(parent);
                model.setLayerNumber(parent.getLayerNumber() + 1);
                if (parent.getChilds() == null || !parent.getChilds().contains(model)) {
                    parent.addChild(model);
                }
                fillChild(model, models);
            }
        }
    }
}
