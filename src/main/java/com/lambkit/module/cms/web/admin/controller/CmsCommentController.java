package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsComment;
import com.lambkit.module.cms.web.validator.CmsCommentValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;

/**
 * 评论控制器
 */
//@Controller
@Api(tag = "评论管理", description = "评论管理")
//@RequestMapping("/manage/comment")
public class CmsCommentController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsCommentController.class);
	
	@ApiOperation(tag = "评论首页")
	@RequiresPermissions("cms:comment:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/comment/index.html");
	}

	@ApiOperation(tag = "评论列表")
	@RequiresPermissions("cms:comment:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsCommentExample = CmsComment.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsCommentExample.setOrderBy(sort + " " + order);
		}
		Page<CmsComment> comments = CmsComment.service().paginate(cmsCommentExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, comments));
	}

	@ApiOperation(tag = "新增评论")
	@RequiresPermissions("cms:comment:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsCommentValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/comment/create.html");
		} else {
			CmsComment cmsComment = getModel(CmsComment.class, "comment");
			long time = System.currentTimeMillis();
			cmsComment.setCtime(DateTimeUtils.parse(time));
			boolean flag = cmsComment.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除评论")
	@RequiresPermissions("cms:comment:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsComment.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改评论")
	@RequiresPermissions("cms:comment:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsCommentValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsComment comment = CmsComment.service().findById(id);
			setAttr("comment", comment);
			render(AdminManager.me().getTemplatePath() + "/cms/comment/update.html");
		} else {
			CmsComment cmsComment = getModel(CmsComment.class, "comment");
			boolean flag = false;
			if(cmsComment.getCommentId()!=null) {
				flag = cmsComment.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
}