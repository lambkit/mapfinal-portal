/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.service.impl;

import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.service.LambkitModelServiceImpl;
import com.lambkit.core.aop.AopKit;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.cms.rpc.api.CmsCommentService;
import com.lambkit.module.cms.rpc.model.CmsComment;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-22
 * @version 1.0
 * @since 1.0
 */
public class CmsCommentServiceImpl extends LambkitModelServiceImpl<CmsComment> implements CmsCommentService {
	
	private CmsComment DAO = null;
	
	public CmsComment dao() {
		if(DAO==null) {
			DAO = AopKit.singleton(CmsComment.class);
		}
		return DAO;
	}

	@Override
	public Page<CmsComment> paginateByArticleIdInNormal(Integer pageNumber, Integer pageSize, Long articleId) {
		Example example = CmsComment.sql().andArticleIdEqualTo(articleId).example();
		return paginate(pageNumber, pageSize, example);
	}
}
