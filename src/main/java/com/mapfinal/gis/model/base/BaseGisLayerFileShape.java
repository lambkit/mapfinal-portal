/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseGisLayerFileShape<M extends BaseGisLayerFileShape<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "gis_layer_file_shape";
	}
    
	public java.lang.String getLayerkey() {
		return this.get("layerkey");
	}

	public void setLayerkey(java.lang.String layerkey) {
		this.set("layerkey", layerkey);
	}
	public java.lang.Long getSid() {
		return this.get("sid");
	}

	public void setSid(java.lang.Long sid) {
		this.set("sid", sid);
	}
	public java.lang.Long getFileid() {
		return this.get("fileid");
	}

	public void setFileid(java.lang.Long fileid) {
		this.set("fileid", fileid);
	}
	public java.lang.String getName() {
		return this.get("name");
	}

	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getGeofld() {
		return this.get("geofld");
	}

	public void setGeofld(java.lang.String geofld) {
		this.set("geofld", geofld);
	}
	public java.lang.String getFldtype() {
		return this.get("fldtype");
	}

	public void setFldtype(java.lang.String fldtype) {
		this.set("fldtype", fldtype);
	}
	public java.lang.String getGeotype() {
		return this.get("geotype");
	}

	public void setGeotype(java.lang.String geotype) {
		this.set("geotype", geotype);
	}
	public java.lang.String getCrs() {
		return this.get("crs");
	}

	public void setCrs(java.lang.String crs) {
		this.set("crs", crs);
	}
}
