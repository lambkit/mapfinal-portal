package com.lambkit.module.cms;

import com.jfinal.config.Routes;
import com.lambkit.module.cms.web.admin.controller.CmsArticleController;
import com.lambkit.module.cms.web.admin.controller.CmsCategoryController;
import com.lambkit.module.cms.web.admin.controller.CmsCommentController;
import com.lambkit.module.cms.web.admin.controller.CmsMenuController;
import com.lambkit.module.cms.web.admin.controller.CmsPageController;
import com.lambkit.module.cms.web.admin.controller.CmsSettingController;
import com.lambkit.module.cms.web.admin.controller.CmsTagController;
import com.lambkit.module.cms.web.admin.controller.CmsTopicController;
import com.lambkit.module.cms.web.admin.controller.ManageController;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms
 */
public class CmsAdminRoutes extends Routes {

	@Override
	public void config() {
		String ckey = "/lambkit/cms";
		add(ckey, ManageController.class);
		add(ckey + "/article", CmsArticleController.class);
		add(ckey + "/category", CmsCategoryController.class);
		add(ckey + "/comment", CmsCommentController.class);
		add(ckey + "/menu", CmsMenuController.class);
		add(ckey + "/page", CmsPageController.class);
		add(ckey + "/setting", CmsSettingController.class);
		add(ckey + "/tag", CmsTagController.class);
		add(ckey + "/topic", CmsTopicController.class);
	}

}
