package com.mapfinal.server.local;

import com.jfinal.kit.StrKit;

//@PropertieConfig(prefix="mapfinal.local")
public class LocalConfig {
	public static final String NAME_DEFAULT = "main";
	
	private String name;
	private String version;
	private String url = "http://localhost:8080/mapfinal";
	private String user = "admin";
	private String password = "mapfinal";
	
	private Boolean config;

    public boolean isConfigOK() {
        if (config == null) {
        	if(StrKit.notBlank(name) && StrKit.notBlank(url)
        			 && StrKit.notBlank(user)
        			 && StrKit.notBlank(password)) {
        		config = true;
        	} else {
        		config = false;
        	}
        }
        return config;
    }
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
}
