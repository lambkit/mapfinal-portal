package com.lambkit.module.cms.core.template;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.lambkit.module.cms.common.CmsConfig;
import com.lambkit.module.cms.common.CmsOptions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TemplateManager {

	private String templatePath = "/templates";
    private Template currentTemplate;

    private static final TemplateManager me = new TemplateManager();

    private TemplateManager() {

    }


    public static TemplateManager me() {
        return me;
    }

    public void init() {
        String templateId = CmsOptions.get("web_template");
        TemplateManager.me().setCurrentTemplate(templateId);
    }

    public List<Template> getInstalledTemplates() {
        String basePath = PathKit.getWebRootPath() + templatePath;

        List<File> templateFolderList = new ArrayList<File>();
        scanTemplateFloders(new File(basePath), templateFolderList);

        List<Template> templatelist = null;
        if (templateFolderList.size() > 0) {
            templatelist = new ArrayList<>();
            for (File templateFolder : templateFolderList) {
                templatelist.add(new Template(templateFolder));
            }
        }
        return templatelist;
    }


    private void scanTemplateFloders(File file, List<File> list) {
        if (file.isDirectory()) {
            File configFile = new File(file, "template.txt");
            if (configFile.exists() && configFile.isFile()) {
                list.add(file);
            } else {
                File[] files = file.listFiles();
                if (null != files && files.length > 0) {
                    for (File f : files) {
                        if (f.isDirectory())
                            scanTemplateFloders(f, list);
                    }
                }
            }
        }
    }


    public Template getTemplateById(String id) {
        List<Template> templates = getInstalledTemplates();
        if (templates == null || templates.isEmpty()) {
            return null;
        }
        for (Template template : templates) {
            if (id.equals(template.getId())) return template;
        }
        return null;
    }

    public Template getCurrentTemplate() {
        return currentTemplate;
    }

    public void setCurrentTemplate(String templateId) {
        if (StrKit.isBlank(templateId)) {
            initDefaultTemplate();
            return;
        }

        Template template = getTemplateById(templateId);
        if (template == null) {
            LogKit.warn("can not find tempalte " + templateId);
            initDefaultTemplate();
        } else {
            setCurrentTemplate(template);
        }
    }


    private void initDefaultTemplate() {
        setCurrentTemplate(getTemplateById(CmsConfig.me().getWebTemplate()));
    }


    public void setCurrentTemplate(Template currentTemplate) {
        this.currentTemplate = currentTemplate;
        CmsOptions.set("web_template", currentTemplate.getId());
    }
    
    public String getCurrentWebPath() {
		return templatePath + "/" + getCurrentTemplate().getFolder();
	}
	
	public String getCurrentWebPath(String file) {
		return templatePath + "/" + getCurrentTemplate().getFolder() + "/" + file;
	}
	
	public String getCurrentFilePath(String file) {
		return PathKit.getWebRootPath() + templatePath + File.separator + getCurrentTemplate().getFolder() + File.separator + file;
	}
    
    public String getTemplatePath() {
		return templatePath;
	}
    
    public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

}