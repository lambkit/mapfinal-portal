package com.lambkit.module.cms.web.controller;

import com.lambkit.module.cms.core.template.TemplateManager;
import com.lambkit.module.cms.rpc.model.*;
import com.lambkit.web.controller.LambkitController;
import com.lambkit.db.sql.SqlJoinMode;
import com.lambkit.db.sql.column.Example;
import com.jfinal.log.Log;


import java.util.List;

/**
 * 首页控制器
 * Created by shuzheng on 2017/3/19.
 */
public class IndexController extends LambkitController {

    private static final Log LOG = Log.getLog(IndexController.class);

    //@RequestMapping(value = "", method = RequestMethod.GET)
    public void index() {
        // 所有系统
        Example cmsSystemExample = CmsSystem.sql().example().setOrderBy("orders asc");
        List<CmsSystem> systems = CmsSystem.service().find(cmsSystemExample);
        setAttr("systems", systems);
        // 所有类目
        Example cmsCategoryExample = CmsCategory.sql().example().setOrderBy("orders asc");
        cmsCategoryExample.join("system_id", "cms_system", "system_id", SqlJoinMode.LEFT_JOIN);
        cmsCategoryExample.setSelectSql("cms_category.*, cms_system.code, cms_system.name as sysname");
        List<CmsCategory> categories = CmsCategory.service().find(cmsCategoryExample);
        setAttr("categories", categories);
        // 所有标签
        Example cmsTagExample = CmsTag.sql().example().setOrderBy("orders asc");
        cmsTagExample.join("system_id", "cms_system", "system_id", SqlJoinMode.LEFT_JOIN);
        cmsTagExample.setSelectSql("cms_tag.*, cms_system.code, cms_system.name as sysname");
        List<CmsTag> tags = CmsTag.service().find(cmsTagExample);
        setAttr("tags", tags);
        render(TemplateManager.me().getCurrentWebPath() + "/index.html");
    }

}