/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisMapStyleCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisMapStyleCriteria create() {
		return new GisMapStyleCriteria();
	}
	
	public static GisMapStyleCriteria create(Column column) {
		GisMapStyleCriteria that = new GisMapStyleCriteria();
		that.add(column);
        return that;
    }

    public static GisMapStyleCriteria create(String name, Object value) {
        return (GisMapStyleCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_map_style", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapStyleCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapStyleCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisMapStyleCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisMapStyleCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapStyleCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapStyleCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapStyleCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapStyleCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisMapStyleCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisMapStyleCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisMapStyleCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisMapStyleCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisMapStyleCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisMapStyleCriteria andMapIdIsNull() {
		isnull("map_id");
		return this;
	}
	
	public GisMapStyleCriteria andMapIdIsNotNull() {
		notNull("map_id");
		return this;
	}
	
	public GisMapStyleCriteria andMapIdIsEmpty() {
		empty("map_id");
		return this;
	}

	public GisMapStyleCriteria andMapIdIsNotEmpty() {
		notEmpty("map_id");
		return this;
	}
       public GisMapStyleCriteria andMapIdEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdNotEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.NOT_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdGreaterThan(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.GREATER_THEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.GREATER_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdLessThan(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.LESS_THEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.LESS_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("map_id", value1, value2, ConditionMode.BETWEEN, "mapId", "java.lang.Integer", "Float");
    	  return this;
      }

      public GisMapStyleCriteria andMapIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("map_id", value1, value2, ConditionMode.NOT_BETWEEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }
        
      public GisMapStyleCriteria andMapIdIn(List<java.lang.Integer> values) {
          addCriterion("map_id", values, ConditionMode.IN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andMapIdNotIn(List<java.lang.Integer> values) {
          addCriterion("map_id", values, ConditionMode.NOT_IN, "mapId", "java.lang.Integer", "Float");
          return this;
      }
	public GisMapStyleCriteria andStyleIdIsNull() {
		isnull("style_id");
		return this;
	}
	
	public GisMapStyleCriteria andStyleIdIsNotNull() {
		notNull("style_id");
		return this;
	}
	
	public GisMapStyleCriteria andStyleIdIsEmpty() {
		empty("style_id");
		return this;
	}

	public GisMapStyleCriteria andStyleIdIsNotEmpty() {
		notEmpty("style_id");
		return this;
	}
       public GisMapStyleCriteria andStyleIdEqualTo(java.lang.Integer value) {
          addCriterion("style_id", value, ConditionMode.EQUAL, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdNotEqualTo(java.lang.Integer value) {
          addCriterion("style_id", value, ConditionMode.NOT_EQUAL, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdGreaterThan(java.lang.Integer value) {
          addCriterion("style_id", value, ConditionMode.GREATER_THEN, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("style_id", value, ConditionMode.GREATER_EQUAL, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdLessThan(java.lang.Integer value) {
          addCriterion("style_id", value, ConditionMode.LESS_THEN, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("style_id", value, ConditionMode.LESS_EQUAL, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("style_id", value1, value2, ConditionMode.BETWEEN, "styleId", "java.lang.Integer", "Float");
    	  return this;
      }

      public GisMapStyleCriteria andStyleIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("style_id", value1, value2, ConditionMode.NOT_BETWEEN, "styleId", "java.lang.Integer", "Float");
          return this;
      }
        
      public GisMapStyleCriteria andStyleIdIn(List<java.lang.Integer> values) {
          addCriterion("style_id", values, ConditionMode.IN, "styleId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapStyleCriteria andStyleIdNotIn(List<java.lang.Integer> values) {
          addCriterion("style_id", values, ConditionMode.NOT_IN, "styleId", "java.lang.Integer", "Float");
          return this;
      }
}