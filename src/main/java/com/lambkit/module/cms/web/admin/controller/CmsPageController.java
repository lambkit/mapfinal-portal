package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsPage;
import com.lambkit.module.cms.web.validator.CmsPageValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;

/**
 * 单页控制器
 */
//@Controller
@Api(tag = "单页管理", description = "单页管理")
//@RequestMapping("/manage/page")
public class CmsPageController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsPageController.class);
	
	@ApiOperation(tag = "评论首页")
	@RequiresPermissions("cms:page:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/page/index.html");
	}

	@ApiOperation(tag = "评论列表")
	@RequiresPermissions("cms:page:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsPageExample = CmsPage.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsPageExample.setOrderBy(sort + " " + order);
		}
		Page<CmsPage> pages = CmsPage.service().paginate(cmsPageExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, pages));
		//result.put("rows", rows);
		//result.put("total", total);
	}

	@ApiOperation(tag = "新增单页")
	@RequiresPermissions("cms:page:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsPageValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/page/create.html");
		} else {
			CmsPage cmsPage = getModel(CmsPage.class, "page");
			long time = System.currentTimeMillis();
			cmsPage.setCtime(DateTimeUtils.parse(time));
			cmsPage.setOrders(time);
			boolean flag = cmsPage.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除单页")
	@RequiresPermissions("cms:page:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsPage.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改单页")
	@RequiresPermissions("cms:page:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsPageValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsPage page = CmsPage.service().findById(id);
			setAttr("page", page);
			render(AdminManager.me().getTemplatePath() + "/cms/page/update.html");
		} else {
			CmsPage cmsPage = getModel(CmsPage.class, "page");
			boolean flag = false;
			if(cmsPage.getPageId()!=null) {
				flag = cmsPage.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

}