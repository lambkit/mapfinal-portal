/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisMapLayerCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisMapLayerCriteria create() {
		return new GisMapLayerCriteria();
	}
	
	public static GisMapLayerCriteria create(Column column) {
		GisMapLayerCriteria that = new GisMapLayerCriteria();
		that.add(column);
        return that;
    }

    public static GisMapLayerCriteria create(String name, Object value) {
        return (GisMapLayerCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_map_layer", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapLayerCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapLayerCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisMapLayerCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisMapLayerCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapLayerCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapLayerCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapLayerCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisMapLayerCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisMapLayerCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisMapLayerCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisMapLayerCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisMapLayerCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisMapLayerCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisMapLayerCriteria andMapIdIsNull() {
		isnull("map_id");
		return this;
	}
	
	public GisMapLayerCriteria andMapIdIsNotNull() {
		notNull("map_id");
		return this;
	}
	
	public GisMapLayerCriteria andMapIdIsEmpty() {
		empty("map_id");
		return this;
	}

	public GisMapLayerCriteria andMapIdIsNotEmpty() {
		notEmpty("map_id");
		return this;
	}
       public GisMapLayerCriteria andMapIdEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdNotEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.NOT_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdGreaterThan(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.GREATER_THEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.GREATER_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdLessThan(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.LESS_THEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("map_id", value, ConditionMode.LESS_EQUAL, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("map_id", value1, value2, ConditionMode.BETWEEN, "mapId", "java.lang.Integer", "Float");
    	  return this;
      }

      public GisMapLayerCriteria andMapIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("map_id", value1, value2, ConditionMode.NOT_BETWEEN, "mapId", "java.lang.Integer", "Float");
          return this;
      }
        
      public GisMapLayerCriteria andMapIdIn(List<java.lang.Integer> values) {
          addCriterion("map_id", values, ConditionMode.IN, "mapId", "java.lang.Integer", "Float");
          return this;
      }

      public GisMapLayerCriteria andMapIdNotIn(List<java.lang.Integer> values) {
          addCriterion("map_id", values, ConditionMode.NOT_IN, "mapId", "java.lang.Integer", "Float");
          return this;
      }
	public GisMapLayerCriteria andLayerKeyIsNull() {
		isnull("layer_key");
		return this;
	}
	
	public GisMapLayerCriteria andLayerKeyIsNotNull() {
		notNull("layer_key");
		return this;
	}
	
	public GisMapLayerCriteria andLayerKeyIsEmpty() {
		empty("layer_key");
		return this;
	}

	public GisMapLayerCriteria andLayerKeyIsNotEmpty() {
		notEmpty("layer_key");
		return this;
	}
        public GisMapLayerCriteria andLayerKeyLike(java.lang.String value) {
    	   addCriterion("layer_key", value, ConditionMode.FUZZY, "layerKey", "java.lang.String", "Float");
    	   return this;
      }

      public GisMapLayerCriteria andLayerKeyNotLike(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.NOT_FUZZY, "layerKey", "java.lang.String", "Float");
          return this;
      }
      public GisMapLayerCriteria andLayerKeyEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyNotEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.NOT_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyGreaterThan(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.GREATER_THEN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.GREATER_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyLessThan(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.LESS_THEN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.LESS_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layer_key", value1, value2, ConditionMode.BETWEEN, "layerKey", "java.lang.String", "String");
    	  return this;
      }

      public GisMapLayerCriteria andLayerKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layer_key", value1, value2, ConditionMode.NOT_BETWEEN, "layerKey", "java.lang.String", "String");
          return this;
      }
        
      public GisMapLayerCriteria andLayerKeyIn(List<java.lang.String> values) {
          addCriterion("layer_key", values, ConditionMode.IN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisMapLayerCriteria andLayerKeyNotIn(List<java.lang.String> values) {
          addCriterion("layer_key", values, ConditionMode.NOT_IN, "layerKey", "java.lang.String", "String");
          return this;
      }
}