/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model;

import com.jfinal.kit.StrKit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;
import com.lambkit.module.cms.common.CmsConfig;
import com.lambkit.Lambkit;

import com.lambkit.module.cms.rpc.model.base.BaseCmsTopic;
import com.lambkit.module.cms.rpc.model.sql.CmsTopicCriteria;
import com.lambkit.module.cms.rpc.api.CmsTopicService;
import com.lambkit.module.cms.rpc.service.impl.CmsTopicServiceImpl;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-28
 * @version 1.0
 * @since 1.0
 */
public class CmsTopic extends BaseCmsTopic<CmsTopic> {

	private static final long serialVersionUID = 1L;
	
	public static CmsTopicService service() {
		return ServiceKit.inject(CmsTopicService.class, CmsTopicServiceImpl.class);
	}
	
	public static CmsTopicCriteria sql() {
		return new CmsTopicCriteria();
	}
	
	public static CmsTopicCriteria sql(Column column) {
		CmsTopicCriteria that = new CmsTopicCriteria();
		that.add(column);
        return that;
    }
	
	public CmsTopic() {
		CmsConfig config = Lambkit.config(CmsConfig.class);
		String dbconfig = config.getDbconfig();
		if(StrKit.notBlank(dbconfig)) {
			this.use(dbconfig);
		}
	}
}
