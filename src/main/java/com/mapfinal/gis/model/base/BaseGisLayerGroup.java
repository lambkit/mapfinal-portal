/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.base;

import com.jfinal.plugin.activerecord.IBean;

import com.lambkit.common.model.LambkitModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings("serial")
public abstract class BaseGisLayerGroup<M extends BaseGisLayerGroup<M>> extends LambkitModel<M> implements IBean {

	public String getTableName() {
		return "gis_layer_group";
	}
    
	public java.lang.String getLayerGroupKey() {
		return this.get("layer_group_key");
	}

	public void setLayerGroupKey(java.lang.String layerGroupKey) {
		this.set("layer_group_key", layerGroupKey);
	}
	public java.lang.String getLayerKey() {
		return this.get("layer_key");
	}

	public void setLayerKey(java.lang.String layerKey) {
		this.set("layer_key", layerKey);
	}
	public java.lang.String getOrds() {
		return this.get("ords");
	}

	public void setOrds(java.lang.String ords) {
		this.set("ords", ords);
	}
}
