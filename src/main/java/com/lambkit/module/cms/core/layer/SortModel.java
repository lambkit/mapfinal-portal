package com.lambkit.module.cms.core.layer;

import java.util.List;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Package com.lambkit.module.cms.core.layer
 */
public interface SortModel<M extends SortModel> {

    public boolean isTop();

    public Long getId();

    public Long getParentId();

    public void setParent(M parent);

    public M getParent();

    public void setChilds(List<M> childs);

    public void addChild(M child);

    public List<M> getChilds();

    public void setLayerNumber(int layerNumber);

    public int getLayerNumber();


}
