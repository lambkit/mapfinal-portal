package com.lambkit.module.cms.common;

import com.lambkit.common.LambkitConsts;

/**
 * cms系统常量类
 * Created by shuzheng on 2017/2/19.
 */
public class CmsConsts extends LambkitConsts {
	
	public static final String VERSION = "v1.0.0";
	
	public static final String OPTION_WEB_FAKE_STATIC_ENABLE = "web_fake_static_enable"; //是否启用伪静态
    public static final String OPTION_WEB_FAKE_STATIC_SUFFIX = "web_fake_static_suffix"; //网站伪静态后缀

	public static final String OPTION_CDN_ENABLE = "cdn_enable"; //是否启用CDN
    public static final String OPTION_CDN_DOMAIN = "cdn_domain"; //CDN域名
    
    public static final String EDIT_MODE_HTML = "html"; //html 的编辑模式
    public static final String EDIT_MODE_MARKDOWN = "markdown"; //markdown 的编辑模式

}
