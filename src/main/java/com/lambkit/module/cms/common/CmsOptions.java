package com.lambkit.module.cms.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.lambkit.module.cms.rpc.model.CmsSetting;

public class CmsOptions {

	private static final Log LOG = Log.getLog(CmsConsts.class);
	
	private static OptionStore store = new OptionStore() {
		private final Map<String, String> cache = new ConcurrentHashMap<>();

		@Override
		public String get(String key) {
			String value = cache.get(key);
			if(StrKit.isBlank(value)) {
				value = CmsSetting.service().get(key);
			}
			return value;
		}

		@Override
		public void put(String key, String value) {
			if (StrKit.isBlank(value)) {
				remove(key);
			} else {
				cache.put(key, value);
				CmsSetting.service().put(key, value);
			}
		}

		@Override
		public void remove(String key) {
			cache.remove(key);
			CmsSetting.service().remove(key);
		}
	};

	private static List<OptionChangeListener> LISTENERS = new ArrayList<>();

	public static void set(String key, String value) {
		if (StrKit.isBlank(key)) {
			return;
		}

		String oldValue = store.get(key);
		if (Objects.equals(value, oldValue)) {
			return;
		}

		store.put(key, value);

		for (OptionChangeListener listener : LISTENERS) {
			try {
				listener.onChanged(key, value, oldValue);
			} catch (Throwable ex) {
				LOG.error(ex.toString(), ex);
			}
		}

		doFinishedChanged(key, value, oldValue);
	}

	public static String get(String key) {
		return store.get(key);
	}

	public static String get(String key, String defaultvalue) {
		String v = get(key);
		return StrKit.isBlank(v) ? defaultvalue : v;
	}

	public static boolean getAsBool(String key) {
		return Boolean.parseBoolean(store.get(key));
	}

	@Deprecated
	public static boolean isTrueOrNull(String key) {
		return isTrueOrEmpty(key);
	}

	public static boolean isTrueOrEmpty(String key) {
		String value = get(key);
		return StrKit.isBlank(value) || "true".equals(value);
	}

	public static int getAsInt(String key, int defaultValue) {
		String value = get(key);
		if (StrKit.isBlank(value)) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(value);
		} catch (Exception ex) {
			LOG.warn(ex.toString(), ex);
			return defaultValue;
		}
	}

	public static float getAsFloat(String key, float defaultValue) {
		String value = get(key);
		if (StrKit.isBlank(value)) {
			return defaultValue;
		}
		try {
			return Float.parseFloat(value);
		} catch (Exception ex) {
			LOG.warn(ex.toString(), ex);
			return defaultValue;
		}
	}

	public static void addListener(OptionChangeListener listener) {
		LISTENERS.add(listener);
	}

	public static void removeListener(OptionChangeListener listener) {
		LISTENERS.remove(listener);
	}

	/*
	 * public static String getCDNDomain() { boolean cdnEnable =
	 * getAsBool(CmsConsts.OPTION_CDN_ENABLE); if (cdnEnable == false) { return
	 * null; }
	 * 
	 * String cdnDomain = get(CmsConsts.OPTION_CDN_DOMAIN); return
	 * StrKit.isBlank(cdnDomain) ? null : cdnDomain; }
	 * 
	 * public static String getResDomain() { String cdnDomain = getCDNDomain();
	 * return cdnDomain == null ? get(CmsConsts.OPTION_WEB_DOMAIN) : cdnDomain; }
	 */

	public static interface OptionChangeListener {
		public void onChanged(String key, String newValue, String oldValue);
	}

	private static final String indexStyleKey = "index_style";

	private static void doFinishedChanged(String key, String value, String oldValue) {
		if (indexStyleKey.equals(key)) {
			indexStyleValue = value;
		}

		// 伪静态的是否启用
		else if (CmsConsts.OPTION_WEB_FAKE_STATIC_ENABLE.equals(key)) {
			fakeStaticEnable = "true".equalsIgnoreCase(value);
		}

		// 伪静态后缀
		else if (CmsConsts.OPTION_WEB_FAKE_STATIC_SUFFIX.equals(key)) {
			fakeStaticSuffix = value;
		}
	}

	private static String indexStyleValue = null;

	public static String getIndexStyle() {
		return indexStyleValue;
	}

	private static boolean fakeStaticEnable = false;
	private static String fakeStaticSuffix = "";

	public static String getAppUrlSuffix() {
		return fakeStaticEnable ? (StrKit.isBlank(fakeStaticSuffix) ? "" : fakeStaticSuffix) : "";
	}

	public static OptionStore getStore() {
		return store;
	}

	public static void setStore(OptionStore store) {
		CmsOptions.store = store;
	}

	public static interface OptionStore {

		public String get(String key);

		public void put(String key, String value);

		public void remove(String key);

	}
}
