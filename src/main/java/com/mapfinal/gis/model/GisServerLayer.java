/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model;

import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;
import com.mapfinal.gis.GisConfig;
import com.mapfinal.gis.model.base.BaseGisServerLayer;
import com.mapfinal.gis.model.sql.GisServerLayerCriteria;
import com.mapfinal.gis.service.GisServerLayerService;
import com.mapfinal.gis.service.impl.GisServerLayerServiceImpl;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisServerLayer extends BaseGisServerLayer<GisServerLayer> {

	private static final long serialVersionUID = 1L;
	
	public static GisServerLayerService service() {
		return ServiceKit.inject(GisServerLayerService.class, GisServerLayerServiceImpl.class);
	}
	
	public static GisServerLayerCriteria sql() {
		return new GisServerLayerCriteria();
	}
	
	public static GisServerLayerCriteria sql(Column column) {
		GisServerLayerCriteria that = new GisServerLayerCriteria();
		that.add(column);
        return that;
    }

	public GisServerLayer() {
		GisConfig config = Lambkit.config(GisConfig.class);
		String dbconfig = config.getDbconfig();
		if(StrKit.notBlank(dbconfig)) {
			this.use(dbconfig);
		}
	}
}
