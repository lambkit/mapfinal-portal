/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisLayerTablePostgisCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisLayerTablePostgisCriteria create() {
		return new GisLayerTablePostgisCriteria();
	}
	
	public static GisLayerTablePostgisCriteria create(Column column) {
		GisLayerTablePostgisCriteria that = new GisLayerTablePostgisCriteria();
		that.add(column);
        return that;
    }

    public static GisLayerTablePostgisCriteria create(String name, Object value) {
        return (GisLayerTablePostgisCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_layer_table_postgis", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerTablePostgisCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerTablePostgisCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisLayerTablePostgisCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisLayerTablePostgisCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerTablePostgisCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerTablePostgisCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerTablePostgisCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerTablePostgisCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisLayerTablePostgisCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisLayerTablePostgisCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisLayerTablePostgisCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisLayerTablePostgisCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisLayerTablePostgisCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisLayerTablePostgisCriteria andLayerkeyIsNull() {
		isnull("layerkey");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andLayerkeyIsNotNull() {
		notNull("layerkey");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andLayerkeyIsEmpty() {
		empty("layerkey");
		return this;
	}

	public GisLayerTablePostgisCriteria andLayerkeyIsNotEmpty() {
		notEmpty("layerkey");
		return this;
	}
        public GisLayerTablePostgisCriteria andLayerkeyLike(java.lang.String value) {
    	   addCriterion("layerkey", value, ConditionMode.FUZZY, "layerkey", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyNotLike(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_FUZZY, "layerkey", "java.lang.String", "String");
          return this;
      }
      public GisLayerTablePostgisCriteria andLayerkeyEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyNotEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyGreaterThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyLessThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layerkey", value1, value2, ConditionMode.BETWEEN, "layerkey", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layerkey", value1, value2, ConditionMode.NOT_BETWEEN, "layerkey", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andLayerkeyIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.IN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andLayerkeyNotIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.NOT_IN, "layerkey", "java.lang.String", "String");
          return this;
      }
	public GisLayerTablePostgisCriteria andSidIsNull() {
		isnull("sid");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andSidIsNotNull() {
		notNull("sid");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andSidIsEmpty() {
		empty("sid");
		return this;
	}

	public GisLayerTablePostgisCriteria andSidIsNotEmpty() {
		notEmpty("sid");
		return this;
	}
       public GisLayerTablePostgisCriteria andSidEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidNotEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.NOT_EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidGreaterThan(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.GREATER_THEN, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.GREATER_EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidLessThan(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.LESS_THEN, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("sid", value, ConditionMode.LESS_EQUAL, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("sid", value1, value2, ConditionMode.BETWEEN, "sid", "java.lang.Long", "Float");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andSidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("sid", value1, value2, ConditionMode.NOT_BETWEEN, "sid", "java.lang.Long", "Float");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andSidIn(List<java.lang.Long> values) {
          addCriterion("sid", values, ConditionMode.IN, "sid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andSidNotIn(List<java.lang.Long> values) {
          addCriterion("sid", values, ConditionMode.NOT_IN, "sid", "java.lang.Long", "Float");
          return this;
      }
	public GisLayerTablePostgisCriteria andTbidIsNull() {
		isnull("tbid");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andTbidIsNotNull() {
		notNull("tbid");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andTbidIsEmpty() {
		empty("tbid");
		return this;
	}

	public GisLayerTablePostgisCriteria andTbidIsNotEmpty() {
		notEmpty("tbid");
		return this;
	}
       public GisLayerTablePostgisCriteria andTbidEqualTo(java.lang.Long value) {
          addCriterion("tbid", value, ConditionMode.EQUAL, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidNotEqualTo(java.lang.Long value) {
          addCriterion("tbid", value, ConditionMode.NOT_EQUAL, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidGreaterThan(java.lang.Long value) {
          addCriterion("tbid", value, ConditionMode.GREATER_THEN, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("tbid", value, ConditionMode.GREATER_EQUAL, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidLessThan(java.lang.Long value) {
          addCriterion("tbid", value, ConditionMode.LESS_THEN, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("tbid", value, ConditionMode.LESS_EQUAL, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("tbid", value1, value2, ConditionMode.BETWEEN, "tbid", "java.lang.Long", "Float");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andTbidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("tbid", value1, value2, ConditionMode.NOT_BETWEEN, "tbid", "java.lang.Long", "Float");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andTbidIn(List<java.lang.Long> values) {
          addCriterion("tbid", values, ConditionMode.IN, "tbid", "java.lang.Long", "Float");
          return this;
      }

      public GisLayerTablePostgisCriteria andTbidNotIn(List<java.lang.Long> values) {
          addCriterion("tbid", values, ConditionMode.NOT_IN, "tbid", "java.lang.Long", "Float");
          return this;
      }
	public GisLayerTablePostgisCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisLayerTablePostgisCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisLayerTablePostgisCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public GisLayerTablePostgisCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public GisLayerTablePostgisCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisLayerTablePostgisCriteria andGeofldIsNull() {
		isnull("geofld");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andGeofldIsNotNull() {
		notNull("geofld");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andGeofldIsEmpty() {
		empty("geofld");
		return this;
	}

	public GisLayerTablePostgisCriteria andGeofldIsNotEmpty() {
		notEmpty("geofld");
		return this;
	}
        public GisLayerTablePostgisCriteria andGeofldLike(java.lang.String value) {
    	   addCriterion("geofld", value, ConditionMode.FUZZY, "geofld", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerTablePostgisCriteria andGeofldNotLike(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.NOT_FUZZY, "geofld", "java.lang.String", "String");
          return this;
      }
      public GisLayerTablePostgisCriteria andGeofldEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldNotEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.NOT_EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldGreaterThan(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.GREATER_THEN, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.GREATER_EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldLessThan(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.LESS_THEN, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldLessThanOrEqualTo(java.lang.String value) {
          addCriterion("geofld", value, ConditionMode.LESS_EQUAL, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("geofld", value1, value2, ConditionMode.BETWEEN, "geofld", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andGeofldNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("geofld", value1, value2, ConditionMode.NOT_BETWEEN, "geofld", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andGeofldIn(List<java.lang.String> values) {
          addCriterion("geofld", values, ConditionMode.IN, "geofld", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeofldNotIn(List<java.lang.String> values) {
          addCriterion("geofld", values, ConditionMode.NOT_IN, "geofld", "java.lang.String", "String");
          return this;
      }
	public GisLayerTablePostgisCriteria andFldtypeIsNull() {
		isnull("fldtype");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andFldtypeIsNotNull() {
		notNull("fldtype");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andFldtypeIsEmpty() {
		empty("fldtype");
		return this;
	}

	public GisLayerTablePostgisCriteria andFldtypeIsNotEmpty() {
		notEmpty("fldtype");
		return this;
	}
        public GisLayerTablePostgisCriteria andFldtypeLike(java.lang.String value) {
    	   addCriterion("fldtype", value, ConditionMode.FUZZY, "fldtype", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeNotLike(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.NOT_FUZZY, "fldtype", "java.lang.String", "String");
          return this;
      }
      public GisLayerTablePostgisCriteria andFldtypeEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeNotEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.NOT_EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeGreaterThan(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.GREATER_THEN, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.GREATER_EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeLessThan(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.LESS_THEN, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fldtype", value, ConditionMode.LESS_EQUAL, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fldtype", value1, value2, ConditionMode.BETWEEN, "fldtype", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fldtype", value1, value2, ConditionMode.NOT_BETWEEN, "fldtype", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andFldtypeIn(List<java.lang.String> values) {
          addCriterion("fldtype", values, ConditionMode.IN, "fldtype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andFldtypeNotIn(List<java.lang.String> values) {
          addCriterion("fldtype", values, ConditionMode.NOT_IN, "fldtype", "java.lang.String", "String");
          return this;
      }
	public GisLayerTablePostgisCriteria andGeotypeIsNull() {
		isnull("geotype");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andGeotypeIsNotNull() {
		notNull("geotype");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andGeotypeIsEmpty() {
		empty("geotype");
		return this;
	}

	public GisLayerTablePostgisCriteria andGeotypeIsNotEmpty() {
		notEmpty("geotype");
		return this;
	}
        public GisLayerTablePostgisCriteria andGeotypeLike(java.lang.String value) {
    	   addCriterion("geotype", value, ConditionMode.FUZZY, "geotype", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeNotLike(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.NOT_FUZZY, "geotype", "java.lang.String", "String");
          return this;
      }
      public GisLayerTablePostgisCriteria andGeotypeEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeNotEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.NOT_EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeGreaterThan(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.GREATER_THEN, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.GREATER_EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeLessThan(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.LESS_THEN, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("geotype", value, ConditionMode.LESS_EQUAL, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("geotype", value1, value2, ConditionMode.BETWEEN, "geotype", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("geotype", value1, value2, ConditionMode.NOT_BETWEEN, "geotype", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andGeotypeIn(List<java.lang.String> values) {
          addCriterion("geotype", values, ConditionMode.IN, "geotype", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andGeotypeNotIn(List<java.lang.String> values) {
          addCriterion("geotype", values, ConditionMode.NOT_IN, "geotype", "java.lang.String", "String");
          return this;
      }
	public GisLayerTablePostgisCriteria andCrsIsNull() {
		isnull("crs");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andCrsIsNotNull() {
		notNull("crs");
		return this;
	}
	
	public GisLayerTablePostgisCriteria andCrsIsEmpty() {
		empty("crs");
		return this;
	}

	public GisLayerTablePostgisCriteria andCrsIsNotEmpty() {
		notEmpty("crs");
		return this;
	}
        public GisLayerTablePostgisCriteria andCrsLike(java.lang.String value) {
    	   addCriterion("crs", value, ConditionMode.FUZZY, "crs", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerTablePostgisCriteria andCrsNotLike(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.NOT_FUZZY, "crs", "java.lang.String", "String");
          return this;
      }
      public GisLayerTablePostgisCriteria andCrsEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsNotEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.NOT_EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsGreaterThan(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.GREATER_THEN, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.GREATER_EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsLessThan(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.LESS_THEN, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("crs", value, ConditionMode.LESS_EQUAL, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("crs", value1, value2, ConditionMode.BETWEEN, "crs", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerTablePostgisCriteria andCrsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("crs", value1, value2, ConditionMode.NOT_BETWEEN, "crs", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerTablePostgisCriteria andCrsIn(List<java.lang.String> values) {
          addCriterion("crs", values, ConditionMode.IN, "crs", "java.lang.String", "String");
          return this;
      }

      public GisLayerTablePostgisCriteria andCrsNotIn(List<java.lang.String> values) {
          addCriterion("crs", values, ConditionMode.NOT_IN, "crs", "java.lang.String", "String");
          return this;
      }
}