/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisServerApiCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisServerApiCriteria create() {
		return new GisServerApiCriteria();
	}
	
	public static GisServerApiCriteria create(Column column) {
		GisServerApiCriteria that = new GisServerApiCriteria();
		that.add(column);
        return that;
    }

    public static GisServerApiCriteria create(String name, Object value) {
        return (GisServerApiCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_server_api", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerApiCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerApiCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisServerApiCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisServerApiCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerApiCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerApiCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerApiCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerApiCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisServerApiCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisServerApiCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisServerApiCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisServerApiCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisServerApiCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisServerApiCriteria andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public GisServerApiCriteria andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public GisServerApiCriteria andIdIsEmpty() {
		empty("id");
		return this;
	}

	public GisServerApiCriteria andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
       public GisServerApiCriteria andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public GisServerApiCriteria andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public GisServerApiCriteria andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public GisServerApiCriteria andServerkeyIsNull() {
		isnull("serverkey");
		return this;
	}
	
	public GisServerApiCriteria andServerkeyIsNotNull() {
		notNull("serverkey");
		return this;
	}
	
	public GisServerApiCriteria andServerkeyIsEmpty() {
		empty("serverkey");
		return this;
	}

	public GisServerApiCriteria andServerkeyIsNotEmpty() {
		notEmpty("serverkey");
		return this;
	}
        public GisServerApiCriteria andServerkeyLike(java.lang.String value) {
    	   addCriterion("serverkey", value, ConditionMode.FUZZY, "serverkey", "java.lang.String", "Float");
    	   return this;
      }

      public GisServerApiCriteria andServerkeyNotLike(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.NOT_FUZZY, "serverkey", "java.lang.String", "Float");
          return this;
      }
      public GisServerApiCriteria andServerkeyEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyNotEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.NOT_EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyGreaterThan(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.GREATER_THEN, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.GREATER_EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyLessThan(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.LESS_THEN, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.LESS_EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("serverkey", value1, value2, ConditionMode.BETWEEN, "serverkey", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andServerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("serverkey", value1, value2, ConditionMode.NOT_BETWEEN, "serverkey", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andServerkeyIn(List<java.lang.String> values) {
          addCriterion("serverkey", values, ConditionMode.IN, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andServerkeyNotIn(List<java.lang.String> values) {
          addCriterion("serverkey", values, ConditionMode.NOT_IN, "serverkey", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andLayerkeyIsNull() {
		isnull("layerkey");
		return this;
	}
	
	public GisServerApiCriteria andLayerkeyIsNotNull() {
		notNull("layerkey");
		return this;
	}
	
	public GisServerApiCriteria andLayerkeyIsEmpty() {
		empty("layerkey");
		return this;
	}

	public GisServerApiCriteria andLayerkeyIsNotEmpty() {
		notEmpty("layerkey");
		return this;
	}
        public GisServerApiCriteria andLayerkeyLike(java.lang.String value) {
    	   addCriterion("layerkey", value, ConditionMode.FUZZY, "layerkey", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andLayerkeyNotLike(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_FUZZY, "layerkey", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andLayerkeyEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyNotEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyGreaterThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyLessThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layerkey", value1, value2, ConditionMode.BETWEEN, "layerkey", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andLayerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layerkey", value1, value2, ConditionMode.NOT_BETWEEN, "layerkey", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andLayerkeyIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.IN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andLayerkeyNotIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.NOT_IN, "layerkey", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisServerApiCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisServerApiCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisServerApiCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisServerApiCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public GisServerApiCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public GisServerApiCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public GisServerApiCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public GisServerApiCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public GisServerApiCriteria andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public GisServerApiCriteria andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public GisServerApiCriteria andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
        public GisServerApiCriteria andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public GisServerApiCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public GisServerApiCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public GisServerApiCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
        public GisServerApiCriteria andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andApiIdIsNull() {
		isnull("api_id");
		return this;
	}
	
	public GisServerApiCriteria andApiIdIsNotNull() {
		notNull("api_id");
		return this;
	}
	
	public GisServerApiCriteria andApiIdIsEmpty() {
		empty("api_id");
		return this;
	}

	public GisServerApiCriteria andApiIdIsNotEmpty() {
		notEmpty("api_id");
		return this;
	}
       public GisServerApiCriteria andApiIdEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdNotEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.NOT_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdGreaterThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdLessThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("api_id", value1, value2, ConditionMode.BETWEEN, "apiId", "java.lang.Long", "Float");
    	  return this;
      }

      public GisServerApiCriteria andApiIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("api_id", value1, value2, ConditionMode.NOT_BETWEEN, "apiId", "java.lang.Long", "Float");
          return this;
      }
        
      public GisServerApiCriteria andApiIdIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.IN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public GisServerApiCriteria andApiIdNotIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.NOT_IN, "apiId", "java.lang.Long", "Float");
          return this;
      }
	public GisServerApiCriteria andUrlpatternIsNull() {
		isnull("urlpattern");
		return this;
	}
	
	public GisServerApiCriteria andUrlpatternIsNotNull() {
		notNull("urlpattern");
		return this;
	}
	
	public GisServerApiCriteria andUrlpatternIsEmpty() {
		empty("urlpattern");
		return this;
	}

	public GisServerApiCriteria andUrlpatternIsNotEmpty() {
		notEmpty("urlpattern");
		return this;
	}
        public GisServerApiCriteria andUrlpatternLike(java.lang.String value) {
    	   addCriterion("urlpattern", value, ConditionMode.FUZZY, "urlpattern", "java.lang.String", "Float");
    	   return this;
      }

      public GisServerApiCriteria andUrlpatternNotLike(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.NOT_FUZZY, "urlpattern", "java.lang.String", "Float");
          return this;
      }
      public GisServerApiCriteria andUrlpatternEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternNotEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.NOT_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternGreaterThan(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.GREATER_THEN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.GREATER_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternLessThan(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.LESS_THEN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternLessThanOrEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.LESS_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("urlpattern", value1, value2, ConditionMode.BETWEEN, "urlpattern", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andUrlpatternNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("urlpattern", value1, value2, ConditionMode.NOT_BETWEEN, "urlpattern", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andUrlpatternIn(List<java.lang.String> values) {
          addCriterion("urlpattern", values, ConditionMode.IN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andUrlpatternNotIn(List<java.lang.String> values) {
          addCriterion("urlpattern", values, ConditionMode.NOT_IN, "urlpattern", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andTargeturiIsNull() {
		isnull("targetUri");
		return this;
	}
	
	public GisServerApiCriteria andTargeturiIsNotNull() {
		notNull("targetUri");
		return this;
	}
	
	public GisServerApiCriteria andTargeturiIsEmpty() {
		empty("targetUri");
		return this;
	}

	public GisServerApiCriteria andTargeturiIsNotEmpty() {
		notEmpty("targetUri");
		return this;
	}
        public GisServerApiCriteria andTargeturiLike(java.lang.String value) {
    	   addCriterion("targetUri", value, ConditionMode.FUZZY, "targeturi", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andTargeturiNotLike(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.NOT_FUZZY, "targeturi", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andTargeturiEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiNotEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.NOT_EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiGreaterThan(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.GREATER_THEN, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.GREATER_EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiLessThan(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.LESS_THEN, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiLessThanOrEqualTo(java.lang.String value) {
          addCriterion("targetUri", value, ConditionMode.LESS_EQUAL, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("targetUri", value1, value2, ConditionMode.BETWEEN, "targeturi", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andTargeturiNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("targetUri", value1, value2, ConditionMode.NOT_BETWEEN, "targeturi", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andTargeturiIn(List<java.lang.String> values) {
          addCriterion("targetUri", values, ConditionMode.IN, "targeturi", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andTargeturiNotIn(List<java.lang.String> values) {
          addCriterion("targetUri", values, ConditionMode.NOT_IN, "targeturi", "java.lang.String", "String");
          return this;
      }
	public GisServerApiCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public GisServerApiCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public GisServerApiCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public GisServerApiCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
        public GisServerApiCriteria andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public GisServerApiCriteria andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public GisServerApiCriteria andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public GisServerApiCriteria andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public GisServerApiCriteria andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerApiCriteria andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
}