package com.mapfinal;

import java.io.File;
import java.util.Map;

import com.jfinal.config.Constants;
import com.jfinal.config.Routes;
import com.jfinal.kit.PathKit;
import com.lambkit.db.datasource.DataSourceConfig;
import com.lambkit.db.datasource.DataSourceConfigManager;
import com.lambkit.module.LambkitModule;
import com.lambkit.module.annotation.Module;
import com.mapfinal.data.controller.MapDataController;
import com.mapfinal.data.postgis.PostgisStore;
import com.mapfinal.data.shpfile.ShapefileStore;
import com.mapfinal.server.ServerRoute;

@Module
public class MapFinalModule extends LambkitModule {
	
	@Override
	public void configConstant(Constants me) {
		super.configConstant(me);
		System.out.println("started mapFinalModule");
	}
	
	@Override
	public void configRoute(Routes me) {
		me.add(new ServerRoute());
		me.add("/map", MapDataController.class);
	}
	
	@Override
	public void onStart() {
		Map<String, DataSourceConfig> configs = DataSourceConfigManager.me().getDatasourceConfigs();
		for (DataSourceConfig config : configs.values()) {
			if(DataSourceConfig.TYPE_POSTGRESQL.equals(config.getType())) {
				PostgisStore.create(config, false);
			}
		}
		String path = PathKit.getWebRootPath();
		path += File.separator + "data" + File.separator + "data";
		ShapefileStore.create("shp", path);
	}
}
