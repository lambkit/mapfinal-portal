/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-27
 * @version 1.0
 * @since 1.0
 */
public class GisLayerCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisLayerCriteria create() {
		return new GisLayerCriteria();
	}
	
	public static GisLayerCriteria create(Column column) {
		GisLayerCriteria that = new GisLayerCriteria();
		that.add(column);
        return that;
    }

    public static GisLayerCriteria create(String name, Object value) {
        return (GisLayerCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_layer", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisLayerCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisLayerCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisLayerCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisLayerCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisLayerCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisLayerCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisLayerCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisLayerCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisLayerCriteria andLayerkeyIsNull() {
		isnull("layerkey");
		return this;
	}
	
	public GisLayerCriteria andLayerkeyIsNotNull() {
		notNull("layerkey");
		return this;
	}
	
	public GisLayerCriteria andLayerkeyIsEmpty() {
		empty("layerkey");
		return this;
	}

	public GisLayerCriteria andLayerkeyIsNotEmpty() {
		notEmpty("layerkey");
		return this;
	}
        public GisLayerCriteria andLayerkeyLike(java.lang.String value) {
    	   addCriterion("layerkey", value, ConditionMode.FUZZY, "layerkey", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andLayerkeyNotLike(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_FUZZY, "layerkey", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andLayerkeyEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyNotEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.NOT_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyGreaterThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.GREATER_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyLessThan(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_THEN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layerkey", value, ConditionMode.LESS_EQUAL, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layerkey", value1, value2, ConditionMode.BETWEEN, "layerkey", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andLayerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layerkey", value1, value2, ConditionMode.NOT_BETWEEN, "layerkey", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andLayerkeyIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.IN, "layerkey", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andLayerkeyNotIn(List<java.lang.String> values) {
          addCriterion("layerkey", values, ConditionMode.NOT_IN, "layerkey", "java.lang.String", "String");
          return this;
      }
	public GisLayerCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisLayerCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisLayerCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisLayerCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisLayerCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisLayerCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public GisLayerCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public GisLayerCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public GisLayerCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public GisLayerCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public GisLayerCriteria andGroupsIsNull() {
		isnull("groups");
		return this;
	}
	
	public GisLayerCriteria andGroupsIsNotNull() {
		notNull("groups");
		return this;
	}
	
	public GisLayerCriteria andGroupsIsEmpty() {
		empty("groups");
		return this;
	}

	public GisLayerCriteria andGroupsIsNotEmpty() {
		notEmpty("groups");
		return this;
	}
        public GisLayerCriteria andGroupsLike(java.lang.String value) {
    	   addCriterion("groups", value, ConditionMode.FUZZY, "groups", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andGroupsNotLike(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.NOT_FUZZY, "groups", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andGroupsEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsNotEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.NOT_EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsGreaterThan(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.GREATER_THEN, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.GREATER_EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsLessThan(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.LESS_THEN, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("groups", value, ConditionMode.LESS_EQUAL, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("groups", value1, value2, ConditionMode.BETWEEN, "groups", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andGroupsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("groups", value1, value2, ConditionMode.NOT_BETWEEN, "groups", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andGroupsIn(List<java.lang.String> values) {
          addCriterion("groups", values, ConditionMode.IN, "groups", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andGroupsNotIn(List<java.lang.String> values) {
          addCriterion("groups", values, ConditionMode.NOT_IN, "groups", "java.lang.String", "String");
          return this;
      }
	public GisLayerCriteria andDataTypeIsNull() {
		isnull("data_type");
		return this;
	}
	
	public GisLayerCriteria andDataTypeIsNotNull() {
		notNull("data_type");
		return this;
	}
	
	public GisLayerCriteria andDataTypeIsEmpty() {
		empty("data_type");
		return this;
	}

	public GisLayerCriteria andDataTypeIsNotEmpty() {
		notEmpty("data_type");
		return this;
	}
        public GisLayerCriteria andDataTypeLike(java.lang.String value) {
    	   addCriterion("data_type", value, ConditionMode.FUZZY, "dataType", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andDataTypeNotLike(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.NOT_FUZZY, "dataType", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andDataTypeEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeNotEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.NOT_EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeGreaterThan(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.GREATER_THEN, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.GREATER_EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeLessThan(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.LESS_THEN, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("data_type", value, ConditionMode.LESS_EQUAL, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("data_type", value1, value2, ConditionMode.BETWEEN, "dataType", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andDataTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("data_type", value1, value2, ConditionMode.NOT_BETWEEN, "dataType", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andDataTypeIn(List<java.lang.String> values) {
          addCriterion("data_type", values, ConditionMode.IN, "dataType", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andDataTypeNotIn(List<java.lang.String> values) {
          addCriterion("data_type", values, ConditionMode.NOT_IN, "dataType", "java.lang.String", "String");
          return this;
      }
	public GisLayerCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public GisLayerCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public GisLayerCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public GisLayerCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
        public GisLayerCriteria andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
	public GisLayerCriteria andThumbnailIsNull() {
		isnull("thumbnail");
		return this;
	}
	
	public GisLayerCriteria andThumbnailIsNotNull() {
		notNull("thumbnail");
		return this;
	}
	
	public GisLayerCriteria andThumbnailIsEmpty() {
		empty("thumbnail");
		return this;
	}

	public GisLayerCriteria andThumbnailIsNotEmpty() {
		notEmpty("thumbnail");
		return this;
	}
        public GisLayerCriteria andThumbnailLike(java.lang.String value) {
    	   addCriterion("thumbnail", value, ConditionMode.FUZZY, "thumbnail", "java.lang.String", "String");
    	   return this;
      }

      public GisLayerCriteria andThumbnailNotLike(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.NOT_FUZZY, "thumbnail", "java.lang.String", "String");
          return this;
      }
      public GisLayerCriteria andThumbnailEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailNotEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.NOT_EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailGreaterThan(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.GREATER_THEN, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.GREATER_EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailLessThan(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.LESS_THEN, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailLessThanOrEqualTo(java.lang.String value) {
          addCriterion("thumbnail", value, ConditionMode.LESS_EQUAL, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("thumbnail", value1, value2, ConditionMode.BETWEEN, "thumbnail", "java.lang.String", "String");
    	  return this;
      }

      public GisLayerCriteria andThumbnailNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("thumbnail", value1, value2, ConditionMode.NOT_BETWEEN, "thumbnail", "java.lang.String", "String");
          return this;
      }
        
      public GisLayerCriteria andThumbnailIn(List<java.lang.String> values) {
          addCriterion("thumbnail", values, ConditionMode.IN, "thumbnail", "java.lang.String", "String");
          return this;
      }

      public GisLayerCriteria andThumbnailNotIn(List<java.lang.String> values) {
          addCriterion("thumbnail", values, ConditionMode.NOT_IN, "thumbnail", "java.lang.String", "String");
          return this;
      }
}