/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-11-01
 * @version 1.0
 * @since 1.0
 */
public class CmsCategoryCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsCategoryCriteria create() {
		return new CmsCategoryCriteria();
	}
	
	public static CmsCategoryCriteria create(Column column) {
		CmsCategoryCriteria that = new CmsCategoryCriteria();
		that.add(column);
        return that;
    }

    public static CmsCategoryCriteria create(String name, Object value) {
        return (CmsCategoryCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_category", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsCategoryCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsCategoryCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsCategoryCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsCategoryCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsCategoryCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsCategoryCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsCategoryCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsCategoryCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsCategoryCriteria andCategoryIdIsNull() {
		isnull("category_id");
		return this;
	}
	
	public CmsCategoryCriteria andCategoryIdIsNotNull() {
		notNull("category_id");
		return this;
	}
	
	public CmsCategoryCriteria andCategoryIdIsEmpty() {
		empty("category_id");
		return this;
	}

	public CmsCategoryCriteria andCategoryIdIsNotEmpty() {
		notEmpty("category_id");
		return this;
	}
       public CmsCategoryCriteria andCategoryIdEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdNotEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.NOT_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdGreaterThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.GREATER_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdLessThan(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_THEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("category_id", value, ConditionMode.LESS_EQUAL, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("category_id", value1, value2, ConditionMode.BETWEEN, "categoryId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategoryCriteria andCategoryIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("category_id", value1, value2, ConditionMode.NOT_BETWEEN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategoryCriteria andCategoryIdIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andCategoryIdNotIn(List<java.lang.Long> values) {
          addCriterion("category_id", values, ConditionMode.NOT_IN, "categoryId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategoryCriteria andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public CmsCategoryCriteria andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public CmsCategoryCriteria andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public CmsCategoryCriteria andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
       public CmsCategoryCriteria andPidEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidNotEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidGreaterThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidLessThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategoryCriteria andPidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategoryCriteria andPidIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andPidNotIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategoryCriteria andLevelIsNull() {
		isnull("level");
		return this;
	}
	
	public CmsCategoryCriteria andLevelIsNotNull() {
		notNull("level");
		return this;
	}
	
	public CmsCategoryCriteria andLevelIsEmpty() {
		empty("level");
		return this;
	}

	public CmsCategoryCriteria andLevelIsNotEmpty() {
		notEmpty("level");
		return this;
	}
       public CmsCategoryCriteria andLevelEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelNotEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.NOT_EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelGreaterThan(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.GREATER_THEN, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.GREATER_EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelLessThan(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.LESS_THEN, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("level", value, ConditionMode.LESS_EQUAL, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("level", value1, value2, ConditionMode.BETWEEN, "level", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCategoryCriteria andLevelNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("level", value1, value2, ConditionMode.NOT_BETWEEN, "level", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCategoryCriteria andLevelIn(List<java.lang.Integer> values) {
          addCriterion("level", values, ConditionMode.IN, "level", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andLevelNotIn(List<java.lang.Integer> values) {
          addCriterion("level", values, ConditionMode.NOT_IN, "level", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCategoryCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsCategoryCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsCategoryCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsCategoryCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public CmsCategoryCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCategoryCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public CmsCategoryCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategoryCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategoryCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsCategoryCriteria andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsCategoryCriteria andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsCategoryCriteria andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsCategoryCriteria andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
        public CmsCategoryCriteria andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategoryCriteria andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsCategoryCriteria andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategoryCriteria andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategoryCriteria andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsCategoryCriteria andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public CmsCategoryCriteria andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public CmsCategoryCriteria andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public CmsCategoryCriteria andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
        public CmsCategoryCriteria andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategoryCriteria andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "String");
          return this;
      }
      public CmsCategoryCriteria andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategoryCriteria andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategoryCriteria andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public CmsCategoryCriteria andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public CmsCategoryCriteria andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public CmsCategoryCriteria andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public CmsCategoryCriteria andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public CmsCategoryCriteria andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsCategoryCriteria andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsCategoryCriteria andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public CmsCategoryCriteria andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public CmsCategoryCriteria andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public CmsCategoryCriteria andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public CmsCategoryCriteria andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public CmsCategoryCriteria andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
        public CmsCategoryCriteria andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "Float");
    	   return this;
      }

      public CmsCategoryCriteria andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "Float");
          return this;
      }
      public CmsCategoryCriteria andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategoryCriteria andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategoryCriteria andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public CmsCategoryCriteria andFlagIsNull() {
		isnull("flag");
		return this;
	}
	
	public CmsCategoryCriteria andFlagIsNotNull() {
		notNull("flag");
		return this;
	}
	
	public CmsCategoryCriteria andFlagIsEmpty() {
		empty("flag");
		return this;
	}

	public CmsCategoryCriteria andFlagIsNotEmpty() {
		notEmpty("flag");
		return this;
	}
        public CmsCategoryCriteria andFlagLike(java.lang.String value) {
    	   addCriterion("flag", value, ConditionMode.FUZZY, "flag", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategoryCriteria andFlagNotLike(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_FUZZY, "flag", "java.lang.String", "String");
          return this;
      }
      public CmsCategoryCriteria andFlagEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagNotEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagGreaterThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagLessThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flag", value1, value2, ConditionMode.BETWEEN, "flag", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategoryCriteria andFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flag", value1, value2, ConditionMode.NOT_BETWEEN, "flag", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategoryCriteria andFlagIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.IN, "flag", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andFlagNotIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.NOT_IN, "flag", "java.lang.String", "String");
          return this;
      }
	public CmsCategoryCriteria andStyleIsNull() {
		isnull("style");
		return this;
	}
	
	public CmsCategoryCriteria andStyleIsNotNull() {
		notNull("style");
		return this;
	}
	
	public CmsCategoryCriteria andStyleIsEmpty() {
		empty("style");
		return this;
	}

	public CmsCategoryCriteria andStyleIsNotEmpty() {
		notEmpty("style");
		return this;
	}
        public CmsCategoryCriteria andStyleLike(java.lang.String value) {
    	   addCriterion("style", value, ConditionMode.FUZZY, "style", "java.lang.String", "String");
    	   return this;
      }

      public CmsCategoryCriteria andStyleNotLike(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_FUZZY, "style", "java.lang.String", "String");
          return this;
      }
      public CmsCategoryCriteria andStyleEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleNotEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.NOT_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleGreaterThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.GREATER_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleLessThan(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_THEN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("style", value, ConditionMode.LESS_EQUAL, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("style", value1, value2, ConditionMode.BETWEEN, "style", "java.lang.String", "String");
    	  return this;
      }

      public CmsCategoryCriteria andStyleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("style", value1, value2, ConditionMode.NOT_BETWEEN, "style", "java.lang.String", "String");
          return this;
      }
        
      public CmsCategoryCriteria andStyleIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.IN, "style", "java.lang.String", "String");
          return this;
      }

      public CmsCategoryCriteria andStyleNotIn(List<java.lang.String> values) {
          addCriterion("style", values, ConditionMode.NOT_IN, "style", "java.lang.String", "String");
          return this;
      }
	public CmsCategoryCriteria andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsCategoryCriteria andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsCategoryCriteria andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsCategoryCriteria andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
       public CmsCategoryCriteria andSystemIdEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdNotEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdGreaterThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdLessThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsCategoryCriteria andSystemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsCategoryCriteria andSystemIdIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public CmsCategoryCriteria andSystemIdNotIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Long", "Float");
          return this;
      }
	public CmsCategoryCriteria andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsCategoryCriteria andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsCategoryCriteria andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsCategoryCriteria andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
       public CmsCategoryCriteria andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsCategoryCriteria andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsCategoryCriteria andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsCategoryCriteria andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsCategoryCriteria andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsCategoryCriteria andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsCategoryCriteria andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsCategoryCriteria andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
       public CmsCategoryCriteria andOrdersEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersNotEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersGreaterThan(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersGreaterThanOrEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersLessThan(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersLessThanOrEqualTo(java.math.BigInteger value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersBetween(java.math.BigInteger value1, java.math.BigInteger value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.math.BigInteger", "String");
    	  return this;
      }

      public CmsCategoryCriteria andOrdersNotBetween(java.math.BigInteger value1, java.math.BigInteger value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.math.BigInteger", "String");
          return this;
      }
        
      public CmsCategoryCriteria andOrdersIn(List<java.math.BigInteger> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.math.BigInteger", "String");
          return this;
      }

      public CmsCategoryCriteria andOrdersNotIn(List<java.math.BigInteger> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.math.BigInteger", "String");
          return this;
      }
}