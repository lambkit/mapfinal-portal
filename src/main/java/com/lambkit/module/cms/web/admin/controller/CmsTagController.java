package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsTag;
import com.lambkit.module.cms.web.validator.CmsTagValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;


import java.math.BigInteger;

/**
 * 标签控制器
 */
//@Controller
@Api(tag = "标签管理", description = "标签管理")
//@RequestMapping("/manage/tag")
public class CmsTagController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsTagController.class);
	
	@ApiOperation(tag = "标签首页")
	@RequiresPermissions("cms:tag:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/tag/index.html");
	}

	@ApiOperation(tag = "标签列表")
	@RequiresPermissions("cms:tag:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsCategoryExample = CmsTag.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsCategoryExample.setOrderBy(sort + " " + order);
		}
		Page<CmsTag> categorys = CmsTag.service().paginate(cmsCategoryExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, categorys));
	}

	@ApiOperation(tag = "新增标签")
	@RequiresPermissions("cms:tag:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsTagValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/tag/create.html");
		} else {
			CmsTag cmsTag = getModel(CmsTag.class, "tag");
			long time = System.currentTimeMillis();
			cmsTag.setCtime(DateTimeUtils.parse(time));
			cmsTag.setOrders(BigInteger.valueOf(time));
			boolean flag = cmsTag.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除标签")
	@RequiresPermissions("cms:tag:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsTag.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改标签")
	@RequiresPermissions("cms:tag:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsTagValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsTag tag = CmsTag.service().findById(id);
			setAttr("tag", tag);
			render(AdminManager.me().getTemplatePath() + "/cms/tag/update.html");
		} else {
			CmsTag cmsTag = getModel(CmsTag.class, "tag");
			boolean flag = false;
			if(cmsTag.getTagId()!=null) {
				flag = cmsTag.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
}