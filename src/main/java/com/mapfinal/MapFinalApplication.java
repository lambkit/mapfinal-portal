package com.mapfinal;

import com.lambkit.LambkitApplicationContext;
import com.lambkit.module.LambkitModule;
import com.lambkit.module.admin.AdminModule;
import com.lambkit.module.cms.CmsModule;
import com.lambkit.module.upms.server.UpmsModule;
import com.mapfinal.api.MapApiModule;
import com.mapfinal.gis.GisModule;
import com.lambkit.LambkitApplication;

public class MapFinalApplication extends LambkitApplicationContext {
	
	@Override
	public void configModule(LambkitModule module) {
		// TODO Auto-generated method stub
		super.configModule(module);
		module.addModule(new UpmsModule());
		
		module.addModule(new MapFinalModule());
		module.addModule(new MapApiModule());
		module.addModule(new GisModule());
		
		module.addModule(new AdminModule());
		module.addModule(new CmsModule());
	}

	public static void main(String[] args) {
		LambkitApplication.run(MapFinalApplication.class, args);
	}
}
