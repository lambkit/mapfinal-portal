package com.lambkit.module.cms.common;

import com.lambkit.Lambkit;
import com.lambkit.core.config.annotation.PropertieConfig;

@PropertieConfig(prefix = "lambkit.cms")
public class CmsConfig {
	
	public static CmsConfig me() {
		return Lambkit.config(CmsConfig.class);
	}

	private String url = "/";
	private String webTemplate;
	private String serverType = "server";
	private String version = "1.0";
	private String dbconfig;

	public String getServerType() {
		return serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getDbconfig() {
		return dbconfig;
	}

	public void setDbconfig(String dbconfig) {
		this.dbconfig = dbconfig;
	}

	public String getWebTemplate() {
		return webTemplate;
	}

	public void setWebTemplate(String webTemplate) {
		this.webTemplate = webTemplate;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}
