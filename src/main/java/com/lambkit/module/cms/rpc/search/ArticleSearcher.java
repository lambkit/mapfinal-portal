package com.lambkit.module.cms.rpc.search;

import com.jfinal.plugin.activerecord.Page;
import com.lambkit.module.cms.rpc.model.CmsArticle;

public interface ArticleSearcher {
	String HIGH_LIGHT_CLASS = "search-highlight";

    public void addArticle(CmsArticle article);

    public void deleteArticle(Object id);

    public void updateArticle(CmsArticle article);

    public Page<CmsArticle> search(String keyword, int pageNum, int pageSize);
}
