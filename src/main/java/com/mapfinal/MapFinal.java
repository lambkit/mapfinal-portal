package com.mapfinal;

import com.mapfinal.api.service.GeoApiServiceFactory;

public class MapFinal {

	private static final MapFinal me = new MapFinal();
	
	public static MapFinal me() {
		return me;
	}
	
	public MapFinal() {
		geoApiServiceFactory = new GeoApiServiceFactory();
	}
	
	private GeoApiServiceFactory geoApiServiceFactory;
	
	public GeoApiServiceFactory getGeoApiServiceFactory() {
		return geoApiServiceFactory;
	}

	public void setGeoApiServiceFactory(GeoApiServiceFactory geoApiServiceFactory) {
		this.geoApiServiceFactory = geoApiServiceFactory;
	}
}
