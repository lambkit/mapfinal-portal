/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model;

import com.jfinal.kit.StrKit;
import com.lambkit.Lambkit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;
import com.mapfinal.gis.GisConfig;
import com.mapfinal.gis.model.base.BaseGisLayerFileShape;
import com.mapfinal.gis.model.sql.GisLayerFileShapeCriteria;
import com.mapfinal.gis.service.GisLayerFileShapeService;
import com.mapfinal.gis.service.impl.GisLayerFileShapeServiceImpl;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisLayerFileShape extends BaseGisLayerFileShape<GisLayerFileShape> {

	private static final long serialVersionUID = 1L;
	
	public static GisLayerFileShapeService service() {
		return ServiceKit.inject(GisLayerFileShapeService.class, GisLayerFileShapeServiceImpl.class);
	}
	
	public static GisLayerFileShapeCriteria sql() {
		return new GisLayerFileShapeCriteria();
	}
	
	public static GisLayerFileShapeCriteria sql(Column column) {
		GisLayerFileShapeCriteria that = new GisLayerFileShapeCriteria();
		that.add(column);
        return that;
    }

	public GisLayerFileShape() {
		GisConfig config = Lambkit.config(GisConfig.class);
		String dbconfig = config.getDbconfig();
		if(StrKit.notBlank(dbconfig)) {
			this.use(dbconfig);
		}
	}
}
