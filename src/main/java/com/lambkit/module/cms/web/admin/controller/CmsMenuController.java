package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsMenu;
import com.lambkit.module.cms.rpc.model.sql.CmsMenuCriteria;
import com.lambkit.module.cms.web.validator.CmsMenuValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.jfinal.log.Log;

/**
 * 菜单控制器
 */
//@Controller
@Api(tag = "菜单管理", description = "菜单管理")
//@RequestMapping("/manage/menu")
public class CmsMenuController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsMenuController.class);
	
	@ApiOperation(tag = "评论首页")
	@RequiresPermissions("cms:menu:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/menu/index.html");
	}

	@ApiOperation(tag = "评论列表")
	@RequiresPermissions("cms:menu:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsMenuExample = CmsMenu.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsMenuExample.setOrderBy(sort + " " + order);
		}
		Page<CmsMenu> menus = CmsMenu.service().paginate(cmsMenuExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, menus));
	}

	@ApiOperation(tag = "新增菜单")
	@RequiresPermissions("cms:menu:create")
	//@RequestMapping(value = "/create", method = RequestMethod.GET)
	@Before(CmsMenuValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			render(AdminManager.me().getTemplatePath() + "/cms/menu/create.html");
		} else {
			CmsMenu cmsMenu = getModel(CmsMenu.class, "menu");
			long time = System.currentTimeMillis();
			cmsMenu.setOrders(time);
			boolean flag = cmsMenu.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除菜单")
	@RequiresPermissions("cms:menu:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);
		int count = CmsMenu.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}
	
	@ApiOperation(tag = "修改菜单")
	@RequiresPermissions("cms:menu:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	//@ResponseBody
	@Before(CmsMenuValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			CmsMenu cmsMenu = CmsMenu.service().findById(id);
			setAttr("menu", cmsMenu);
			render(AdminManager.me().getTemplatePath() + "/cms/menu/update.html");
		} else {
			CmsMenu cmsMenu = getModel(CmsMenu.class, "menu");
			boolean flag = false;
			if(cmsMenu.getMenuId()!=null) {
				flag = cmsMenu.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
	
	@ApiOperation(tag = "上移菜单")
	@RequiresPermissions("cms:menu:up")
	//@RequestMapping(value = "/up/{id}", method = RequestMethod.GET)
	//@ResponseBody
	public void up() {
		Long id = getParaToLong(0);
		CmsMenu cmsMenu = CmsMenu.service().findById(id);
		if (null == cmsMenu) {
			renderJson(new CmsResult(CmsResultConstant.INVALID_PARAMETER, "无效参数！"));
		}
		CmsMenuCriteria criteria = CmsMenu.sql();
		if (null == cmsMenu.getPid()) {
			criteria.andPidIsNull();
		} else {
			criteria.andPidEqualTo(cmsMenu.getPid());
		}
		criteria.andOrdersLessThan(cmsMenu.getOrders());
		Example cmsMenuExample = criteria.example().setOrderBy("orders desc");
		CmsMenu upCmsMenu = CmsMenu.service().findFirst(cmsMenuExample);
		if (null == upCmsMenu) {
			renderJson(new CmsResult(CmsResultConstant.FAILED, "不能上移了！"));
		}
		long tempOrders = upCmsMenu.getOrders();
		upCmsMenu.setOrders(cmsMenu.getOrders());
		cmsMenu.setOrders(tempOrders);
		cmsMenu.update();
		upCmsMenu.update();
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, 1));
	}

	@ApiOperation(tag = "下移菜单")
	@RequiresPermissions("cms:menu:down")
	//@RequestMapping(value = "/down/{id}", method = RequestMethod.GET)
	//@ResponseBody
	public Object down() {
		Long id = getParaToLong(0);
		CmsMenu cmsMenu = CmsMenu.service().findById(id);
		if (null == cmsMenu) {
			return new CmsResult(CmsResultConstant.INVALID_PARAMETER, "无效参数！");
		}
		CmsMenuCriteria criteria = CmsMenu.sql();
		if (null == cmsMenu.getPid()) {
			criteria.andPidIsNull();
		} else {
			criteria.andPidEqualTo(cmsMenu.getPid());
		}
		criteria.andOrdersGreaterThan(cmsMenu.getOrders());
		Example cmsMenuExample = criteria.example().setOrderBy("orders desc");
		CmsMenu upCmsMenu = CmsMenu.service().findFirst(cmsMenuExample);
		if (null == upCmsMenu) {
			return new CmsResult(CmsResultConstant.FAILED, "不能下移了！");
		}
		long tempOrders = upCmsMenu.getOrders();
		upCmsMenu.setOrders(cmsMenu.getOrders());
		cmsMenu.setOrders(tempOrders);
		cmsMenu.update();
		upCmsMenu.update();
		return new CmsResult(CmsResultConstant.SUCCESS, 1);
	}

}