/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.controller;

import com.mapfinal.gis.MschController;
import com.mapfinal.gis.model.GisMapLayer;
import com.mapfinal.gis.web.validator.GisMapLayerValidator;
import com.jfinal.aop.Before;

public class MapLayerController extends MschController<GisMapLayer> {

	@Override
	public void index() {
		// TODO Auto-generated method stub
		super.index();
	}
	
	@Override
	public void list() {
		// TODO Auto-generated method stub
		super.list();
	}
	
	@Override
	public void view() {
		// TODO Auto-generated method stub
		super.view();
	}
	
	@Override
	public void add() {
		// TODO Auto-generated method stub
		super.add();
	}
	
	@Override
	public void edit() {
		// TODO Auto-generated method stub
		super.edit();
	}
	
	@Override
	public void get() {
		// TODO Auto-generated method stub
		super.get();
	}
	
	@Override
	public void page() {
		// TODO Auto-generated method stub
		super.page();
	}
	
	@Override
	public void delete() {
		// TODO Auto-generated method stub
		super.delete();
	}
	
	@Before(GisMapLayerValidator.class)
	public void save() {
		doSave(getModel(GisMapLayer.class, "model"));
	}
	
	@Before(GisMapLayerValidator.class)
	public void update() {
		doUpdate(getModel(GisMapLayer.class, "model"));
	}
	
	@Override
	protected String getTableName() {
		// TODO Auto-generated method stub
		return GisMapLayer.service().getTableName();
	}

	@Override
	protected GisMapLayer findById(Object id) {
		// TODO Auto-generated method stub
		return GisMapLayer.service().findById(id);
	}
	
	@Override
	protected String getTemplatePath() {
		// TODO Auto-generated method stub
		return "mapLayer/";
	}
}
