package com.lambkit.module.cms.web.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.lambkit.module.cms.rpc.model.CmsTag;
import com.lambkit.web.directive.LambkitDirective;
import com.lambkit.web.directive.annotation.JFinalDirective;

import java.util.List;

/**
 * @author Henry Yang 杨勇 (gismail@foxmail.com)
 * @version 1.0
 * @Title: 文章分类：分类、专题、标签等
 * @Package com.lambkit.module.cms.web.directive
 */
@JFinalDirective("tags")
public class TagsDirective extends LambkitDirective {

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        String orderBy = getPara("orderBy", scope, "id desc");
        int count = getParaToInt("count", scope, 10);

        List<CmsTag> cmsTags = CmsTag.service().findTagList(orderBy, count);
        if (cmsTags == null || cmsTags.isEmpty()) {
            return;
        }


        scope.setLocal("tags", cmsTags);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
