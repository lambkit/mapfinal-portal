package com.lambkit.module.cms.web.admin.controller;

import com.jfinal.aop.Before;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Page;
import com.lambkit.common.util.DateTimeUtils;
import com.lambkit.component.swagger.annotation.Api;
import com.lambkit.component.swagger.annotation.ApiOperation;
import com.lambkit.db.sql.column.Example;
import com.lambkit.module.admin.AdminManager;
import com.lambkit.module.cms.common.CmsResult;
import com.lambkit.module.cms.common.CmsResultConstant;
import com.lambkit.module.cms.rpc.model.CmsArticle;
import com.lambkit.module.cms.rpc.model.CmsTopic;
import com.lambkit.module.cms.web.validator.CmsArticleValidator;
import com.lambkit.web.controller.LambkitController;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;

import java.math.BigInteger;
import java.util.List;

/**
 * 文章控制器
 */
@Api(tag = "文章管理", description = "文章管理")
//@RequestMapping("/manage/article")
public class CmsArticleController extends LambkitController {

	private static final Log LOG = Log.getLog(CmsArticleController.class);
	
	@ApiOperation(tag = "文章首页")
	@RequiresPermissions("cms:article:read")
	//@RequestMapping(value = "/index", method = RequestMethod.GET)
	public void index() {
		render(AdminManager.me().getTemplatePath() + "/cms/article/index.html");
	}

	@ApiOperation(tag = "文章列表")
	@RequiresPermissions("cms:article:read")
	//@RequestMapping(value = "/list", method = RequestMethod.GET)
	//@ResponseBody
	public void list() {
		int offset = getParaToInt("offset", 0);
		int limit = getParaToInt("limit", 10);
		String sort = getPara("sort");
		String order = getPara("order");
		Example cmsArticleExample = CmsArticle.sql().example();
		if (!StringUtils.isBlank(sort) && !StringUtils.isBlank(order)) {
			cmsArticleExample.setOrderBy(sort + " " + order);
		}
		Page<CmsArticle> articles = CmsArticle.service().paginate(cmsArticleExample, offset, limit);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, articles));
	}

	@ApiOperation(tag = "新增文章")
	@RequiresPermissions("cms:article:create")
	//@RequestMapping(value = "/create", method = RequestMethod.POST)
	//@ResponseBody
	@Before(CmsArticleValidator.class)
	public void create() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Example cmsTopicExample = CmsTopic.sql().example();
			cmsTopicExample.setOrderBy("ctime desc");
			List<CmsTopic> cmsTopics = CmsTopic.service().find(cmsTopicExample);
			setAttr("cmsTopics", cmsTopics);
			render(AdminManager.me().getTemplatePath() + "/cms/article/create.html");
		} else {
			CmsArticle cmsArticle = getModel(CmsArticle.class, "artice");
			long time = System.currentTimeMillis();
			cmsArticle.setCtime(DateTimeUtils.parse(time));
			cmsArticle.setOrders(BigInteger.valueOf(time));
			cmsArticle.setReadnumber(0L);
			boolean flag = cmsArticle.save();
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}

	@ApiOperation(tag = "删除文章")
	@RequiresPermissions("cms:article:delete")
	//@RequestMapping(value = "/delete/{ids}",method = RequestMethod.GET)
	//@ResponseBody
	public void delete() {
		String ids = getPara(0);//逗号分隔
		int count = CmsArticle.service().deleteByPrimaryKeys(ids);
		renderJson(new CmsResult(CmsResultConstant.SUCCESS, count));
	}

	@ApiOperation(tag = "修改文章")
	@RequiresPermissions("cms:article:update")
	//@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	@Before(CmsArticleValidator.class)
	public void update() {
		if("GET".equalsIgnoreCase(getRequest().getMethod())) {
			Long id = getParaToLong(0);
			Example cmsTopicExample = CmsTopic.sql().example();
			cmsTopicExample.setOrderBy("ctime desc");
			List<CmsTopic> cmsTopics = CmsTopic.service().find(cmsTopicExample);
			CmsArticle article = CmsArticle.service().findById(id);
			setAttr("cmsTopics", cmsTopics);
			setAttr("article", article);
			render(AdminManager.me().getTemplatePath() + "/cms/article/update.html");
		} else {
			CmsArticle cmsArticle = getModel(CmsArticle.class, "artice");
			boolean flag = false;
			if(cmsArticle.getArticleId()!=null) {
				flag = cmsArticle.update();
			}
			renderJson(new CmsResult(CmsResultConstant.SUCCESS, flag));
		}
	}
}