package com.mapfinal.data.postgis;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.jdbc.JDBCDataStore;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.styling.SLD;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.opengis.filter.Filter;

import com.lambkit.db.datasource.DataSourceConfig;

public class PostgisKit {
	
	public static SimpleFeatureCollection readPostgisTable(DataSourceConfig config, String tablename) {
		return readPostgisTable(config, tablename, null);
	}
	
	public static SimpleFeatureCollection readPostgisTable(DataSourceConfig config, String tablename, Filter filter) {
		String url = config.getUrl();
		url = url.replace("jdbc:postgresql://", "");
		int i = url.indexOf(":");
		int j = url.indexOf("/");
		String host = url.substring(0, i);
		String port = url.substring(i + 1, j);
		String dbname = url.substring(j + 1);
		return readPostgisTable(host, Integer.valueOf(port), config.getUser(), config.getPassword(), dbname,
				config.getSchema(), tablename, filter);
	}

	/**
	 * 读取postgis表格数据
	 * 
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param dbname
	 * @param schema
	 * @param tablename
	 * @return
	 */
	public static SimpleFeatureCollection readPostgisTable(String host, int port, String user, String pass,
			String dbname, String schema, String tablename) {
		return readPostgisTable(host, port, user, pass, dbname, schema, tablename, null);
	}

	/**
	 * 读取postgis表格数据
	 * 
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param dbname
	 * @param schema
	 * @param tablename
	 * @param filter
	 * @return
	 */
	public static SimpleFeatureCollection readPostgisTable(String host, int port, String user, String pass,
			String dbname, String schema, String tablename, Filter filter) {
		Map<String, Object> params = new HashMap<>();
		params.put("dbtype", "postgis");
		params.put("host", host);
		params.put("port", port);
		params.put("schema", schema);
		params.put("database", dbname);
		params.put("user", user);
		params.put("passwd", pass);
		try {
			JDBCDataStore dataStore = (JDBCDataStore) DataStoreFinder.getDataStore(params);

			return readDatastore(dataStore, tablename, filter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 读取postgis表格数据
	 * 
	 * @param store
	 * @param typeName
	 * @param filter
	 * @return
	 */
	public static SimpleFeatureCollection readDatastore(JDBCDataStore store, String typeName, Filter filter) {
		try {
			SimpleFeatureSource featureSource = store.getFeatureSource(typeName);

			return filter != null ? featureSource.getFeatures(filter) : featureSource.getFeatures();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static SimpleFeatureSource getPostgisTableSource(DataSourceConfig config, String tablename) {
		String url = config.getUrl();
		url = url.replace("jdbc:postgresql://", "");
		int i = url.indexOf(":");
		int j = url.indexOf("/");
		String host = url.substring(0, i);
		String port = url.substring(i + 1, j);
		String dbname = url.substring(j + 1);
		return PostgisKit.getPostgisTableSource(host, Integer.valueOf(port), config.getUser(), config.getPassword(),
				dbname, config.getSchema(), tablename);
	}

	/**
	 * 读取postgis表格数据
	 * 
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param dbname
	 * @param schema
	 * @param tablename
	 * @return
	 */
	public static SimpleFeatureSource getPostgisTableSource(String host, int port, String user, String pass,
			String dbname, String schema, String tablename) {
		Map<String, Object> params = new HashMap<>();
		params.put("dbtype", "postgis");
		params.put("host", host);
		params.put("port", port);
		params.put("schema", schema);
		params.put("database", dbname);
		params.put("user", user);
		params.put("passwd", pass);
		try {
			JDBCDataStore dataStore = (JDBCDataStore) DataStoreFinder.getDataStore(params);
			return dataStore.getFeatureSource(tablename);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 创建GeoTools图层
	 * @param config
	 * @param tbname
	 * @param sldPath
	 * @return
	 */
	public static Layer createLayer(DataSourceConfig config, String tbname, String sldPath) {
		try {
			String url = config.getUrl();
			url = url.replace("jdbc:postgresql://", "");
			int i = url.indexOf(":");
			int j = url.indexOf("/");
			String host = url.substring(0, i);
			String port = url.substring(i+1, j);
			String dbname = url.substring(j+1);

			SimpleFeatureSource featureSource = PostgisKit.getPostgisTableSource(host, Integer.valueOf(port),
					config.getUser(), config.getPassword(), dbname, config.getSchema(), tbname);

			// 样式文件的加载
			Style style = SLD.createSimpleStyle(featureSource.getSchema());
			if (sldPath != "") {
				// SLD的方式
				File sldFile = new File(sldPath);
				StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
				SLDParser stylereader = new SLDParser(styleFactory, sldFile.toURI().toURL());
				Style[] stylearray = stylereader.readXML();
				style = stylearray[0];
			} else {
				SLD.setPolyColour(style, Color.RED);
			}

			// 承载数据文件及样式文件的Layer
			Layer layer = new FeatureLayer(featureSource, style);

			return layer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
