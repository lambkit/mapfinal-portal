/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisServerLayerCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisServerLayerCriteria create() {
		return new GisServerLayerCriteria();
	}
	
	public static GisServerLayerCriteria create(Column column) {
		GisServerLayerCriteria that = new GisServerLayerCriteria();
		that.add(column);
        return that;
    }

    public static GisServerLayerCriteria create(String name, Object value) {
        return (GisServerLayerCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_server_layer", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerLayerCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerLayerCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisServerLayerCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisServerLayerCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerLayerCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerLayerCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerLayerCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerLayerCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisServerLayerCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisServerLayerCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisServerLayerCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisServerLayerCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisServerLayerCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisServerLayerCriteria andServerKeyIsNull() {
		isnull("server_key");
		return this;
	}
	
	public GisServerLayerCriteria andServerKeyIsNotNull() {
		notNull("server_key");
		return this;
	}
	
	public GisServerLayerCriteria andServerKeyIsEmpty() {
		empty("server_key");
		return this;
	}

	public GisServerLayerCriteria andServerKeyIsNotEmpty() {
		notEmpty("server_key");
		return this;
	}
        public GisServerLayerCriteria andServerKeyLike(java.lang.String value) {
    	   addCriterion("server_key", value, ConditionMode.FUZZY, "serverKey", "java.lang.String", "String");
    	   return this;
      }

      public GisServerLayerCriteria andServerKeyNotLike(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.NOT_FUZZY, "serverKey", "java.lang.String", "String");
          return this;
      }
      public GisServerLayerCriteria andServerKeyEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyNotEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.NOT_EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyGreaterThan(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.GREATER_THEN, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.GREATER_EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyLessThan(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.LESS_THEN, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("server_key", value, ConditionMode.LESS_EQUAL, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("server_key", value1, value2, ConditionMode.BETWEEN, "serverKey", "java.lang.String", "String");
    	  return this;
      }

      public GisServerLayerCriteria andServerKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("server_key", value1, value2, ConditionMode.NOT_BETWEEN, "serverKey", "java.lang.String", "String");
          return this;
      }
        
      public GisServerLayerCriteria andServerKeyIn(List<java.lang.String> values) {
          addCriterion("server_key", values, ConditionMode.IN, "serverKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andServerKeyNotIn(List<java.lang.String> values) {
          addCriterion("server_key", values, ConditionMode.NOT_IN, "serverKey", "java.lang.String", "String");
          return this;
      }
	public GisServerLayerCriteria andLayerKeyIsNull() {
		isnull("layer_key");
		return this;
	}
	
	public GisServerLayerCriteria andLayerKeyIsNotNull() {
		notNull("layer_key");
		return this;
	}
	
	public GisServerLayerCriteria andLayerKeyIsEmpty() {
		empty("layer_key");
		return this;
	}

	public GisServerLayerCriteria andLayerKeyIsNotEmpty() {
		notEmpty("layer_key");
		return this;
	}
        public GisServerLayerCriteria andLayerKeyLike(java.lang.String value) {
    	   addCriterion("layer_key", value, ConditionMode.FUZZY, "layerKey", "java.lang.String", "String");
    	   return this;
      }

      public GisServerLayerCriteria andLayerKeyNotLike(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.NOT_FUZZY, "layerKey", "java.lang.String", "String");
          return this;
      }
      public GisServerLayerCriteria andLayerKeyEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyNotEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.NOT_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyGreaterThan(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.GREATER_THEN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.GREATER_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyLessThan(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.LESS_THEN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("layer_key", value, ConditionMode.LESS_EQUAL, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("layer_key", value1, value2, ConditionMode.BETWEEN, "layerKey", "java.lang.String", "String");
    	  return this;
      }

      public GisServerLayerCriteria andLayerKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("layer_key", value1, value2, ConditionMode.NOT_BETWEEN, "layerKey", "java.lang.String", "String");
          return this;
      }
        
      public GisServerLayerCriteria andLayerKeyIn(List<java.lang.String> values) {
          addCriterion("layer_key", values, ConditionMode.IN, "layerKey", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andLayerKeyNotIn(List<java.lang.String> values) {
          addCriterion("layer_key", values, ConditionMode.NOT_IN, "layerKey", "java.lang.String", "String");
          return this;
      }
	public GisServerLayerCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisServerLayerCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisServerLayerCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisServerLayerCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisServerLayerCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public GisServerLayerCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public GisServerLayerCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisServerLayerCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisServerLayerCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisServerLayerCriteria andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public GisServerLayerCriteria andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public GisServerLayerCriteria andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public GisServerLayerCriteria andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
        public GisServerLayerCriteria andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public GisServerLayerCriteria andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public GisServerLayerCriteria andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public GisServerLayerCriteria andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public GisServerLayerCriteria andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerLayerCriteria andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
}