/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mapfinal.gis.model.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.column.Column;
import com.lambkit.db.sql.column.Columns;
import com.lambkit.db.sql.column.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-05-23
 * @version 1.0
 * @since 1.0
 */
public class GisServerCriteria extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static GisServerCriteria create() {
		return new GisServerCriteria();
	}
	
	public static GisServerCriteria create(Column column) {
		GisServerCriteria that = new GisServerCriteria();
		that.add(column);
        return that;
    }

    public static GisServerCriteria create(String name, Object value) {
        return (GisServerCriteria) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("gis_server", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerCriteria eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerCriteria ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public GisServerCriteria like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public GisServerCriteria notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerCriteria gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerCriteria ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerCriteria lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public GisServerCriteria le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public GisServerCriteria isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public GisServerCriteria notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public GisServerCriteria empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public GisServerCriteria notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public GisServerCriteria add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public GisServerCriteria andServerkeyIsNull() {
		isnull("serverkey");
		return this;
	}
	
	public GisServerCriteria andServerkeyIsNotNull() {
		notNull("serverkey");
		return this;
	}
	
	public GisServerCriteria andServerkeyIsEmpty() {
		empty("serverkey");
		return this;
	}

	public GisServerCriteria andServerkeyIsNotEmpty() {
		notEmpty("serverkey");
		return this;
	}
        public GisServerCriteria andServerkeyLike(java.lang.String value) {
    	   addCriterion("serverkey", value, ConditionMode.FUZZY, "serverkey", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andServerkeyNotLike(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.NOT_FUZZY, "serverkey", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andServerkeyEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyNotEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.NOT_EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyGreaterThan(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.GREATER_THEN, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.GREATER_EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyLessThan(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.LESS_THEN, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("serverkey", value, ConditionMode.LESS_EQUAL, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("serverkey", value1, value2, ConditionMode.BETWEEN, "serverkey", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andServerkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("serverkey", value1, value2, ConditionMode.NOT_BETWEEN, "serverkey", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andServerkeyIn(List<java.lang.String> values) {
          addCriterion("serverkey", values, ConditionMode.IN, "serverkey", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andServerkeyNotIn(List<java.lang.String> values) {
          addCriterion("serverkey", values, ConditionMode.NOT_IN, "serverkey", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public GisServerCriteria andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public GisServerCriteria andNameIsEmpty() {
		empty("name");
		return this;
	}

	public GisServerCriteria andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
        public GisServerCriteria andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public GisServerCriteria andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public GisServerCriteria andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public GisServerCriteria andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
        public GisServerCriteria andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andVersionIsNull() {
		isnull("version");
		return this;
	}
	
	public GisServerCriteria andVersionIsNotNull() {
		notNull("version");
		return this;
	}
	
	public GisServerCriteria andVersionIsEmpty() {
		empty("version");
		return this;
	}

	public GisServerCriteria andVersionIsNotEmpty() {
		notEmpty("version");
		return this;
	}
        public GisServerCriteria andVersionLike(java.lang.String value) {
    	   addCriterion("version", value, ConditionMode.FUZZY, "version", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andVersionNotLike(java.lang.String value) {
          addCriterion("version", value, ConditionMode.NOT_FUZZY, "version", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andVersionEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionNotEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.NOT_EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionGreaterThan(java.lang.String value) {
          addCriterion("version", value, ConditionMode.GREATER_THEN, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.GREATER_EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionLessThan(java.lang.String value) {
          addCriterion("version", value, ConditionMode.LESS_THEN, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.LESS_EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("version", value1, value2, ConditionMode.BETWEEN, "version", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andVersionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("version", value1, value2, ConditionMode.NOT_BETWEEN, "version", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andVersionIn(List<java.lang.String> values) {
          addCriterion("version", values, ConditionMode.IN, "version", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andVersionNotIn(List<java.lang.String> values) {
          addCriterion("version", values, ConditionMode.NOT_IN, "version", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public GisServerCriteria andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public GisServerCriteria andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public GisServerCriteria andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
        public GisServerCriteria andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andTokenIsNull() {
		isnull("token");
		return this;
	}
	
	public GisServerCriteria andTokenIsNotNull() {
		notNull("token");
		return this;
	}
	
	public GisServerCriteria andTokenIsEmpty() {
		empty("token");
		return this;
	}

	public GisServerCriteria andTokenIsNotEmpty() {
		notEmpty("token");
		return this;
	}
        public GisServerCriteria andTokenLike(java.lang.String value) {
    	   addCriterion("token", value, ConditionMode.FUZZY, "token", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andTokenNotLike(java.lang.String value) {
          addCriterion("token", value, ConditionMode.NOT_FUZZY, "token", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andTokenEqualTo(java.lang.String value) {
          addCriterion("token", value, ConditionMode.EQUAL, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenNotEqualTo(java.lang.String value) {
          addCriterion("token", value, ConditionMode.NOT_EQUAL, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenGreaterThan(java.lang.String value) {
          addCriterion("token", value, ConditionMode.GREATER_THEN, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("token", value, ConditionMode.GREATER_EQUAL, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenLessThan(java.lang.String value) {
          addCriterion("token", value, ConditionMode.LESS_THEN, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenLessThanOrEqualTo(java.lang.String value) {
          addCriterion("token", value, ConditionMode.LESS_EQUAL, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("token", value1, value2, ConditionMode.BETWEEN, "token", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andTokenNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("token", value1, value2, ConditionMode.NOT_BETWEEN, "token", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andTokenIn(List<java.lang.String> values) {
          addCriterion("token", values, ConditionMode.IN, "token", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andTokenNotIn(List<java.lang.String> values) {
          addCriterion("token", values, ConditionMode.NOT_IN, "token", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andSnIsNull() {
		isnull("sn");
		return this;
	}
	
	public GisServerCriteria andSnIsNotNull() {
		notNull("sn");
		return this;
	}
	
	public GisServerCriteria andSnIsEmpty() {
		empty("sn");
		return this;
	}

	public GisServerCriteria andSnIsNotEmpty() {
		notEmpty("sn");
		return this;
	}
        public GisServerCriteria andSnLike(java.lang.String value) {
    	   addCriterion("sn", value, ConditionMode.FUZZY, "sn", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andSnNotLike(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.NOT_FUZZY, "sn", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andSnEqualTo(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.EQUAL, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnNotEqualTo(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.NOT_EQUAL, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnGreaterThan(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.GREATER_THEN, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.GREATER_EQUAL, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnLessThan(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.LESS_THEN, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnLessThanOrEqualTo(java.lang.String value) {
          addCriterion("sn", value, ConditionMode.LESS_EQUAL, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("sn", value1, value2, ConditionMode.BETWEEN, "sn", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andSnNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("sn", value1, value2, ConditionMode.NOT_BETWEEN, "sn", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andSnIn(List<java.lang.String> values) {
          addCriterion("sn", values, ConditionMode.IN, "sn", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andSnNotIn(List<java.lang.String> values) {
          addCriterion("sn", values, ConditionMode.NOT_IN, "sn", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public GisServerCriteria andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public GisServerCriteria andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public GisServerCriteria andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
        public GisServerCriteria andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
	public GisServerCriteria andClusterIsNull() {
		isnull("cluster");
		return this;
	}
	
	public GisServerCriteria andClusterIsNotNull() {
		notNull("cluster");
		return this;
	}
	
	public GisServerCriteria andClusterIsEmpty() {
		empty("cluster");
		return this;
	}

	public GisServerCriteria andClusterIsNotEmpty() {
		notEmpty("cluster");
		return this;
	}
        public GisServerCriteria andClusterLike(java.lang.String value) {
    	   addCriterion("cluster", value, ConditionMode.FUZZY, "cluster", "java.lang.String", "String");
    	   return this;
      }

      public GisServerCriteria andClusterNotLike(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.NOT_FUZZY, "cluster", "java.lang.String", "String");
          return this;
      }
      public GisServerCriteria andClusterEqualTo(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.EQUAL, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterNotEqualTo(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.NOT_EQUAL, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterGreaterThan(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.GREATER_THEN, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.GREATER_EQUAL, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterLessThan(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.LESS_THEN, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterLessThanOrEqualTo(java.lang.String value) {
          addCriterion("cluster", value, ConditionMode.LESS_EQUAL, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("cluster", value1, value2, ConditionMode.BETWEEN, "cluster", "java.lang.String", "String");
    	  return this;
      }

      public GisServerCriteria andClusterNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("cluster", value1, value2, ConditionMode.NOT_BETWEEN, "cluster", "java.lang.String", "String");
          return this;
      }
        
      public GisServerCriteria andClusterIn(List<java.lang.String> values) {
          addCriterion("cluster", values, ConditionMode.IN, "cluster", "java.lang.String", "String");
          return this;
      }

      public GisServerCriteria andClusterNotIn(List<java.lang.String> values) {
          addCriterion("cluster", values, ConditionMode.NOT_IN, "cluster", "java.lang.String", "String");
          return this;
      }
}