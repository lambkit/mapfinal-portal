/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.cms.rpc.model;

import com.jfinal.kit.StrKit;
import com.lambkit.common.service.ServiceKit;
import com.lambkit.db.sql.column.Column;
import com.lambkit.module.cms.common.CmsConfig;
import com.lambkit.Lambkit;

import com.lambkit.module.cms.rpc.model.base.BaseCmsComment;
import com.lambkit.module.cms.rpc.model.sql.CmsCommentCriteria;
import com.lambkit.module.cms.rpc.api.CmsCommentService;
import com.lambkit.module.cms.rpc.service.impl.CmsCommentServiceImpl;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date 2019-10-28
 * @version 1.0
 * @since 1.0
 */
public class CmsComment extends BaseCmsComment<CmsComment> {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 状态(1:通过)
	 */
	public static final int STATUS_NORMAL = 1;
	/**
	 * 状态(0未审核)
	 */
    public static final int STATUS_DRAFT = 0;
    /**
     * 状态(-1:不通过)
     */
    public static final int STATUS_TRASH = -1;
    
	
	public static CmsCommentService service() {
		return ServiceKit.inject(CmsCommentService.class, CmsCommentServiceImpl.class);
	}
	
	public static CmsCommentCriteria sql() {
		return new CmsCommentCriteria();
	}
	
	public static CmsCommentCriteria sql(Column column) {
		CmsCommentCriteria that = new CmsCommentCriteria();
		that.add(column);
        return that;
    }
	
	public CmsComment() {
		CmsConfig config = Lambkit.config(CmsConfig.class);
		String dbconfig = config.getDbconfig();
		if(StrKit.notBlank(dbconfig)) {
			this.use(dbconfig);
		}
	}
}
