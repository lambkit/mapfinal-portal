# mapfinal

## 介绍
mapfinal（≈GeoServer Portal）是一个WebGIS内容管理平台，以PostGIS+GeoTools+GeoServer为基础的GIS服务平台，致力于给用户提供极速应用、快速开发的GIS服务平台。用户可以直接把mapfinal当作服务器使用，由mapfinal帮助管理一台或多台GeoServer服务器。

## 帮助文档

- [GeoApi](./doc/GeoApi.md)

## QQ交流群

374075649