package com.mapfinal.gis;

import com.mapfinal.portal.web.controller.MapfinalController;

public class GisController extends MapfinalController {

	@Override
	public void render(String view) {
		super.render("gis/" + view);
	}
	
	@Override
	public void renderTemplate(String template) {
		super.renderTemplate("gis/" + template);
	}
	
	@Override
	public void renderFreeMarker(String view) {
		super.renderFreeMarker("gis/" + view);
	}
}
