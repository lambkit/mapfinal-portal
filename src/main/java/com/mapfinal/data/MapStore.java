package com.mapfinal.data;

import java.util.List;
import java.util.Map;

/**
 * 存储
 * @author yangyong
 *
 */
public interface MapStore {
	/**
	 * 存储名称
	 * @return
	 */
	String getName();
	/**
	 * 存储类型
	 * @return
	 */
	StoreType getType();

	/**
	 * 图层列表
	 * @return
	 */
	List<MapLayer> getLayers();
	/**
	 * 加入图层
	 * @param layer
	 */
	void addLayer(MapLayer layer);
	
	/**
	 * 获取图层
	 * @param name
	 * @return
	 */
	MapLayer getLayer(String name);
	
	/**
	 * 获取未发布的数据列表
	 * @return
	 */
	List<String> getDatas();
	
	/**
	 * 发布数据
	 * @param dataName
	 * @param layer
	 */
	void publish(String dataName, MapLayer layer);
	
	/**
	 * 发送前台信息
	 * @return
	 */
	Map<String, Object> info();
}
