package com.mapfinal.gis;

import com.jfinal.config.Routes;
import com.lambkit.module.cms.web.interceptor.CmsWebInterceptor;
import com.mapfinal.gis.controller.ApiController;
import com.mapfinal.gis.controller.IndexController;
import com.mapfinal.gis.controller.LayerApiController;
import com.mapfinal.gis.controller.LayerController;
import com.mapfinal.gis.controller.LayerFileShapeController;
import com.mapfinal.gis.controller.LayerGroupController;
import com.mapfinal.gis.controller.LayerTablePostgisController;
import com.mapfinal.gis.controller.MapController;
import com.mapfinal.gis.controller.MapLayerController;
import com.mapfinal.gis.controller.MapServerController;
import com.mapfinal.gis.controller.MapStyleController;
import com.mapfinal.gis.controller.ServerApiController;
import com.mapfinal.gis.controller.ServerController;
import com.mapfinal.gis.controller.ServerLayerController;
import com.mapfinal.gis.controller.StyleController;

/**
 * 管理
 * @author yangyong
 *
 */
public class GisRoute extends Routes {

	@Override
	public void config() {
		// TODO Auto-generated method stub
		addInterceptor(new CmsWebInterceptor());
		add("/gis", IndexController.class);
		add("/gis/api", ApiController.class);
		add("/gis/layer", LayerController.class);
		add("/gis/layerApi", LayerApiController.class);
		add("/gis/layerFileShape", LayerFileShapeController.class);
		add("/gis/layerGroup", LayerGroupController.class);
		add("/gis/layerTablePostgis", LayerTablePostgisController.class);
		add("/gis/map", MapController.class);
		add("/gis/mapLayer", MapLayerController.class);
		add("/gis/mapServer", MapServerController.class);
		add("/gis/mapStyle", MapStyleController.class);
		add("/gis/server", ServerController.class);
		add("/gis/serverApi", ServerApiController.class);
		add("/gis/serverLayer", ServerLayerController.class);
		add("/gis/style", StyleController.class);
	}

}
