-- public.cms_action_log definition

-- Drop table

-- DROP TABLE public.cms_action_log;

CREATE TABLE public.cms_action_log (
	id bigserial NOT NULL,
	user_id int8 NULL, -- 用户ID
	anonym varchar(32) NULL, -- 匿名标识
	action_key varchar(512) NULL, -- 访问路径
	action_query varchar(512) NULL, -- 访问参数
	action_name varchar(128) NULL, -- 访问路径名称
	"source" varchar(32) NULL, -- 渠道
	medium varchar(32) NULL, --  媒介
	campaign varchar(128) NULL,
	"content" varchar(128) NULL, -- 来源内容
	term varchar(256) NULL, -- 关键词
	ip varchar(64) NULL, -- IP
	agent varchar(1024) NULL, -- 浏览器
	referer varchar(1024) NULL, -- 来源的url
	se varchar(32) NULL, -- Search Engine 搜索引擎
	sek varchar(512) NULL, -- Search Engine Keyword 搜索引擎关键字
	device_id varchar(128) NULL, -- 设备ID
	platform varchar(128) NULL, -- 平台
	"system" varchar(128) NULL, -- 系统
	brand varchar(128) NULL, -- 硬件平台
	model varchar(128) NULL, -- 硬件型号
	network varchar(128) NULL, -- 网络情况
	created timestamp NULL, -- 创建时间
	CONSTRAINT cms_action_log_pkey PRIMARY KEY (id)
);
CREATE INDEX created ON cms_action_log USING btree (created);
CREATE INDEX user_id ON cms_action_log USING btree (user_id);
COMMENT ON TABLE public.cms_action_log IS '用户行为记录表';

-- Column comments

COMMENT ON COLUMN public.cms_action_log.user_id IS '用户ID';
COMMENT ON COLUMN public.cms_action_log.anonym IS '匿名标识';
COMMENT ON COLUMN public.cms_action_log.action_key IS '访问路径';
COMMENT ON COLUMN public.cms_action_log.action_query IS '访问参数';
COMMENT ON COLUMN public.cms_action_log.action_name IS '访问路径名称';
COMMENT ON COLUMN public.cms_action_log."source" IS '渠道';
COMMENT ON COLUMN public.cms_action_log.medium IS ' 媒介';
COMMENT ON COLUMN public.cms_action_log."content" IS '来源内容';
COMMENT ON COLUMN public.cms_action_log.term IS '关键词';
COMMENT ON COLUMN public.cms_action_log.ip IS 'IP';
COMMENT ON COLUMN public.cms_action_log.agent IS '浏览器';
COMMENT ON COLUMN public.cms_action_log.referer IS '来源的url';
COMMENT ON COLUMN public.cms_action_log.se IS 'Search Engine 搜索引擎';
COMMENT ON COLUMN public.cms_action_log.sek IS 'Search Engine Keyword 搜索引擎关键字';
COMMENT ON COLUMN public.cms_action_log.device_id IS '设备ID';
COMMENT ON COLUMN public.cms_action_log.platform IS '平台';
COMMENT ON COLUMN public.cms_action_log."system" IS '系统';
COMMENT ON COLUMN public.cms_action_log.brand IS '硬件平台';
COMMENT ON COLUMN public.cms_action_log.model IS '硬件型号';
COMMENT ON COLUMN public.cms_action_log.network IS '网络情况';
COMMENT ON COLUMN public.cms_action_log.created IS '创建时间';


-- public.cms_ad definition

-- Drop table

-- DROP TABLE public.cms_ad;

CREATE TABLE public.cms_ad (
	id bigserial NOT NULL, -- 广告id
	"name" varchar(255) NOT NULL,
	"content" text NULL,
	status varchar(32) NOT NULL, -- 状态，1显示，0不显示
	CONSTRAINT cms_ad_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.cms_ad.id IS '广告id';
COMMENT ON COLUMN public.cms_ad.status IS '状态，1显示，0不显示';


-- public.cms_article definition

-- Drop table

-- DROP TABLE public.cms_article;

CREATE TABLE public.cms_article (
	id bigserial NOT NULL, -- 主键ID
	pid int4 NULL, -- 子版本的文章id
	slug varchar(128) NULL, -- slug
	title varchar(256) NULL, -- 标题
	"content" text NULL, -- 内容
	edit_mode varchar(32) NULL, -- 编辑模式，默认为html，其他可选项包括html，markdown ..
	summary text NULL, -- 摘要
	link_to varchar(256) NULL, -- 连接到(常用于谋文章只是一个连接)
	fromurl varchar(300) NULL,
	thumbnail varchar(128) NULL, -- 缩略图
	"style" varchar(32) NULL, -- 样式
	user_id int8 NULL, -- 用户ID
	order_number int8 NULL, -- 排序编号
	status varchar(32) NULL, -- 状态
	comment_status int2 NULL, -- 评论状态，默认允许评论
	comment_count int8 NULL, -- 评论总数
	comment_time timestamp NULL, -- 最后评论时间
	view_count int8 NULL, -- 访问量
	created timestamp NULL, -- 创建日期
	modified timestamp NULL, -- 最后更新日期
	flag varchar(256) NULL, -- 标识，通常用于对某几篇文章进行标识，从而实现单独查询
	meta_keywords varchar(512) NULL, -- SEO关键字
	meta_description varchar(512) NULL, -- SEO描述信息
	remarks text NULL, -- 备注信息
	CONSTRAINT cms_article_pkey PRIMARY KEY (id)
);
CREATE INDEX cms_article_created_idx ON cms_article USING btree (created);
CREATE INDEX cms_article_user_id_idx ON cms_article USING btree (user_id);
CREATE INDEX cms_article_view_count_idx ON cms_article USING btree (view_count);
CREATE UNIQUE INDEX slug ON cms_article USING btree (slug);
COMMENT ON TABLE public.cms_article IS '文章表';

-- Column comments

COMMENT ON COLUMN public.cms_article.id IS '主键ID';
COMMENT ON COLUMN public.cms_article.pid IS '子版本的文章id';
COMMENT ON COLUMN public.cms_article.slug IS 'slug';
COMMENT ON COLUMN public.cms_article.title IS '标题';
COMMENT ON COLUMN public.cms_article."content" IS '内容';
COMMENT ON COLUMN public.cms_article.edit_mode IS '编辑模式，默认为html，其他可选项包括html，markdown ..';
COMMENT ON COLUMN public.cms_article.summary IS '摘要';
COMMENT ON COLUMN public.cms_article.link_to IS '连接到(常用于谋文章只是一个连接)';
COMMENT ON COLUMN public.cms_article.thumbnail IS '缩略图';
COMMENT ON COLUMN public.cms_article."style" IS '样式';
COMMENT ON COLUMN public.cms_article.user_id IS '用户ID';
COMMENT ON COLUMN public.cms_article.order_number IS '排序编号';
COMMENT ON COLUMN public.cms_article.status IS '状态';
COMMENT ON COLUMN public.cms_article.comment_status IS '评论状态，默认允许评论';
COMMENT ON COLUMN public.cms_article.comment_count IS '评论总数';
COMMENT ON COLUMN public.cms_article.comment_time IS '最后评论时间';
COMMENT ON COLUMN public.cms_article.view_count IS '访问量';
COMMENT ON COLUMN public.cms_article.created IS '创建日期';
COMMENT ON COLUMN public.cms_article.modified IS '最后更新日期';
COMMENT ON COLUMN public.cms_article.flag IS '标识，通常用于对某几篇文章进行标识，从而实现单独查询';
COMMENT ON COLUMN public.cms_article.meta_keywords IS 'SEO关键字';
COMMENT ON COLUMN public.cms_article.meta_description IS 'SEO描述信息';
COMMENT ON COLUMN public.cms_article.remarks IS '备注信息';


-- public.cms_article_category definition

-- Drop table

-- DROP TABLE public.cms_article_category;

CREATE TABLE public.cms_article_category (
	id bigserial NOT NULL, -- 主键ID
	pid int8 NOT NULL, -- 父级分类的ID
	user_id int4 NULL, -- 分类创建的用户ID
	slug varchar(128) NULL, -- slug
	title varchar(512) NULL, -- 标题
	"content" text NULL, -- 内容描述
	summary text NULL, -- 摘要
	"style" varchar(32) NULL, -- 模板样式
	"type" varchar(32) NULL, -- 类型，比如：分类、tag、专题
	icon varchar(128) NULL, -- 图标
	count int8 NULL, -- 该分类的内容数量
	order_number int4 NULL, -- 排序编码
	flag varchar(256) NULL, -- 标识
	meta_keywords varchar(256) NULL, -- SEO关键字
	meta_description varchar(256) NULL, -- SEO描述内容
	created timestamp NULL, -- 创建日期
	modified timestamp NULL, -- 修改日期
	CONSTRAINT cms_article_category_pkey PRIMARY KEY (id)
);
CREATE INDEX cms_article_category_slug_type_idx ON cms_article_category USING btree (slug, type);
COMMENT ON TABLE public.cms_article_category IS '文章分类表。标签、专题、类别等都属于category。';

-- Column comments

COMMENT ON COLUMN public.cms_article_category.id IS '主键ID';
COMMENT ON COLUMN public.cms_article_category.pid IS '父级分类的ID';
COMMENT ON COLUMN public.cms_article_category.user_id IS '分类创建的用户ID';
COMMENT ON COLUMN public.cms_article_category.slug IS 'slug';
COMMENT ON COLUMN public.cms_article_category.title IS '标题';
COMMENT ON COLUMN public.cms_article_category."content" IS '内容描述';
COMMENT ON COLUMN public.cms_article_category.summary IS '摘要';
COMMENT ON COLUMN public.cms_article_category."style" IS '模板样式';
COMMENT ON COLUMN public.cms_article_category."type" IS '类型，比如：分类、tag、专题';
COMMENT ON COLUMN public.cms_article_category.icon IS '图标';
COMMENT ON COLUMN public.cms_article_category.count IS '该分类的内容数量';
COMMENT ON COLUMN public.cms_article_category.order_number IS '排序编码';
COMMENT ON COLUMN public.cms_article_category.flag IS '标识';
COMMENT ON COLUMN public.cms_article_category.meta_keywords IS 'SEO关键字';
COMMENT ON COLUMN public.cms_article_category.meta_description IS 'SEO描述内容';
COMMENT ON COLUMN public.cms_article_category.created IS '创建日期';
COMMENT ON COLUMN public.cms_article_category.modified IS '修改日期';


-- public.cms_article_category_mapping definition

-- Drop table

-- DROP TABLE public.cms_article_category_mapping;

CREATE TABLE public.cms_article_category_mapping (
	article_id int8 NOT NULL, -- 文章ID
	category_id int8 NOT NULL, -- 分类ID
	CONSTRAINT cms_article_category_mapping_pkey PRIMARY KEY (article_id, category_id)
);
COMMENT ON TABLE public.cms_article_category_mapping IS '文章和分类的多对多关系表';

-- Column comments

COMMENT ON COLUMN public.cms_article_category_mapping.article_id IS '文章ID';
COMMENT ON COLUMN public.cms_article_category_mapping.category_id IS '分类ID';


-- public.cms_article_category_system_mapping definition

-- Drop table

-- DROP TABLE public.cms_article_category_system_mapping;

CREATE TABLE public.cms_article_category_system_mapping (
	category_id int8 NOT NULL, -- 文章ID
	system_id int8 NOT NULL, -- 分类ID
	CONSTRAINT cms_article_category_system_mapping_pkey PRIMARY KEY (category_id, system_id)
);
COMMENT ON TABLE public.cms_article_category_system_mapping IS '文章和分类的多对多关系表';

-- Column comments

COMMENT ON COLUMN public.cms_article_category_system_mapping.category_id IS '文章ID';
COMMENT ON COLUMN public.cms_article_category_system_mapping.system_id IS '分类ID';


-- public.cms_article_category_tag_mapping definition

-- Drop table

-- DROP TABLE public.cms_article_category_tag_mapping;

CREATE TABLE public.cms_article_category_tag_mapping (
	category_id int8 NOT NULL, -- 文章ID
	tag_id int8 NOT NULL, -- 分类ID
	CONSTRAINT cms_article_category_tag_mapping_pkey PRIMARY KEY (category_id, tag_id)
);
COMMENT ON TABLE public.cms_article_category_tag_mapping IS '文章和分类的多对多关系表';

-- Column comments

COMMENT ON COLUMN public.cms_article_category_tag_mapping.category_id IS '文章ID';
COMMENT ON COLUMN public.cms_article_category_tag_mapping.tag_id IS '分类ID';


-- public.cms_article_comment definition

-- Drop table

-- DROP TABLE public.cms_article_comment;

CREATE TABLE public.cms_article_comment (
	id bigserial NOT NULL, -- 主键ID
	pid int8 NULL, -- 回复的评论ID
	article_id int8 NULL, -- 评论的内容ID
	user_id int8 NULL, -- 评论的用户ID
	author varchar(128) NULL, -- 评论的作者
	email varchar(64) NULL, -- 邮箱
	wechat varchar(64) NULL, -- 微信号
	qq varchar(32) NULL, -- qq号
	"content" text NULL, -- 评论的内容
	reply_count int8 NULL, -- 评论的回复数量
	order_number int8 NULL, -- 排序编号，常用语置顶等
	vote_up int8 NULL, -- “顶”的数量
	vote_down int8 NULL, -- “踩”的数量
	status varchar(32) NULL, -- 评论的状态
	created timestamp NULL, -- 评论的时间
	CONSTRAINT cms_article_comment_pkey PRIMARY KEY (id)
);
CREATE INDEX cms_article_comment_article_id_idx ON cms_article_comment USING btree (article_id);
CREATE INDEX cms_article_comment_user_id_idx ON cms_article_comment USING btree (user_id);
COMMENT ON TABLE public.cms_article_comment IS '文章评论表';

-- Column comments

COMMENT ON COLUMN public.cms_article_comment.id IS '主键ID';
COMMENT ON COLUMN public.cms_article_comment.pid IS '回复的评论ID';
COMMENT ON COLUMN public.cms_article_comment.article_id IS '评论的内容ID';
COMMENT ON COLUMN public.cms_article_comment.user_id IS '评论的用户ID';
COMMENT ON COLUMN public.cms_article_comment.author IS '评论的作者';
COMMENT ON COLUMN public.cms_article_comment.email IS '邮箱';
COMMENT ON COLUMN public.cms_article_comment.wechat IS '微信号';
COMMENT ON COLUMN public.cms_article_comment.qq IS 'qq号';
COMMENT ON COLUMN public.cms_article_comment."content" IS '评论的内容';
COMMENT ON COLUMN public.cms_article_comment.reply_count IS '评论的回复数量';
COMMENT ON COLUMN public.cms_article_comment.order_number IS '排序编号，常用语置顶等';
COMMENT ON COLUMN public.cms_article_comment.vote_up IS '“顶”的数量';
COMMENT ON COLUMN public.cms_article_comment.vote_down IS '“踩”的数量';
COMMENT ON COLUMN public.cms_article_comment.status IS '评论的状态';
COMMENT ON COLUMN public.cms_article_comment.created IS '评论的时间';


-- public.cms_article_tag_mapping definition

-- Drop table

-- DROP TABLE public.cms_article_tag_mapping;

CREATE TABLE public.cms_article_tag_mapping (
	article_id int8 NOT NULL, -- 文章ID
	tag_id int8 NOT NULL, -- 分类ID
	CONSTRAINT cms_article_tag_mapping_pkey PRIMARY KEY (article_id, tag_id)
);
COMMENT ON TABLE public.cms_article_tag_mapping IS '文章和分类的多对多关系表';

-- Column comments

COMMENT ON COLUMN public.cms_article_tag_mapping.article_id IS '文章ID';
COMMENT ON COLUMN public.cms_article_tag_mapping.tag_id IS '分类ID';


-- public.cms_attachment definition

-- Drop table

-- DROP TABLE public.cms_attachment;

CREATE TABLE public.cms_attachment (
	id bigserial NOT NULL, -- ID主键
	user_id int8 NULL, -- 上传附件的用户ID
	title text NULL, -- 标题
	description text NULL, -- 附件描述
	"path" varchar(512) NULL, -- 路径
	mime_type varchar(128) NULL, -- mime
	suffix varchar(32) NULL, -- 附件的后缀
	"type" varchar(32) NULL, -- 类型
	flag varchar(256) NULL, -- 标示
	order_number int4 NULL, -- 排序字段
	accessible int2 NOT NULL, -- 是否可以被访问
	created timestamp NULL, -- 上传时间
	modified timestamp NULL, -- 修改时间
	CONSTRAINT cms_attachment_pkey PRIMARY KEY (id)
);
CREATE INDEX cms_attachment_created_idx ON cms_attachment USING btree (created);
CREATE INDEX cms_attachment_mime_type_idx ON cms_attachment USING btree (mime_type);
CREATE INDEX cms_attachment_suffix_idx ON cms_attachment USING btree (suffix);
CREATE INDEX cms_attachment_user_id_idx ON cms_attachment USING btree (user_id);
COMMENT ON TABLE public.cms_attachment IS '附件表，用于保存用户上传的附件内容。';

-- Column comments

COMMENT ON COLUMN public.cms_attachment.id IS 'ID主键';
COMMENT ON COLUMN public.cms_attachment.user_id IS '上传附件的用户ID';
COMMENT ON COLUMN public.cms_attachment.title IS '标题';
COMMENT ON COLUMN public.cms_attachment.description IS '附件描述';
COMMENT ON COLUMN public.cms_attachment."path" IS '路径';
COMMENT ON COLUMN public.cms_attachment.mime_type IS 'mime';
COMMENT ON COLUMN public.cms_attachment.suffix IS '附件的后缀';
COMMENT ON COLUMN public.cms_attachment."type" IS '类型';
COMMENT ON COLUMN public.cms_attachment.flag IS '标示';
COMMENT ON COLUMN public.cms_attachment.order_number IS '排序字段';
COMMENT ON COLUMN public.cms_attachment.accessible IS '是否可以被访问';
COMMENT ON COLUMN public.cms_attachment.created IS '上传时间';
COMMENT ON COLUMN public.cms_attachment.modified IS '修改时间';


-- public.cms_document definition

-- Drop table

-- DROP TABLE public.cms_document;

CREATE TABLE public.cms_document (
	id bigserial NOT NULL, -- 主菜单
	pid int8 NOT NULL, -- 子菜单
	user_id int8 NULL,
	title varchar(300) NOT NULL,
	"content" text NOT NULL,
	created timestamp NOT NULL,
	modified timestamp NOT NULL,
	status int2 NOT NULL,
	CONSTRAINT cms_document_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.cms_document.id IS '主菜单';
COMMENT ON COLUMN public.cms_document.pid IS '子菜单';


-- public.cms_guestbook definition

-- Drop table

-- DROP TABLE public.cms_guestbook;

CREATE TABLE public.cms_guestbook (
	id bigserial NOT NULL,
	full_name varchar(50) NOT NULL, -- 留言者姓名
	email varchar(100) NOT NULL, -- 留言者邮箱
	title varchar(255) NULL, -- 留言标题
	msg text NOT NULL, -- 留言内容
	createtime timestamp NOT NULL,
	status varchar(32) NOT NULL,
	CONSTRAINT cms_guestbook_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.cms_guestbook.full_name IS '留言者姓名';
COMMENT ON COLUMN public.cms_guestbook.email IS '留言者邮箱';
COMMENT ON COLUMN public.cms_guestbook.title IS '留言标题';
COMMENT ON COLUMN public.cms_guestbook.msg IS '留言内容';


-- public.cms_links definition

-- Drop table

-- DROP TABLE public.cms_links;

CREATE TABLE public.cms_links (
	id bigserial NOT NULL,
	url varchar(255) NOT NULL, -- 友情链接地址
	"name" varchar(255) NOT NULL, -- 友情链接名称
	image varchar(255) NULL, -- 友情链接图标
	target varchar(25) NOT NULL, -- 友情链接打开方式
	description text NOT NULL, -- 友情链接描述
	status varchar(32) NOT NULL,
	rating int4 NOT NULL, -- 友情链接评级
	rel varchar(255) NULL,
	listorder int4 NOT NULL, -- 排序
	CONSTRAINT cms_links_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.cms_links.url IS '友情链接地址';
COMMENT ON COLUMN public.cms_links."name" IS '友情链接名称';
COMMENT ON COLUMN public.cms_links.image IS '友情链接图标';
COMMENT ON COLUMN public.cms_links.target IS '友情链接打开方式';
COMMENT ON COLUMN public.cms_links.description IS '友情链接描述';
COMMENT ON COLUMN public.cms_links.rating IS '友情链接评级';
COMMENT ON COLUMN public.cms_links.listorder IS '排序';


-- public.cms_menu definition

-- Drop table

-- DROP TABLE public.cms_menu;

CREATE TABLE public.cms_menu (
	id bigserial NOT NULL, -- 菜单ID
	cid int8 NULL,
	pid int8 NULL, -- 父级ID
	"text" varchar(128) NULL, -- 文本内容
	url varchar(512) NULL, -- 链接的url
	target varchar(32) NULL, -- 打开的方式
	icon varchar(64) NULL, -- 菜单的icon
	flag varchar(32) NULL, -- 菜单标识
	"type" varchar(32) NULL, -- 菜单类型：主菜单、顶部菜单、底部菜单
	order_number int4 NULL, -- 排序字段
	relative_table varchar(32) NULL, -- 该菜单是否和其他表关联
	relative_id int8 NULL, -- 关联的具体数据id
	created timestamp NULL, -- 创建时间
	modified timestamp NULL, -- 修改时间
	CONSTRAINT cms_menu_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_menu IS '菜单表';

-- Column comments

COMMENT ON COLUMN public.cms_menu.id IS '菜单ID';
COMMENT ON COLUMN public.cms_menu.pid IS '父级ID';
COMMENT ON COLUMN public.cms_menu."text" IS '文本内容';
COMMENT ON COLUMN public.cms_menu.url IS '链接的url';
COMMENT ON COLUMN public.cms_menu.target IS '打开的方式';
COMMENT ON COLUMN public.cms_menu.icon IS '菜单的icon';
COMMENT ON COLUMN public.cms_menu.flag IS '菜单标识';
COMMENT ON COLUMN public.cms_menu."type" IS '菜单类型：主菜单、顶部菜单、底部菜单';
COMMENT ON COLUMN public.cms_menu.order_number IS '排序字段';
COMMENT ON COLUMN public.cms_menu.relative_table IS '该菜单是否和其他表关联';
COMMENT ON COLUMN public.cms_menu.relative_id IS '关联的具体数据id';
COMMENT ON COLUMN public.cms_menu.created IS '创建时间';
COMMENT ON COLUMN public.cms_menu.modified IS '修改时间';


-- public.cms_menu_cat definition

-- Drop table

-- DROP TABLE public.cms_menu_cat;

CREATE TABLE public.cms_menu_cat (
	id bigserial NOT NULL,
	"name" varchar(255) NOT NULL,
	system_id int8 NOT NULL,
	remark text NULL,
	active int4 NULL,
	CONSTRAINT cms_menu_cat_pkey PRIMARY KEY (id)
);


-- public.cms_option definition

-- Drop table

-- DROP TABLE public.cms_option;

CREATE TABLE public.cms_option (
	id bigserial NOT NULL, -- 主键ID
	"key" varchar(128) NULL, -- 配置KEY
	value text NULL, -- 配置内容
	CONSTRAINT cms_option_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_option IS '配置信息表，用来保存网站的所有配置信息。';

-- Column comments

COMMENT ON COLUMN public.cms_option.id IS '主键ID';
COMMENT ON COLUMN public.cms_option."key" IS '配置KEY';
COMMENT ON COLUMN public.cms_option.value IS '配置内容';


-- public.cms_plugins definition

-- Drop table

-- DROP TABLE public.cms_plugins;

CREATE TABLE public.cms_plugins (
	id bigserial NOT NULL, -- 自增id
	"name" varchar(50) NOT NULL, -- 插件名，英文
	title varchar(50) NOT NULL, -- 插件名称
	description text NULL, -- 插件描述
	"type" int2 NULL, -- 插件类型, 1:网站；8;微信
	status varchar(32) NOT NULL, -- 状态；1开启；
	config text NULL, -- 插件配置
	hooks varchar(255) NULL, -- 实现的钩子;以“，”分隔
	has_admin int2 NULL, -- 插件是否有后台管理界面
	author varchar(50) NULL, -- 插件作者
	"version" varchar(20) NULL, -- 插件版本号
	createtime int8 NOT NULL, -- 插件安装时间
	listorder int2 NOT NULL,
	CONSTRAINT cms_plugins_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_plugins IS '插件表';

-- Column comments

COMMENT ON COLUMN public.cms_plugins.id IS '自增id';
COMMENT ON COLUMN public.cms_plugins."name" IS '插件名，英文';
COMMENT ON COLUMN public.cms_plugins.title IS '插件名称';
COMMENT ON COLUMN public.cms_plugins.description IS '插件描述';
COMMENT ON COLUMN public.cms_plugins."type" IS '插件类型, 1:网站；8;微信';
COMMENT ON COLUMN public.cms_plugins.status IS '状态；1开启；';
COMMENT ON COLUMN public.cms_plugins.config IS '插件配置';
COMMENT ON COLUMN public.cms_plugins.hooks IS '实现的钩子;以“，”分隔';
COMMENT ON COLUMN public.cms_plugins.has_admin IS '插件是否有后台管理界面';
COMMENT ON COLUMN public.cms_plugins.author IS '插件作者';
COMMENT ON COLUMN public.cms_plugins."version" IS '插件版本号';
COMMENT ON COLUMN public.cms_plugins.createtime IS '插件安装时间';


-- public.cms_single_page definition

-- Drop table

-- DROP TABLE public.cms_single_page;

CREATE TABLE public.cms_single_page (
	id bigserial NOT NULL, -- 主键ID
	slug varchar(128) NULL, -- slug
	title text NULL, -- 标题
	"content" text NULL, -- 内容
	edit_mode varchar(32) NULL, -- 编辑模式：html可视化，markdown ..
	link_to varchar(512) NULL, -- 链接
	summary text NULL, -- 摘要
	thumbnail varchar(128) NULL, -- 缩略图
	"style" varchar(32) NULL, -- 样式
	flag varchar(32) NULL, -- 标识
	status varchar(32) NOT NULL, -- 状态
	view_count int8 NOT NULL, -- 访问量
	created timestamp NULL, -- 创建日期
	modified timestamp NULL, -- 最后更新日期
	meta_keywords varchar(256) NULL, -- SEO关键字
	meta_description varchar(256) NULL, -- SEO描述信息
	remarks text NULL, -- 备注信息
	CONSTRAINT cms_single_page_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_single_page IS '单页表';

-- Column comments

COMMENT ON COLUMN public.cms_single_page.id IS '主键ID';
COMMENT ON COLUMN public.cms_single_page.slug IS 'slug';
COMMENT ON COLUMN public.cms_single_page.title IS '标题';
COMMENT ON COLUMN public.cms_single_page."content" IS '内容';
COMMENT ON COLUMN public.cms_single_page.edit_mode IS '编辑模式：html可视化，markdown ..';
COMMENT ON COLUMN public.cms_single_page.link_to IS '链接';
COMMENT ON COLUMN public.cms_single_page.summary IS '摘要';
COMMENT ON COLUMN public.cms_single_page.thumbnail IS '缩略图';
COMMENT ON COLUMN public.cms_single_page."style" IS '样式';
COMMENT ON COLUMN public.cms_single_page.flag IS '标识';
COMMENT ON COLUMN public.cms_single_page.status IS '状态';
COMMENT ON COLUMN public.cms_single_page.view_count IS '访问量';
COMMENT ON COLUMN public.cms_single_page.created IS '创建日期';
COMMENT ON COLUMN public.cms_single_page.modified IS '最后更新日期';
COMMENT ON COLUMN public.cms_single_page.meta_keywords IS 'SEO关键字';
COMMENT ON COLUMN public.cms_single_page.meta_description IS 'SEO描述信息';
COMMENT ON COLUMN public.cms_single_page.remarks IS '备注信息';


-- public.cms_slide definition

-- Drop table

-- DROP TABLE public.cms_slide;

CREATE TABLE public.cms_slide (
	id bigserial NOT NULL,
	cid int8 NOT NULL,
	"name" varchar(255) NOT NULL,
	pic varchar(255) NULL,
	url varchar(255) NULL,
	des varchar(255) NULL,
	"content" text NULL,
	status varchar(32) NOT NULL,
	listorder int4 NULL,
	CONSTRAINT cms_slide_pkey PRIMARY KEY (id)
);


-- public.cms_slide_cat definition

-- Drop table

-- DROP TABLE public.cms_slide_cat;

CREATE TABLE public.cms_slide_cat (
	id bigserial NOT NULL,
	"name" varchar(255) NOT NULL,
	idname varchar(255) NOT NULL,
	remark text NULL,
	status int4 NOT NULL,
	CONSTRAINT cms_slide_cat_pkey PRIMARY KEY (id)
);


-- public.cms_system definition

-- Drop table

-- DROP TABLE public.cms_system;

CREATE TABLE public.cms_system (
	id bigserial NOT NULL, -- 编号
	"name" varchar(20) NOT NULL, -- 系统名称
	code varchar(20) NULL, -- 别名
	description varchar(300) NULL, -- 描述
	ctime int8 NULL, -- 创建时间
	orders int8 NULL, -- 排序
	CONSTRAINT cms_system_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_system IS '系统管理';

-- Column comments

COMMENT ON COLUMN public.cms_system.id IS '编号';
COMMENT ON COLUMN public.cms_system."name" IS '系统名称';
COMMENT ON COLUMN public.cms_system.code IS '别名';
COMMENT ON COLUMN public.cms_system.description IS '描述';
COMMENT ON COLUMN public.cms_system.ctime IS '创建时间';
COMMENT ON COLUMN public.cms_system.orders IS '排序';


-- public.cms_tag definition

-- Drop table

-- DROP TABLE public.cms_tag;

CREATE TABLE public.cms_tag (
	id bigserial NOT NULL, -- 标签编号
	pid int8 NULL,
	"name" varchar(20) NOT NULL, -- 名称
	description varchar(200) NULL, -- 描述
	icon varchar(50) NULL, -- 图标
	"type" int2 NOT NULL, -- 类型(1:普通,2:热门,3专题)
	alias varchar(20) NULL, -- 别名
	system_id int4 NULL, -- 所属系统
	ctime numeric(20) NOT NULL, -- 创建时间
	orders numeric(20) NOT NULL, -- 排序
	CONSTRAINT cms_tag_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_tag IS '标签表';

-- Column comments

COMMENT ON COLUMN public.cms_tag.id IS '标签编号';
COMMENT ON COLUMN public.cms_tag."name" IS '名称';
COMMENT ON COLUMN public.cms_tag.description IS '描述';
COMMENT ON COLUMN public.cms_tag.icon IS '图标';
COMMENT ON COLUMN public.cms_tag."type" IS '类型(1:普通,2:热门,3专题)';
COMMENT ON COLUMN public.cms_tag.alias IS '别名';
COMMENT ON COLUMN public.cms_tag.system_id IS '所属系统';
COMMENT ON COLUMN public.cms_tag.ctime IS '创建时间';
COMMENT ON COLUMN public.cms_tag.orders IS '排序';


-- public.cms_wechat_menu definition

-- Drop table

-- DROP TABLE public.cms_wechat_menu;

CREATE TABLE public.cms_wechat_menu (
	id bigserial NOT NULL,
	pid int8 NULL, -- 父级ID
	"text" varchar(512) NULL, -- 文本内容
	keyword varchar(128) NULL, -- 关键字
	"type" varchar(32) NULL, -- 菜单类型
	order_number int4 NULL, -- 排序字段
	created timestamp NULL, -- 创建时间
	modified timestamp NULL, -- 修改时间
	CONSTRAINT cms_wechat_menu_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_wechat_menu IS '微信公众号菜单表';

-- Column comments

COMMENT ON COLUMN public.cms_wechat_menu.pid IS '父级ID';
COMMENT ON COLUMN public.cms_wechat_menu."text" IS '文本内容';
COMMENT ON COLUMN public.cms_wechat_menu.keyword IS '关键字';
COMMENT ON COLUMN public.cms_wechat_menu."type" IS '菜单类型';
COMMENT ON COLUMN public.cms_wechat_menu.order_number IS '排序字段';
COMMENT ON COLUMN public.cms_wechat_menu.created IS '创建时间';
COMMENT ON COLUMN public.cms_wechat_menu.modified IS '修改时间';


-- public.cms_wechat_reply definition

-- Drop table

-- DROP TABLE public.cms_wechat_reply;

CREATE TABLE public.cms_wechat_reply (
	id bigserial NOT NULL,
	keyword varchar(128) NULL, -- 关键字
	"content" text NULL, -- 回复内容
	created timestamp NULL, -- 创建时间
	modified timestamp NULL, -- 修改时间
	CONSTRAINT cms_wechat_reply_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.cms_wechat_reply IS '用户自定义关键字回复表';

-- Column comments

COMMENT ON COLUMN public.cms_wechat_reply.keyword IS '关键字';
COMMENT ON COLUMN public.cms_wechat_reply."content" IS '回复内容';
COMMENT ON COLUMN public.cms_wechat_reply.created IS '创建时间';
COMMENT ON COLUMN public.cms_wechat_reply.modified IS '修改时间';


-- public.dic_area definition

-- Drop table

-- DROP TABLE public.dic_area;

CREATE TABLE public.dic_area (
	id varchar(45) NOT NULL,
	parent_id varchar(45) NULL,
	level_type varchar(45) NULL,
	"name" varchar(45) NULL,
	short_name varchar(45) NULL,
	parent_path varchar(45) NULL,
	province varchar(45) NULL,
	city varchar(45) NULL,
	district varchar(45) NULL,
	province_short_name varchar(45) NULL,
	city_short_name varchar(45) NULL,
	district_short_name varchar(45) NULL,
	province_pinyin varchar(45) NULL,
	city_pinyin varchar(45) NULL,
	district_pinyin varchar(45) NULL,
	city_code varchar(45) NULL,
	zip_code varchar(45) NULL,
	pinyin varchar(45) NULL,
	jianpin varchar(45) NULL,
	first_char varchar(45) NULL,
	lng varchar(45) NULL,
	lat varchar(45) NULL,
	remark1 varchar(45) NULL,
	remark2 varchar(45) NULL,
	CONSTRAINT dic_area_pkey PRIMARY KEY (id)
);


-- public.gis_api definition

-- Drop table

-- DROP TABLE public.gis_api;

CREATE TABLE public.gis_api (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	title varchar(255) NULL,
	url varchar(255) NULL,
	summary text NULL,
	"options" json NULL,
	"type" varchar(255) NULL,
	origin varchar(255) NULL,
	urlpattern varchar(255) NULL,
	"targetUri" varchar(255) NULL,
	viewcount int4 NULL,
	status varchar(255) NULL,
	CONSTRAINT gis_api_pkey PRIMARY KEY (id)
);


-- public.gis_layer definition

-- Drop table

-- DROP TABLE public.gis_layer;

CREATE TABLE public.gis_layer (
	layerkey varchar(32) NOT NULL,
	"name" varchar(255) NULL,
	title varchar(255) NULL,
	"groups" varchar(2) NULL DEFAULT '0'::character varying,
	data_type varchar(255) NULL,
	status varchar(2) NULL DEFAULT '0'::character varying,
	thumbnail varchar(255) NULL,
	ctime timestamp NULL,
	CONSTRAINT gis_layer_pkey PRIMARY KEY (layerkey)
);


-- public.gis_layer_api definition

-- Drop table

-- DROP TABLE public.gis_layer_api;

CREATE TABLE public.gis_layer_api (
	layerkey varchar(255) NOT NULL,
	api_id int8 NOT NULL,
	"type" varchar(255) NULL,
	CONSTRAINT gis_layer_api_pkey PRIMARY KEY (layerkey, api_id)
);


-- public.gis_layer_file_shape definition

-- Drop table

-- DROP TABLE public.gis_layer_file_shape;

CREATE TABLE public.gis_layer_file_shape (
	layerkey varchar(32) NOT NULL,
	sid int8 NULL,
	fileid int8 NOT NULL,
	"name" varchar(255) NULL,
	geofld varchar(255) NULL,
	fldtype varchar(255) NULL, -- 字段类型，经纬度，geom，wkt等
	geotype varchar(255) NULL, -- 地图类型，点、线、面
	crs varchar(255) NULL,
	CONSTRAINT gis_layer_file_shape_pkey PRIMARY KEY (layerkey, fileid)
);

-- Column comments

COMMENT ON COLUMN public.gis_layer_file_shape.fldtype IS '字段类型，经纬度，geom，wkt等';
COMMENT ON COLUMN public.gis_layer_file_shape.geotype IS '地图类型，点、线、面';


-- public.gis_layer_group definition

-- Drop table

-- DROP TABLE public.gis_layer_group;

CREATE TABLE public.gis_layer_group (
	layer_group_key varchar(32) NOT NULL,
	layer_key varchar(32) NOT NULL,
	ords varchar(255) NULL, -- 排序
	CONSTRAINT gis_layer_group_mapping_pkey PRIMARY KEY (layer_group_key, layer_key)
);

-- Column comments

COMMENT ON COLUMN public.gis_layer_group.ords IS '排序';


-- public.gis_layer_table_postgis definition

-- Drop table

-- DROP TABLE public.gis_layer_table_postgis;

CREATE TABLE public.gis_layer_table_postgis (
	layerkey varchar(32) NOT NULL,
	sid int8 NULL,
	tbid int8 NOT NULL,
	"name" varchar(255) NULL,
	geofld varchar(255) NULL,
	fldtype varchar(255) NULL, -- 字段类型，经纬度，geom，wkt等
	geotype varchar(255) NULL, -- 地图类型，点、线、面
	crs varchar(255) NULL,
	CONSTRAINT gis_layer_table_postgis_pkey PRIMARY KEY (layerkey, tbid)
);

-- Column comments

COMMENT ON COLUMN public.gis_layer_table_postgis.fldtype IS '字段类型，经纬度，geom，wkt等';
COMMENT ON COLUMN public.gis_layer_table_postgis.geotype IS '地图类型，点、线、面';


-- public.gis_map definition

-- Drop table

-- DROP TABLE public.gis_map;

CREATE TABLE public.gis_map (
	id bigserial NOT NULL,
	appid int8 NULL,
	title varchar(255) NULL,
	"name" varchar(255) NULL,
	user_id int8 NULL,
	"template" varchar(255) NULL,
	CONSTRAINT gis_map_pkey PRIMARY KEY (id)
);


-- public.gis_map_layer definition

-- Drop table

-- DROP TABLE public.gis_map_layer;

CREATE TABLE public.gis_map_layer (
	map_id int4 NOT NULL,
	layer_key varchar(32) NOT NULL,
	CONSTRAINT gis_map_layer_mapping_pkey PRIMARY KEY (map_id, layer_key)
);


-- public.gis_map_server definition

-- Drop table

-- DROP TABLE public.gis_map_server;

CREATE TABLE public.gis_map_server (
	map_id int4 NOT NULL,
	server_key varchar(32) NOT NULL,
	CONSTRAINT gis_map_server_pkey PRIMARY KEY (map_id, server_key)
);


-- public.gis_map_style definition

-- Drop table

-- DROP TABLE public.gis_map_style;

CREATE TABLE public.gis_map_style (
	map_id int4 NOT NULL,
	style_id int4 NOT NULL,
	CONSTRAINT gis_map_style_mapping_pkey PRIMARY KEY (map_id, style_id)
);


-- public.gis_server definition

-- Drop table

-- DROP TABLE public.gis_server;

CREATE TABLE public.gis_server (
	serverkey varchar(32) NOT NULL,
	"name" varchar(255) NULL,
	title varchar(255) NULL,
	"version" varchar(255) NULL,
	url varchar(255) NULL,
	"token" varchar(255) NULL,
	sn varchar(255) NULL,
	status varchar(2) NULL DEFAULT '0'::character varying,
	"cluster" varchar(2) NULL DEFAULT '0'::character varying,
	CONSTRAINT gis_server_pkey PRIMARY KEY (serverkey)
);


-- public.gis_server_api definition

-- Drop table

-- DROP TABLE public.gis_server_api;

CREATE TABLE public.gis_server_api (
	id bigserial NOT NULL,
	serverkey varchar(32) NULL,
	layerkey varchar(32) NULL,
	"name" varchar(255) NULL,
	title varchar(255) NULL,
	url varchar(255) NULL,
	"type" varchar(255) NULL,
	api_id int8 NULL,
	urlpattern varchar(255) NULL,
	"targetUri" varchar(255) NULL,
	status varchar(255) NULL,
	CONSTRAINT gis_server_layer_id_pkey PRIMARY KEY (id)
);


-- public.gis_server_layer definition

-- Drop table

-- DROP TABLE public.gis_server_layer;

CREATE TABLE public.gis_server_layer (
	server_key varchar(32) NOT NULL,
	layer_key varchar(32) NOT NULL,
	"name" varchar(255) NULL,
	url varchar(255) NULL,
	CONSTRAINT gis_server_layer_pkey PRIMARY KEY (server_key, layer_key)
);


-- public.gis_style definition

-- Drop table

-- DROP TABLE public.gis_style;

CREATE TABLE public.gis_style (
	id serial NOT NULL,
	"name" varchar(255) NULL,
	title varchar(255) NULL,
	"path" varchar(255) NULL,
	"type" varchar(255) NULL, -- local,url
	"owner" varchar(2) NULL,
	status varchar(2) NULL,
	suffix varchar(255) NULL, -- 后缀
	CONSTRAINT gis_style_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.gis_style."type" IS 'local,url';
COMMENT ON COLUMN public.gis_style.suffix IS '后缀';


-- public.map_city definition

-- Drop table

-- DROP TABLE public.map_city;

CREATE TABLE public.map_city (
	gid serial NOT NULL,
	"name" varchar(30) NULL,
	kind varchar(4) NULL,
	geom geometry(MULTIPOLYGON) NULL,
	code varchar(45) NULL,
	CONSTRAINT city_pkey PRIMARY KEY (gid)
);
CREATE INDEX city_geom_idx ON map_city USING gist (geom);


-- public.map_city_road definition

-- Drop table

-- DROP TABLE public.map_city_road;

CREATE TABLE public.map_city_road (
	gid serial NOT NULL,
	"name" varchar(100) NULL,
	kind varchar(30) NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_city_road_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_city_road_geom_idx ON map_city_road USING gist (geom);


-- public.map_county definition

-- Drop table

-- DROP TABLE public.map_county;

CREATE TABLE public.map_county (
	gid serial NOT NULL,
	"name" varchar(60) NULL,
	kind varchar(4) NULL,
	geom geometry(MULTIPOLYGON) NULL,
	code varchar(45) NULL,
	CONSTRAINT county_pkey PRIMARY KEY (gid)
);
CREATE INDEX county_geom_idx ON map_county USING gist (geom);


-- public.map_hyd1_4l definition

-- Drop table

-- DROP TABLE public.map_hyd1_4l;

CREATE TABLE public.map_hyd1_4l (
	gid serial NOT NULL,
	fnode_ float8 NULL,
	tnode_ float8 NULL,
	lpoly_ float8 NULL,
	rpoly_ float8 NULL,
	length float8 NULL,
	hyd1_4m_ float8 NULL,
	hyd1_4m_id float8 NULL,
	gbcode int4 NULL,
	"name" varchar(60) NULL,
	level_rive int2 NULL,
	level_lake int2 NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_hyd1_4l_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_hyd1_4l_geom_idx ON map_hyd1_4l USING gist (geom);


-- public.map_hyd1_4p definition

-- Drop table

-- DROP TABLE public.map_hyd1_4p;

CREATE TABLE public.map_hyd1_4p (
	gid serial NOT NULL,
	area float8 NULL,
	perimeter float8 NULL,
	hyd1_4m_ float8 NULL,
	hyd1_4m_id float8 NULL,
	gbcode int4 NULL,
	"name" varchar(30) NULL,
	level_lake int2 NULL,
	code_lake varchar(30) NULL,
	geom geometry(MULTIPOLYGON) NULL,
	CONSTRAINT gis_hyd1_4p_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_hyd1_4p_geom_idx ON map_hyd1_4p USING gist (geom);


-- public.map_hyd2_4l definition

-- Drop table

-- DROP TABLE public.map_hyd2_4l;

CREATE TABLE public.map_hyd2_4l (
	gid serial NOT NULL,
	fnode_ float8 NULL,
	tnode_ float8 NULL,
	lpoly_ float8 NULL,
	rpoly_ float8 NULL,
	length float8 NULL,
	hyd2_4m_ float8 NULL,
	hyd2_4m_id float8 NULL,
	gbcode int4 NULL,
	"name" varchar(60) NULL,
	level_rive int2 NULL,
	level_lake int2 NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_hyd2_4l_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_hyd2_4l_geom_idx ON map_hyd2_4l USING gist (geom);


-- public.map_hyd2_4p definition

-- Drop table

-- DROP TABLE public.map_hyd2_4p;

CREATE TABLE public.map_hyd2_4p (
	gid serial NOT NULL,
	area float8 NULL,
	perimeter float8 NULL,
	hyd2_4m_ float8 NULL,
	hyd2_4m_id float8 NULL,
	gbcode int4 NULL,
	"name" varchar(30) NULL,
	level_lake int2 NULL,
	code_lake varchar(30) NULL,
	geom geometry(MULTIPOLYGON) NULL,
	CONSTRAINT gis_hyd2_4p_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_hyd2_4p_geom_idx ON map_hyd2_4p USING gist (geom);


-- public.map_province definition

-- Drop table

-- DROP TABLE public.map_province;

CREATE TABLE public.map_province (
	gid serial NOT NULL,
	"name" varchar(40) NULL,
	kind varchar(23) NULL,
	geom geometry(MULTIPOLYGON) NULL,
	code varchar(45) NULL,
	CONSTRAINT province_pkey PRIMARY KEY (gid)
);
CREATE INDEX province_geom_idx ON map_province USING gist (geom);


-- public.map_rai_4m definition

-- Drop table

-- DROP TABLE public.map_rai_4m;

CREATE TABLE public.map_rai_4m (
	gid serial NOT NULL,
	fnode_ float8 NULL,
	tnode_ float8 NULL,
	lpoly_ float8 NULL,
	rpoly_ float8 NULL,
	length float8 NULL,
	rai_4m_ float8 NULL,
	rai_4m_id float8 NULL,
	gbcode int4 NULL,
	"name" varchar(10) NULL,
	pinyin varchar(30) NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_rai_4m_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_rai_4m_geom_idx ON map_rai_4m USING gist (geom);


-- public.map_river4_polyline definition

-- Drop table

-- DROP TABLE public.map_river4_polyline;

CREATE TABLE public.map_river4_polyline (
	gid serial NOT NULL,
	fnode_ float8 NULL,
	tnode_ float8 NULL,
	lpoly_ float8 NULL,
	rpoly_ float8 NULL,
	length numeric NULL,
	hydntg_ float8 NULL,
	hydntg_id float8 NULL,
	"share" int4 NULL,
	gbcode int4 NULL,
	"name" varchar(60) NULL,
	"class" int4 NULL,
	r_code varchar(6) NULL,
	basin1 varchar(1) NULL,
	basin2 varchar(1) NULL,
	branch varchar(2) NULL,
	branch2 varchar(1) NULL,
	"level" int2 NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_river4_polyline_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_river4_polyline_geom_idx ON map_river4_polyline USING gist (geom);


-- public.map_river5_polyline definition

-- Drop table

-- DROP TABLE public.map_river5_polyline;

CREATE TABLE public.map_river5_polyline (
	gid serial NOT NULL,
	fnode_ float8 NULL,
	tnode_ float8 NULL,
	lpoly_ float8 NULL,
	rpoly_ float8 NULL,
	length numeric NULL,
	hydntg_ float8 NULL,
	hydntg_id float8 NULL,
	"share" int4 NULL,
	gbcode int4 NULL,
	"name" varchar(60) NULL,
	"class" int4 NULL,
	r_code varchar(6) NULL,
	basin1 varchar(1) NULL,
	basin2 varchar(1) NULL,
	branch varchar(2) NULL,
	branch2 varchar(1) NULL,
	"level" int2 NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_river5_polyline_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_river5_polyline_geom_idx ON map_river5_polyline USING gist (geom);


-- public.map_river_region definition

-- Drop table

-- DROP TABLE public.map_river_region;

CREATE TABLE public.map_river_region (
	gid serial NOT NULL,
	"name" varchar(60) NULL,
	kind varchar(4) NULL,
	geom geometry(MULTIPOLYGON) NULL,
	CONSTRAINT gis_river_region_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_river_region_geom_idx ON map_river_region USING gist (geom);


-- public.map_roa_4m definition

-- Drop table

-- DROP TABLE public.map_roa_4m;

CREATE TABLE public.map_roa_4m (
	gid serial NOT NULL,
	fnode_ float8 NULL,
	tnode_ float8 NULL,
	lpoly_ float8 NULL,
	rpoly_ float8 NULL,
	length float8 NULL,
	roa_4m_ float8 NULL,
	roa_4m_id float8 NULL,
	gbcode int4 NULL,
	geom geometry(MULTILINESTRING) NULL,
	CONSTRAINT gis_roa_4m_pkey PRIMARY KEY (gid)
);
CREATE INDEX gis_roa_4m_geom_idx ON map_roa_4m USING gist (geom);


-- public.meta_api definition

-- Drop table

-- DROP TABLE public.meta_api;

CREATE TABLE public.meta_api (
	id bigserial NOT NULL,
	sid int8 NULL,
	"name" varchar(255) NULL,
	url varchar(255) NULL,
	"action" varchar(255) NULL,
	format varchar(255) NULL,
	status varchar(255) NULL,
	view_count int4 NULL,
	CONSTRAINT meta_api_pkey PRIMARY KEY (id)
);


-- public.meta_app definition

-- Drop table

-- DROP TABLE public.meta_app;

CREATE TABLE public.meta_app (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	url varchar(255) NULL,
	status varchar(255) NULL,
	CONSTRAINT meta_app_pkey PRIMARY KEY (id)
);


-- public.meta_file definition

-- Drop table

-- DROP TABLE public.meta_file;

CREATE TABLE public.meta_file (
	id bigserial NOT NULL,
	sid int4 NULL,
	user_id int8 NULL, -- 上传附件的用户ID
	title varchar(255) NULL, -- 标题
	description text NULL, -- 附件描述
	"path" varchar(512) NULL, -- 路径
	mime_type varchar(128) NULL, -- mime
	suffix varchar(32) NULL, -- 附件的后缀
	filesize int8 NULL,
	"type" varchar(32) NULL, -- 类型
	flag varchar(256) NULL, -- 标示
	orders int8 NULL,
	status int2 NOT NULL, -- 是否可以被访问
	created timestamp NULL, -- 上传时间
	modified timestamp NULL, -- 修改时间
	"permission" varchar(255) NULL, -- 表格权限
	CONSTRAINT meta_file_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.meta_file.user_id IS '上传附件的用户ID';
COMMENT ON COLUMN public.meta_file.title IS '标题';
COMMENT ON COLUMN public.meta_file.description IS '附件描述';
COMMENT ON COLUMN public.meta_file."path" IS '路径';
COMMENT ON COLUMN public.meta_file.mime_type IS 'mime';
COMMENT ON COLUMN public.meta_file.suffix IS '附件的后缀';
COMMENT ON COLUMN public.meta_file."type" IS '类型';
COMMENT ON COLUMN public.meta_file.flag IS '标示';
COMMENT ON COLUMN public.meta_file.status IS '是否可以被访问';
COMMENT ON COLUMN public.meta_file.created IS '上传时间';
COMMENT ON COLUMN public.meta_file.modified IS '修改时间';
COMMENT ON COLUMN public.meta_file."permission" IS '表格权限';


-- public.meta_file_catalog definition

-- Drop table

-- DROP TABLE public.meta_file_catalog;

CREATE TABLE public.meta_file_catalog (
	id bigserial NOT NULL,
	sid int4 NULL,
	user_id int8 NULL,
	pid int8 NULL,
	title varchar(255) NULL,
	filesize int8 NULL,
	"type" varchar(32) NULL,
	flag varchar(256) NULL,
	orders int8 NULL,
	status int2 NULL,
	created timestamp NULL,
	modified timestamp NULL,
	"permission" varchar(255) NULL,
	CONSTRAINT meta_file_catalog_pkey PRIMARY KEY (id)
);


-- public.meta_file_catalog_mapping definition

-- Drop table

-- DROP TABLE public.meta_file_catalog_mapping;

CREATE TABLE public.meta_file_catalog_mapping (
	catalog_id int8 NOT NULL,
	file_id int8 NOT NULL,
	CONSTRAINT meta_file_catalog_mapping_pkey PRIMARY KEY (catalog_id, file_id)
);


-- public.meta_image definition

-- Drop table

-- DROP TABLE public.meta_image;

CREATE TABLE public.meta_image (
	id bigserial NOT NULL,
	sid int8 NULL, -- meta_image_set的id
	user_id int8 NULL, -- 上传附件的用户ID
	"name" varchar(45) NOT NULL,
	description varchar(255) NULL, -- 标题
	"path" varchar(512) NULL, -- 路径
	mime_type varchar(128) NULL, -- mime
	suffix varchar(32) NULL, -- 附件的后缀
	filesize int8 NULL,
	width int4 NULL, -- 类型
	height int4 NULL, -- 标示
	orders int8 NULL,
	status int2 NOT NULL, -- 是否可以被访问
	created timestamp NULL, -- 上传时间
	CONSTRAINT meta_image_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.meta_image.sid IS 'meta_image_set的id';
COMMENT ON COLUMN public.meta_image.user_id IS '上传附件的用户ID';
COMMENT ON COLUMN public.meta_image.description IS '标题';
COMMENT ON COLUMN public.meta_image."path" IS '路径';
COMMENT ON COLUMN public.meta_image.mime_type IS 'mime';
COMMENT ON COLUMN public.meta_image.suffix IS '附件的后缀';
COMMENT ON COLUMN public.meta_image.width IS '类型';
COMMENT ON COLUMN public.meta_image.height IS '标示';
COMMENT ON COLUMN public.meta_image.status IS '是否可以被访问';
COMMENT ON COLUMN public.meta_image.created IS '上传时间';


-- public.meta_image_set definition

-- Drop table

-- DROP TABLE public.meta_image_set;

CREATE TABLE public.meta_image_set (
	id bigserial NOT NULL,
	sid int8 NULL, -- meta_store_resource的id
	user_id int8 NULL,
	"name" varchar(255) NOT NULL,
	description text NULL,
	"path" varchar(255) NULL,
	"type" varchar(255) NULL,
	flag varchar(255) NULL,
	num int4 NULL,
	orders int8 NULL,
	created timestamp NULL,
	modified timestamp NULL,
	status int2 NULL,
	CONSTRAINT meta_image_set_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.meta_image_set.sid IS 'meta_store_resource的id';


-- public.meta_store definition

-- Drop table

-- DROP TABLE public.meta_store;

CREATE TABLE public.meta_store (
	id bigserial NOT NULL,
	appid int8 NULL,
	"name" varchar(255) NULL,
	"type" varchar(255) NULL,
	CONSTRAINT meta_store_pkey PRIMARY KEY (id)
);


-- public.meta_store_db definition

-- Drop table

-- DROP TABLE public.meta_store_db;

CREATE TABLE public.meta_store_db (
	id bigserial NOT NULL,
	sid int8 NULL,
	dbname varchar(255) NULL,
	dbtype varchar(255) NULL,
	dburl varchar(255) NULL,
	"user" varchar(255) NULL,
	"password" varchar(255) NULL,
	CONSTRAINT meta_store_db_pkey PRIMARY KEY (id)
);


-- public.meta_store_resource definition

-- Drop table

-- DROP TABLE public.meta_store_resource;

CREATE TABLE public.meta_store_resource (
	id bigserial NOT NULL,
	sid int8 NULL,
	pid int8 NULL, -- 上级id
	"name" varchar(255) NULL,
	url varchar(255) NULL,
	summary text NULL,
	status varchar(255) NULL,
	CONSTRAINT meta_store_resource_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.meta_store_resource.pid IS '上级id';


-- public.meta_store_route definition

-- Drop table

-- DROP TABLE public.meta_store_route;

CREATE TABLE public.meta_store_route (
	id bigserial NOT NULL,
	sid int8 NULL,
	"name" varchar(255) NULL,
	url varchar(255) NULL,
	summary text NULL,
	status varchar(255) NULL,
	CONSTRAINT meta_store_route_pkey PRIMARY KEY (id)
);


-- public.meta_table definition

-- Drop table

-- DROP TABLE public.meta_table;

CREATE TABLE public.meta_table (
	id bigserial NOT NULL,
	sid int8 NULL,
	user_id int8 NULL,
	"name" varchar(45) NOT NULL,
	title varchar(255) NULL,
	keyname varchar(255) NULL, -- 主键名称。id,tid
	namefld varchar(255) NULL,
	olap_type varchar(255) NULL, -- olap类型（字典1类，主表2类，映射3类，多主键字典4类）
	"type" varchar(255) NULL,
	created timestamp NULL,
	modified timestamp NULL,
	status int4 NULL, -- 表格状态
	orders int8 NULL,
	"permission" varchar(255) NULL, -- 表格权限
	CONSTRAINT meta_table_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.meta_table.keyname IS '主键名称。id,tid';
COMMENT ON COLUMN public.meta_table.olap_type IS 'olap类型（字典1类，主表2类，映射3类，多主键字典4类）';
COMMENT ON COLUMN public.meta_table.status IS '表格状态';
COMMENT ON COLUMN public.meta_table."permission" IS '表格权限';


-- public.meta_theme definition

-- Drop table

-- DROP TABLE public.meta_theme;

CREATE TABLE public.meta_theme (
	id bigserial NOT NULL,
	"name" varchar(255) NULL, -- 主题名称
	userid int8 NULL,
	tmtype varchar(30) NULL,
	active int4 NULL,
	CONSTRAINT meta_theme_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.meta_theme."name" IS '主题名称';


-- public.pointcloud_formats definition

-- Drop table

-- DROP TABLE public.pointcloud_formats;

CREATE TABLE public.pointcloud_formats (
	pcid serial NOT NULL,
	srid int4 NULL,
	"schema" text NULL,
	CONSTRAINT pointcloud_formats_pkey PRIMARY KEY (pcid)
);


-- public.sys_tableconfig definition

-- Drop table

-- DROP TABLE public.sys_tableconfig;

CREATE TABLE public.sys_tableconfig (
	tbid bigserial NOT NULL,
	tbname varchar(45) NOT NULL,
	tbcnn varchar(255) NULL,
	isdelete varchar(1) NULL,
	isedit varchar(1) NULL,
	ispages varchar(1) NULL,
	isallsel varchar(1) NULL,
	isorder varchar(1) NULL,
	isdiycol varchar(1) NULL,
	diycolname varchar(45) NULL,
	tborder int4 NULL,
	tbrole int4 NULL,
	tbcreatetime timestamp NULL,
	tbmodifytime timestamp NULL,
	tbnavtypeid int4 NULL,
	delname varchar(45) NULL,
	iscreate varchar(1) NULL,
	meta varchar(255) NULL,
	ext01 varchar(16) NULL,
	ext02 varchar(32) NULL,
	ext03 varchar(64) NULL,
	ext04 varchar(128) NULL,
	ext05 varchar(255) NULL,
	orglevel varchar(32) NULL,
	tbtype varchar(32) NULL,
	haslonlat varchar(2) NULL,
	hasorglevel varchar(2) NULL,
	CONSTRAINT sys_tableconfig_pkey PRIMARY KEY (tbid)
);


-- public.upms_credit definition

-- Drop table

-- DROP TABLE public.upms_credit;

CREATE TABLE public.upms_credit (
	id bigserial NOT NULL,
	user_id int8 NULL,
	credit int4 NULL,
	ctype varchar(255) NULL,
	ctime timestamp NULL,
	"action" varchar(255) NULL,
	CONSTRAINT upms_credit_pkey PRIMARY KEY (id)
);


-- public.upms_favorites definition

-- Drop table

-- DROP TABLE public.upms_favorites;

CREATE TABLE public.upms_favorites (
	id bigserial NOT NULL,
	user_id int8 NULL,
	title varchar(255) NULL, -- 收藏内容的标题
	url varchar(255) NULL, -- 收藏内容的原文地址，不带域名
	description text NULL, -- 收藏内容的描述
	tags varchar(255) NULL,
	ctime numeric(20) NULL,
	CONSTRAINT upms_favorites_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.upms_favorites.title IS '收藏内容的标题';
COMMENT ON COLUMN public.upms_favorites.url IS '收藏内容的原文地址，不带域名';
COMMENT ON COLUMN public.upms_favorites.description IS '收藏内容的描述';


-- public.upms_log definition

-- Drop table

-- DROP TABLE public.upms_log;

CREATE TABLE public.upms_log (
	log_id serial NOT NULL, -- 编号
	description varchar(100) NULL, -- 操作描述
	username varchar(20) NULL, -- 操作用户
	start_time int8 NULL, -- 操作时间
	spend_time int4 NULL, -- 消耗时间
	base_path varchar(500) NULL, -- 根路径
	uri varchar(500) NULL, -- URI
	url varchar(500) NULL, -- URL
	"method" varchar(10) NULL, -- 请求类型
	"parameter" text NULL,
	user_agent varchar(500) NULL, -- 用户标识
	ip varchar(30) NULL, -- IP地址
	"result" text NULL,
	permissions varchar(100) NULL, -- 权限值
	CONSTRAINT upms_log_pkey PRIMARY KEY (log_id)
);
COMMENT ON TABLE public.upms_log IS '操作日志';

-- Column comments

COMMENT ON COLUMN public.upms_log.log_id IS '编号';
COMMENT ON COLUMN public.upms_log.description IS '操作描述';
COMMENT ON COLUMN public.upms_log.username IS '操作用户';
COMMENT ON COLUMN public.upms_log.start_time IS '操作时间';
COMMENT ON COLUMN public.upms_log.spend_time IS '消耗时间';
COMMENT ON COLUMN public.upms_log.base_path IS '根路径';
COMMENT ON COLUMN public.upms_log.uri IS 'URI';
COMMENT ON COLUMN public.upms_log.url IS 'URL';
COMMENT ON COLUMN public.upms_log."method" IS '请求类型';
COMMENT ON COLUMN public.upms_log.user_agent IS '用户标识';
COMMENT ON COLUMN public.upms_log.ip IS 'IP地址';
COMMENT ON COLUMN public.upms_log.permissions IS '权限值';


-- public.upms_organization definition

-- Drop table

-- DROP TABLE public.upms_organization;

CREATE TABLE public.upms_organization (
	organization_id bigserial NOT NULL, -- 编号
	pid int4 NULL, -- 所属上级
	"name" varchar(20) NULL, -- 组织名称
	description varchar(1000) NULL, -- 组织描述
	ctime int8 NULL, -- 创建时间
	CONSTRAINT upms_organization_pkey PRIMARY KEY (organization_id)
);
COMMENT ON TABLE public.upms_organization IS '组织';

-- Column comments

COMMENT ON COLUMN public.upms_organization.organization_id IS '编号';
COMMENT ON COLUMN public.upms_organization.pid IS '所属上级';
COMMENT ON COLUMN public.upms_organization."name" IS '组织名称';
COMMENT ON COLUMN public.upms_organization.description IS '组织描述';
COMMENT ON COLUMN public.upms_organization.ctime IS '创建时间';


-- public.upms_permission definition

-- Drop table

-- DROP TABLE public.upms_permission;

CREATE TABLE public.upms_permission (
	permission_id bigserial NOT NULL, -- 编号
	system_id int8 NOT NULL, -- 所属系统
	pid int4 NULL, -- 所属上级
	"name" varchar(20) NULL, -- 名称
	"type" int2 NULL, -- 类型(1:目录,2:菜单,3:按钮)
	permission_value varchar(50) NULL, -- 权限值
	uri varchar(100) NULL, -- 路径
	icon varchar(50) NULL, -- 图标
	status int2 NULL, -- 状态(0:禁止,1:正常)
	ctime int8 NULL, -- 创建时间
	orders int8 NULL, -- 排序
	CONSTRAINT upms_permission_pkey PRIMARY KEY (permission_id)
);
COMMENT ON TABLE public.upms_permission IS '权限';

-- Column comments

COMMENT ON COLUMN public.upms_permission.permission_id IS '编号';
COMMENT ON COLUMN public.upms_permission.system_id IS '所属系统';
COMMENT ON COLUMN public.upms_permission.pid IS '所属上级';
COMMENT ON COLUMN public.upms_permission."name" IS '名称';
COMMENT ON COLUMN public.upms_permission."type" IS '类型(1:目录,2:菜单,3:按钮)';
COMMENT ON COLUMN public.upms_permission.permission_value IS '权限值';
COMMENT ON COLUMN public.upms_permission.uri IS '路径';
COMMENT ON COLUMN public.upms_permission.icon IS '图标';
COMMENT ON COLUMN public.upms_permission.status IS '状态(0:禁止,1:正常)';
COMMENT ON COLUMN public.upms_permission.ctime IS '创建时间';
COMMENT ON COLUMN public.upms_permission.orders IS '排序';


-- public.upms_role definition

-- Drop table

-- DROP TABLE public.upms_role;

CREATE TABLE public.upms_role (
	role_id bigserial NOT NULL, -- 编号
	"name" varchar(20) NULL, -- 角色名称
	title varchar(20) NULL, -- 角色标题
	description varchar(1000) NULL, -- 角色描述
	ctime int8 NOT NULL, -- 创建时间
	orders int8 NOT NULL, -- 排序
	CONSTRAINT upms_role_pkey PRIMARY KEY (role_id)
);
COMMENT ON TABLE public.upms_role IS '角色';

-- Column comments

COMMENT ON COLUMN public.upms_role.role_id IS '编号';
COMMENT ON COLUMN public.upms_role."name" IS '角色名称';
COMMENT ON COLUMN public.upms_role.title IS '角色标题';
COMMENT ON COLUMN public.upms_role.description IS '角色描述';
COMMENT ON COLUMN public.upms_role.ctime IS '创建时间';
COMMENT ON COLUMN public.upms_role.orders IS '排序';


-- public.upms_role_permission definition

-- Drop table

-- DROP TABLE public.upms_role_permission;

CREATE TABLE public.upms_role_permission (
	role_permission_id bigserial NOT NULL, -- 编号
	role_id int8 NOT NULL, -- 角色编号
	permission_id int8 NOT NULL, -- 权限编号
	CONSTRAINT upms_role_permission_pkey PRIMARY KEY (role_permission_id)
);
COMMENT ON TABLE public.upms_role_permission IS '角色权限关联表';

-- Column comments

COMMENT ON COLUMN public.upms_role_permission.role_permission_id IS '编号';
COMMENT ON COLUMN public.upms_role_permission.role_id IS '角色编号';
COMMENT ON COLUMN public.upms_role_permission.permission_id IS '权限编号';


-- public.upms_sign definition

-- Drop table

-- DROP TABLE public.upms_sign;

CREATE TABLE public.upms_sign (
	id bigserial NOT NULL,
	user_id int8 NULL,
	stime timestamp NULL,
	lng float4 NULL,
	lat float4 NULL,
	sway varchar(255) NULL, -- 签到方式，web，android，等终端
	CONSTRAINT upms_sign_pkey PRIMARY KEY (id)
);

-- Column comments

COMMENT ON COLUMN public.upms_sign.sway IS '签到方式，web，android，等终端';


-- public.upms_system definition

-- Drop table

-- DROP TABLE public.upms_system;

CREATE TABLE public.upms_system (
	system_id bigserial NOT NULL, -- 编号
	icon varchar(50) NULL, -- 图标
	banner varchar(150) NULL, -- 背景
	theme varchar(50) NULL, -- 主题
	basepath varchar(100) NULL, -- 根目录
	status int2 NULL, -- 状态(-1:黑名单,1:正常)
	"name" varchar(20) NULL, -- 系统名称
	title varchar(20) NULL, -- 系统标题
	description varchar(300) NULL, -- 系统描述
	ctime int8 NULL, -- 创建时间
	orders int8 NULL, -- 排序
	CONSTRAINT upms_system_pkey PRIMARY KEY (system_id)
);
COMMENT ON TABLE public.upms_system IS '系统';

-- Column comments

COMMENT ON COLUMN public.upms_system.system_id IS '编号';
COMMENT ON COLUMN public.upms_system.icon IS '图标';
COMMENT ON COLUMN public.upms_system.banner IS '背景';
COMMENT ON COLUMN public.upms_system.theme IS '主题';
COMMENT ON COLUMN public.upms_system.basepath IS '根目录';
COMMENT ON COLUMN public.upms_system.status IS '状态(-1:黑名单,1:正常)';
COMMENT ON COLUMN public.upms_system."name" IS '系统名称';
COMMENT ON COLUMN public.upms_system.title IS '系统标题';
COMMENT ON COLUMN public.upms_system.description IS '系统描述';
COMMENT ON COLUMN public.upms_system.ctime IS '创建时间';
COMMENT ON COLUMN public.upms_system.orders IS '排序';


-- public.upms_tag definition

-- Drop table

-- DROP TABLE public.upms_tag;

CREATE TABLE public.upms_tag (
	id bigserial NOT NULL, -- 标签编号
	user_id int8 NULL,
	"name" varchar(20) NOT NULL, -- 名称
	description varchar(200) NULL, -- 描述
	orders numeric(20) NOT NULL, -- 排序
	ctime numeric(20) NULL,
	CONSTRAINT upms_tag_pkey PRIMARY KEY (id)
);
COMMENT ON TABLE public.upms_tag IS '标签表';

-- Column comments

COMMENT ON COLUMN public.upms_tag.id IS '标签编号';
COMMENT ON COLUMN public.upms_tag."name" IS '名称';
COMMENT ON COLUMN public.upms_tag.description IS '描述';
COMMENT ON COLUMN public.upms_tag.orders IS '排序';


-- public.upms_user definition

-- Drop table

-- DROP TABLE public.upms_user;

CREATE TABLE public.upms_user (
	user_id bigserial NOT NULL, -- 编号
	username varchar(20) NOT NULL, -- 帐号
	"password" varchar(32) NOT NULL, -- 密码MD5(密码+盐)
	salt varchar(32) NULL, -- 盐
	realname varchar(20) NULL, -- 姓名
	avatar varchar(150) NULL, -- 头像
	phone varchar(20) NULL, -- 电话
	email varchar(50) NULL, -- 邮箱
	sex int2 NULL, -- 性别
	locked int2 NULL, -- 状态(0:正常,1:锁定)
	ctime int8 NULL, -- 创建时间
	CONSTRAINT upms_user_pkey PRIMARY KEY (user_id)
);
COMMENT ON TABLE public.upms_user IS '用户';

-- Column comments

COMMENT ON COLUMN public.upms_user.user_id IS '编号';
COMMENT ON COLUMN public.upms_user.username IS '帐号';
COMMENT ON COLUMN public.upms_user."password" IS '密码MD5(密码+盐)';
COMMENT ON COLUMN public.upms_user.salt IS '盐';
COMMENT ON COLUMN public.upms_user.realname IS '姓名';
COMMENT ON COLUMN public.upms_user.avatar IS '头像';
COMMENT ON COLUMN public.upms_user.phone IS '电话';
COMMENT ON COLUMN public.upms_user.email IS '邮箱';
COMMENT ON COLUMN public.upms_user.sex IS '性别';
COMMENT ON COLUMN public.upms_user.locked IS '状态(0:正常,1:锁定)';
COMMENT ON COLUMN public.upms_user.ctime IS '创建时间';


-- public.upms_user_credit definition

-- Drop table

-- DROP TABLE public.upms_user_credit;

CREATE TABLE public.upms_user_credit (
	user_id int8 NOT NULL,
	credit int4 NULL
);


-- public.upms_user_organization definition

-- Drop table

-- DROP TABLE public.upms_user_organization;

CREATE TABLE public.upms_user_organization (
	user_organization_id bigserial NOT NULL, -- 编号
	user_id int8 NOT NULL, -- 用户编号
	organization_id int8 NOT NULL, -- 组织编号
	CONSTRAINT upms_user_organization_pkey PRIMARY KEY (user_organization_id)
);
COMMENT ON TABLE public.upms_user_organization IS '用户组织关联表';

-- Column comments

COMMENT ON COLUMN public.upms_user_organization.user_organization_id IS '编号';
COMMENT ON COLUMN public.upms_user_organization.user_id IS '用户编号';
COMMENT ON COLUMN public.upms_user_organization.organization_id IS '组织编号';


-- public.upms_user_permission definition

-- Drop table

-- DROP TABLE public.upms_user_permission;

CREATE TABLE public.upms_user_permission (
	user_permission_id bigserial NOT NULL, -- 编号
	user_id int8 NOT NULL, -- 用户编号
	permission_id int8 NOT NULL, -- 权限编号
	"type" int2 NOT NULL, -- 权限类型(-1:减权限,1:增权限)
	CONSTRAINT upms_user_permission_pkey PRIMARY KEY (user_permission_id)
);
COMMENT ON TABLE public.upms_user_permission IS '用户权限关联表';

-- Column comments

COMMENT ON COLUMN public.upms_user_permission.user_permission_id IS '编号';
COMMENT ON COLUMN public.upms_user_permission.user_id IS '用户编号';
COMMENT ON COLUMN public.upms_user_permission.permission_id IS '权限编号';
COMMENT ON COLUMN public.upms_user_permission."type" IS '权限类型(-1:减权限,1:增权限)';


-- public.upms_user_role definition

-- Drop table

-- DROP TABLE public.upms_user_role;

CREATE TABLE public.upms_user_role (
	user_role_id bigserial NOT NULL, -- 编号
	user_id int8 NOT NULL, -- 用户编号
	role_id int4 NULL, -- 角色编号
	CONSTRAINT upms_user_role_pkey PRIMARY KEY (user_role_id)
);
COMMENT ON TABLE public.upms_user_role IS '用户角色关联表';

-- Column comments

COMMENT ON COLUMN public.upms_user_role.user_role_id IS '编号';
COMMENT ON COLUMN public.upms_user_role.user_id IS '用户编号';
COMMENT ON COLUMN public.upms_user_role.role_id IS '角色编号';


-- public.meta_field definition

-- Drop table

-- DROP TABLE public.meta_field;

CREATE TABLE public.meta_field (
	id bigserial NOT NULL,
	tbid int8 NOT NULL,
	"name" varchar(45) NOT NULL,
	title varchar(255) NULL,
	"datatype" varchar(45) NULL,
	classtype varchar(45) NULL,
	iskey varchar(1) NULL,
	isunsigned varchar(1) NULL,
	isnullable varchar(1) NULL,
	isai varchar(1) NULL,
	flddefault varchar(45) NULL,
	descript text NULL,
	isfk varchar(1) NULL,
	fktbid int8 NULL,
	isview varchar(32) NULL,
	isselect varchar(32) NULL,
	isedit varchar(32) NULL,
	ismustfld varchar(32) NULL,
	ismap varchar(32) NULL,
	olap varchar(16) NULL, -- dimession or measure
	orders int8 NULL,
	"permission" varchar(255) NULL,
	CONSTRAINT meta_field_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_tbid_fkey FOREIGN KEY (tbid) REFERENCES meta_table(id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Column comments

COMMENT ON COLUMN public.meta_field.olap IS 'dimession or measure';


-- public.meta_field_dimession definition

-- Drop table

-- DROP TABLE public.meta_field_dimession;

CREATE TABLE public.meta_field_dimession (
	id bigserial NOT NULL,
	tbid int8 NULL,
	fldid int8 NULL,
	rtbid int8 NULL,
	rtype varchar(255) NULL, -- 关联类型，直连direct，字典dictionary
	rfldid int8 NULL, -- 直连模式的时候，对应的字段
	dtype varchar(255) NULL, -- 维度类型，编码code or 名称name
	level_type varchar(255) NULL, -- 级别类型（单表级别id为1类，字典级别关系为2类，单表上级字段3类）
	parentfldid int8 NULL, -- 上级字段
	childfldid int8 NULL, -- 下级字段
	CONSTRAINT meta_field_dimession_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_dimession_fldid_fkey FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE
);

-- Column comments

COMMENT ON COLUMN public.meta_field_dimession.rtype IS '关联类型，直连direct，字典dictionary';
COMMENT ON COLUMN public.meta_field_dimession.rfldid IS '直连模式的时候，对应的字段';
COMMENT ON COLUMN public.meta_field_dimession.dtype IS '维度类型，编码code or 名称name';
COMMENT ON COLUMN public.meta_field_dimession.level_type IS '级别类型（单表级别id为1类，字典级别关系为2类，单表上级字段3类）';
COMMENT ON COLUMN public.meta_field_dimession.parentfldid IS '上级字段';
COMMENT ON COLUMN public.meta_field_dimession.childfldid IS '下级字段';


-- public.meta_field_edit definition

-- Drop table

-- DROP TABLE public.meta_field_edit;

CREATE TABLE public.meta_field_edit (
	id bigserial NOT NULL,
	fldid int8 NULL,
	tmid int8 NULL,
	checktype int8 NULL,
	edittype int8 NULL,
	editid int8 NULL,
	editminlen int8 NULL,
	editmaxlen int8 NULL,
	editorder int8 NULL,
	CONSTRAINT meta_field_edit_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_edit_fldid_fkey FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE,
	CONSTRAINT meta_field_edit_fldid_fkey1 FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE
);


-- public.meta_field_list definition

-- Drop table

-- DROP TABLE public.meta_field_list;

CREATE TABLE public.meta_field_list (
	id bigserial NOT NULL,
	fldid int8 NOT NULL,
	tmid int8 NULL, -- 主题id
	viewname varchar(255) NULL,
	isview varchar(1) NULL,
	isorder varchar(1) NULL,
	viewmaxlen int8 NULL,
	viewtype varchar(45) NULL,
	vieworder int8 NULL,
	issearch varchar(1) NULL,
	searchtype varchar(255) NULL,
	searchinfo varchar(255) NULL,
	CONSTRAINT meta_field_list_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_list_fldid_fkey FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE
);

-- Column comments

COMMENT ON COLUMN public.meta_field_list.tmid IS '主题id';


-- public.meta_field_map definition

-- Drop table

-- DROP TABLE public.meta_field_map;

CREATE TABLE public.meta_field_map (
	id bigserial NOT NULL,
	fldid int8 NOT NULL,
	mtype varchar(45) NULL, -- 点、线、面
	srid varchar(45) NULL, -- 坐标类型,4326
	geotype varchar(45) NULL, -- lon,lat,point,geom,wtk等
	CONSTRAINT meta_field_map_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_map_fldid_fkey FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE
);

-- Column comments

COMMENT ON COLUMN public.meta_field_map.mtype IS '点、线、面';
COMMENT ON COLUMN public.meta_field_map.srid IS '坐标类型,4326';
COMMENT ON COLUMN public.meta_field_map.geotype IS 'lon,lat,point,geom,wtk等';


-- public.meta_field_measure definition

-- Drop table

-- DROP TABLE public.meta_field_measure;

CREATE TABLE public.meta_field_measure (
	id bigserial NOT NULL,
	fldid int8 NOT NULL,
	"name" int8 NULL, -- 度量名称
	"type" varchar(64) NULL, -- 单字段、多字段、多表格字段等类型
	agg varchar(32) NULL, -- 度量方法
	formula varchar(255) NULL, -- 公式
	CONSTRAINT meta_field_measure_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_measure_fldid_fkey FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE
);

-- Column comments

COMMENT ON COLUMN public.meta_field_measure."name" IS '度量名称';
COMMENT ON COLUMN public.meta_field_measure."type" IS '单字段、多字段、多表格字段等类型';
COMMENT ON COLUMN public.meta_field_measure.agg IS '度量方法';
COMMENT ON COLUMN public.meta_field_measure.formula IS '公式';


-- public.meta_field_relation definition

-- Drop table

-- DROP TABLE public.meta_field_relation;

CREATE TABLE public.meta_field_relation (
	id bigserial NOT NULL,
	tbid int8 NULL,
	fldid int8 NULL, -- 关联元素
	relation varchar(255) NULL,
	rtbid int8 NULL, -- 对应的关联表格
	rfldid int8 NULL, -- 对应的关联字段名称
	CONSTRAINT meta_field_relation_pkey PRIMARY KEY (id),
	CONSTRAINT meta_field_relation_fldid_fkey FOREIGN KEY (fldid) REFERENCES meta_field(id) ON DELETE CASCADE
);

-- Column comments

COMMENT ON COLUMN public.meta_field_relation.fldid IS '关联元素';
COMMENT ON COLUMN public.meta_field_relation.rtbid IS '对应的关联表格';
COMMENT ON COLUMN public.meta_field_relation.rfldid IS '对应的关联字段名称';


-- public.sys_fieldconfig definition

-- Drop table

-- DROP TABLE public.sys_fieldconfig;

CREATE TABLE public.sys_fieldconfig (
	fldid bigserial NOT NULL,
	fldtbid int8 NOT NULL,
	fldname varchar(45) NOT NULL,
	fldcnn varchar(255) NULL,
	"datatype" varchar(45) NULL,
	iskey varchar(1) NULL,
	isunsigned varchar(1) NULL,
	isnullable varchar(1) NULL,
	isai varchar(1) NULL,
	flddefault varchar(45) NULL,
	descript text NULL,
	isfk varchar(1) NULL,
	fktbname varchar(45) NULL,
	fktbkey varchar(45) NULL,
	fldlinkfk varchar(45) NULL,
	checktype int4 NULL,
	edittype int4 NULL,
	fldmetaid int4 NULL,
	isselect varchar(1) NULL,
	isview varchar(1) NULL,
	maxlenview int4 NULL,
	viewfldorder int4 NULL,
	isedit varchar(1) NULL,
	editorder varchar(45) NULL,
	editminlen int4 NULL,
	editmaxlen int4 NULL,
	ismustfld varchar(1) NULL,
	fldorder int4 NULL,
	fldrole int4 NULL,
	meta varchar(255) NULL,
	ext01 varchar(16) NULL,
	ext02 varchar(32) NULL,
	ext03 varchar(64) NULL,
	ext04 varchar(128) NULL,
	ext05 varchar(255) NULL,
	"update" text NULL,
	searchtype varchar(255) NULL,
	searchinfo varchar(255) NULL,
	tag varchar(64) NULL,
	heji varchar(32) NULL,
	CONSTRAINT sys_fieldconfig_pkey PRIMARY KEY (fldid),
	CONSTRAINT sys_fieldconfig_fldtbid_fkey FOREIGN KEY (fldtbid) REFERENCES sys_tableconfig(tbid) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO public.upms_user
(username, "password", salt, realname, avatar, phone, email, sex, locked, ctime)
VALUES('admin', '3038D9CB63B3152A79B8153FB06C02F7', '66f1b370c660445a8657bf8bf1794486', '管理员', '/lambkit/assets/upms/images/avatar.jpg', '', '469741414@qq.com', 1, 0, 1493394720495);
